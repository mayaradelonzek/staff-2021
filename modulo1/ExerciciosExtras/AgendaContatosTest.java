import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class AgendaContatosTest {

    @Test
    public void adicionarContatoEPesquisa(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Marcos", "555555");
        String resultado = agenda.consultar("Marcos");
        assertEquals("555555", resultado);
    }
    
    @Test
    public void adicionarContatoEPesquisaPeloTelefone(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Marcos", "555555");
        String resultado = agenda.consutarPeloTelefone("555555");
        assertEquals("Marcos", resultado);
    }
    
    @Test
    public void adicionarDoisContatosEGerarCSV(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Marcos", "555555");
        agenda.adicionar("Mithrandir", "444444");
        String separador = System.lineSeparator();
        String resultado = String.format("Marcos,555555%sMithrandir,444444%s", separador,separador);
        assertEquals(resultado, agenda.csv());
    }    
}
