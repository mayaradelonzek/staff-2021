import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.*;


public class InventarioTest {
    
    @Test
    public void adicionarUmItem() {
        Inventario inventario = new Inventario();
        Item item = new Item(1, "Adaga");
        inventario.adicionar(item);
        assertEquals(item, inventario.getItens().get(0));
    }
    
    @Test 
    public void adicionarDoisItens() {
        Inventario inventario = new Inventario();
        Item adaga = new Item(1, "Adaga");
        Item escudo = new Item(1, "Escudo");
        inventario.adicionar(adaga);
        inventario.adicionar(escudo);
        assertEquals(adaga, inventario.getItens().get(0));
        assertEquals(escudo, inventario.getItens().get(1));
    }
    
    @Test
    public void adicionarItemEObter() {
        Inventario inventario = new Inventario();
        Item adaga = new Item(1, "Adaga");
        inventario.adicionar(adaga);
        assertEquals(adaga, inventario.obter(0));
    }
    
    @Test
    public void adicionarItemERemover() {
        Inventario inventario = new Inventario();
        Item adaga = new Item(1, "Adaga");
        inventario.adicionar(adaga);
        inventario.remover(0);
        assertNull(inventario.obter(0));
    }
    
    @Test
    public void getDescricoesItensSeparadosPorVirgula() {
        Inventario inventario = new Inventario();
        Item adaga = new Item(1, "Adaga");
        Item escudo = new Item(2, "Escudo");
        inventario.adicionar(adaga);
        inventario.adicionar(escudo);
        String resultado = inventario.getDescricoes();
        assertEquals("Adaga,Escudo", resultado);
    }
    
    @Test
    public void getItemMaiorQuantidade() {
        Inventario inventario = new Inventario();
        Item adaga = new Item(1, "Adaga");
        Item escudo = new Item(2, "Escudo");
        Item espada = new Item(1, "Espada");
        inventario.adicionar(adaga);
        inventario.adicionar(escudo);
        inventario.adicionar(espada);
        Item resultado = inventario.getItemComMaiorQuantidade();
        assertEquals(escudo, resultado);
    }
    
    @Test
    public void getItemMaiorQuantidadeComMesmaQuantidade() {
        Inventario inventario = new Inventario();
        Item adaga = new Item(3, "Adaga");
        Item escudo = new Item(3, "Escudo");
        Item espada = new Item(2, "Espada");
        inventario.adicionar(adaga);
        inventario.adicionar(escudo);
        inventario.adicionar(espada);
        Item resultado = inventario.getItemComMaiorQuantidade();
        assertEquals(adaga, resultado);
    }
    
    @Test
    public void buscarItem() {
        Inventario inventario = new Inventario();
        Item bolsa = new Item(1, "bolsa");
        inventario.adicionar(bolsa);
        Item resultado = inventario.buscar(new String("bolsa"));
        assertEquals(bolsa, resultado);
    } 
    
    @Test
    public void inverterItens() {
        Inventario inventario = new Inventario();
        Item escudo = new Item(1, "Escudo");
        Item espada = new Item(1, "Espada");
        inventario.adicionar(escudo);
        inventario.adicionar(espada);
        ArrayList<Item> resultado = inventario.inverter();
        assertEquals(espada, resultado.get(0));
        assertEquals(escudo, resultado.get(1));
        
        assertEquals(escudo, inventario.obter(0));
        assertEquals(espada, inventario.obter(1));
        
        assertEquals(2, resultado.size());
        
    }
    
    @Test
    public void ordenarItens() {
        ArrayList<Item> itensDesordenados = new ArrayList<>();
        ArrayList<Item> itensOrdenados = new ArrayList<>();
        
        itensDesordenados.add(new Item(6, "item3"));
        itensDesordenados.add(new Item(3, "item1"));
        itensDesordenados.add(new Item(4, "item2"));
        
        itensOrdenados.add(new Item(3, "item1"));
        itensOrdenados.add(new Item(4, "item2"));
        itensOrdenados.add(new Item(6, "item3"));
        
        Inventario inventario = new Inventario();
        inventario.setItens(itensDesordenados);
        inventario.ordernarItens();
        
        assertEquals(itensOrdenados.get(0).getDescricao(), inventario.getItens().get(0).getDescricao());
    }
    
    @Test
    public void ordenarItensPorDesc() {
        ArrayList<Item> itensDesordenados = new ArrayList<>();
        ArrayList<Item> itensOrdenados = new ArrayList<>();
        
        itensDesordenados.add(new Item(4, "item2"));
        itensDesordenados.add(new Item(6, "item1"));
        itensDesordenados.add(new Item(3, "item3"));
        
        itensOrdenados.add(new Item(6, "item1"));
        itensOrdenados.add(new Item(4, "item2"));
        itensOrdenados.add(new Item(3, "item3"));
        
        Inventario inventario = new Inventario();
        inventario.setItens(itensDesordenados);
        inventario.ordernarItens(OrdenacaoEnum.DESC);
        
        assertEquals(itensOrdenados.get(0).getDescricao(), inventario.getItens().get(0).getDescricao());
    }
    
    @Test
    public void ordenarItensPorAsc() {
        ArrayList<Item> itensDesordenados = new ArrayList<>();
        ArrayList<Item> itensOrdenados = new ArrayList<>();
        
        itensDesordenados.add(new Item(4, "item2"));
        itensDesordenados.add(new Item(3, "item1"));
        itensDesordenados.add(new Item(6, "item3"));
        
        itensOrdenados.add(new Item(3, "item1"));
        itensOrdenados.add(new Item(4, "item2"));
        itensOrdenados.add(new Item(6, "item3"));
        
        Inventario inventario = new Inventario();
        inventario.setItens(itensDesordenados);
        inventario.ordernarItens(OrdenacaoEnum.ASC);
        
        assertEquals(itensOrdenados.get(0).getDescricao(), inventario.getItens().get(0).getDescricao());
    }
    
}
