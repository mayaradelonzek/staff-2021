import java.util.*;

public class ElfoDaLuz extends Elfo {
    private double qtdVida;
    private int qtdAtaques;
    
    private final ArrayList<String> DESCRICOES_ITENS = new ArrayList<>(
        Arrays.asList(
            "Espada de Galvorn"                    
        )
    ); 
    
    ElfoDaLuz(String nome) {
        super(nome);
        qtdVida = 10;
        qtdAtaques = 0;
        qtdDano = 21;
        super.ganharItem(new ItemImpossivelDePerder (1, DESCRICOES_ITENS.get(0)));
    }
    
    @Override
    public void perderItem( Item item ) { 
        if( !item.getDescricao().equals( DESCRICOES_ITENS.get(0) )){
            this.inventario.remover( item );
        }
    }
    
    private boolean devePerderVida() {
        return qtdAtaques % 2 != 0;
    }
    
    private void ganharVida() {
        this.vida += qtdVida;        
    }
    
    private Item getEspada() {
        return this.getInventario().buscar(DESCRICOES_ITENS.get(0));
    }
    
    
    
    public void atacarComEspada(Anao anao) {
        //Item espada = getEspada();        
        qtdAtaques++;
        anao.sofrerDano();
            
        if (devePerderVida()) {
            sofrerDano();       
        } else {
            ganharVida();
        }
        
    }
}
