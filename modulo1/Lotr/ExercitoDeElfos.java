import java.util.*;

public class ExercitoDeElfos {

	private final static ArrayList<Class> CLASSES_DO_EXERCITO = new ArrayList<>(
			Arrays.asList(ElfoVerde.class, ElfoNoturno.class));

	private ArrayList<Elfo> elfos = new ArrayList<>();
	private HashMap<Status, ArrayList<Elfo>> porStatus = new HashMap<>();

	public void alistar(Elfo elfo) {
		boolean podeAlistar = CLASSES_DO_EXERCITO.contains(elfo.getClass());

		if (podeAlistar) {
        		elfos.add(elfo);

			ArrayList<Elfo> elfoDeUmStatus = porStatus.get(elfo.getStatus());
			if (elfoDeUmStatus == null) {
				elfoDeUmStatus = new ArrayList<>();
				porStatus.put(elfo.getStatus(), elfoDeUmStatus);
			}
			elfoDeUmStatus.add(elfo);
		}
	}

	public ArrayList<Elfo> buscar(Status status) {
		return this.porStatus.get(status);
	}
	
	public ArrayList<Elfo> buscarExcluindoUmStatus(Status status) {
            ArrayList<Elfo> todosElfos = this.elfos;
            ArrayList<Elfo> elfosParaExcluir = this.porStatus.get(status); 
            todosElfos.removeAll(elfosParaExcluir);
            return todosElfos;
	}

	public ArrayList<Elfo> getElfos() {
		return this.elfos;
	}

	public ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> atacantes, OrdemAtaqueEnum ordemAtaque) {
		ArrayList<Elfo> pelotao = new ArrayList<>();
		HashMap<Class, ArrayList<Elfo>> elfosAgrupadosPorRaca = new HashMap<>();

		if (!OrdemAtaqueEnum.ESTRATEGIA_CUSTOMIZADA.equals(ordemAtaque)) {
			elfosAgrupadosPorRaca = agruparElfosPorRaca(atacantes);
		}

		if (OrdemAtaqueEnum.NOTURNOS_POR_ULTIMO.equals(ordemAtaque)) {
			ordenarPorNoturnosPorUltimo(pelotao, elfosAgrupadosPorRaca);
		} else if (OrdemAtaqueEnum.ATAQUE_INTERCALADO.equals(ordemAtaque)) {
			ordenarPorAtaqueIntercalado(pelotao, elfosAgrupadosPorRaca);
		} else if (OrdemAtaqueEnum.ESTRATEGIA_CUSTOMIZADA.equals(ordemAtaque)) {
			ordenarPorEstrategiaCustomizada(pelotao, atacantes);
		}

		return pelotao;
	}

	private void ordenarPorEstrategiaCustomizada(ArrayList<Elfo> pelotao, ArrayList<Elfo> atacantes) {
		final Float PERCENT_ELFOS_NOTURNOS = 0.3f;
		ArrayList<Elfo> elfosNoturnos = new ArrayList<>();
		int qtdElfosAptos = 0;
		int qtdMaximaElfosNoturnos = 0;
		
		Collections.sort(atacantes, new OrdenarPorQtdFlecha());
		for (Elfo elfo : atacantes) {
			if (!Status.MORTO.equals(elfo.getStatus()) && elfo.getFlecha().getQuantidade() != 0) {
				qtdElfosAptos++;
				if (elfo instanceof ElfoNoturno) {
					elfosNoturnos.add(elfo);
				} else {
					pelotao.add(elfo);
				}
			}
		}

		qtdMaximaElfosNoturnos = (int) (qtdElfosAptos * PERCENT_ELFOS_NOTURNOS);
		qtdMaximaElfosNoturnos = qtdMaximaElfosNoturnos > elfosNoturnos.size() ? elfosNoturnos.size() : qtdMaximaElfosNoturnos;
		
		pelotao.addAll(elfosNoturnos.subList(0, qtdMaximaElfosNoturnos));
	}

	private void ordenarPorNoturnosPorUltimo(ArrayList<Elfo> pelotao, HashMap<Class, ArrayList<Elfo>> elfosAgrupados) {
		pelotao.addAll(elfosAgrupados.get(ElfoVerde.class));
		pelotao.addAll(elfosAgrupados.get(ElfoNoturno.class));
	}

	private void ordenarPorAtaqueIntercalado(ArrayList<Elfo> pelotao, HashMap<Class, ArrayList<Elfo>> elfosAgrupados) {
		int qtdRepeticoes = elfosAgrupados.get(ElfoVerde.class).size() < elfosAgrupados.get(ElfoNoturno.class).size()
				? elfosAgrupados.get(ElfoVerde.class).size()
				: elfosAgrupados.get(ElfoNoturno.class).size();

		for (int i = 0; i < qtdRepeticoes; i++) {
			pelotao.add(elfosAgrupados.get(ElfoVerde.class).get(i));
			pelotao.add(elfosAgrupados.get(ElfoNoturno.class).get(i));
		}
	}

	private HashMap<Class, ArrayList<Elfo>> agruparElfosPorRaca(ArrayList<Elfo> atacantes) {
		HashMap<Class, ArrayList<Elfo>> elfosRaca = new HashMap<>();
		elfosRaca.put(ElfoVerde.class, new ArrayList<Elfo>());
		elfosRaca.put(ElfoNoturno.class, new ArrayList<Elfo>());

		for (Elfo elfo : atacantes) {
			if (!Status.MORTO.equals(elfo.getStatus()) && elfo instanceof ElfoVerde) {
				elfosRaca.get(ElfoVerde.class).add(elfo);
			} else if (!Status.MORTO.equals(elfo.getStatus()) && elfo instanceof ElfoNoturno) {
				elfosRaca.get(ElfoNoturno.class).add(elfo);
			}
		}

		return elfosRaca;
	}

}