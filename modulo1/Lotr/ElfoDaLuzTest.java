
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;


public class ElfoDaLuzTest {
    
    @Test
    public void elfoDaLuzDevePerderVida() {
        ElfoDaLuz elfo1 = new ElfoDaLuz("Elfo1");
        Anao anao1 = new Anao("Anao1");
        elfo1.atacarComEspada(anao1);
        assertEquals(79, elfo1.getVida(), 0.000001);
        assertEquals(100, anao1.getVida(), 0.000001);
    }
    
    @Test
    public void elfoDaLuzDeveGanharVida() {
        ElfoDaLuz elfo1 = new ElfoDaLuz("Elfo1");
        Anao anao1 = new Anao("Anao1");
        elfo1.atacarComEspada(anao1);
        elfo1.atacarComEspada(anao1);
        assertEquals(89, elfo1.getVida(), 0.000001);
        assertEquals(90, anao1.getVida(), 0.000001);
    }
    
}
