import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ElfoNoturnoTest {
    @Test
    public void elfoNoturnoGanha3XpPorFlecha(){
        ElfoNoturno noturno = new ElfoNoturno("Noturno");
        noturno.atirarFlecha(new Anao("Balin"));
        assertEquals( 3, noturno.getExperiencia() );
    }
    
    @Test
    public void elfoNoturnoPerde15HPPorFlecha(){
        ElfoNoturno noturno = new ElfoNoturno("Noturno");
        noturno.atirarFlecha(new Anao("Balin"));
        assertEquals( 85.0, noturno.getVida(), 1e-9 );
        assertEquals( Status.SOFREU_DANO, noturno.getStatus() );
    }
    
    @Test
    public void elfoNoturnoAtira7FlechasEMorre(){
        ElfoNoturno noturno = new ElfoNoturno("Noturno");
        noturno.getInventario().obter(0).setQuantidade(1000);
        
        noturno.atirarFlecha(new Anao("Balin"));
        noturno.atirarFlecha(new Anao("Balin"));
        noturno.atirarFlecha(new Anao("Balin"));
        noturno.atirarFlecha(new Anao("Balin"));
        noturno.atirarFlecha(new Anao("Balin"));
        noturno.atirarFlecha(new Anao("Balin"));
        noturno.atirarFlecha(new Anao("Balin"));
        
        assertEquals( 0.0, noturno.getVida(), 1e-9 );
        assertEquals( Status.MORTO, noturno.getStatus() );
    }
}
