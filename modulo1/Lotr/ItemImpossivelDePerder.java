import java.util.*;

public class ItemImpossivelDePerder extends Item {
    public ItemImpossivelDePerder(int quantidade, String descricao) {
        super(quantidade, descricao);
    }
    
    public void setQuantidade(int quantidade) {
        boolean podeAlterar = quantidade > 0;
        this.quantidade = podeAlterar ? quantidade : 1;    
    }
    
}
