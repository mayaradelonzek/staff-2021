import java.util.*;

public class Inventario extends Object {       
    private ArrayList<Item> itens;
    
    public Inventario() {
        this.itens = new ArrayList<>();
    }
    
    public ArrayList<Item> getItens() {
        return this.itens;
    }
    
    
    public void setItens(ArrayList<Item> itens) {
        this.itens = itens;
    }
    
    public void adicionar(Item item) {
        this.itens.add(item);
    }
    
    private boolean validaSePosicao( int posicao) {
        return posicao >= this.itens.size();
    }
    
    public Item obter(int posicao) {
        
        return this.validaSePosicao(posicao) ? null : this.itens.get(posicao);
        
    }
    
    public void remover(int posicao) {
        if (!this.validaSePosicao(posicao)) {
            this.itens.remove(posicao);
        }    
    }
    
    public void remover(Item item) {        
        this.itens.remove(item);         
    }    
    
    public Item buscar (String descricao) {
        for (Item item : this.itens) {
            if (item.getDescricao().equals(descricao)) {
                return item;
            }
        }
        return null;
    }
    
    public String getDescricoes() {
        StringBuilder descricoes = new StringBuilder();
        for (int i = 0; i < this.itens.size(); i++) {
            Item item = this.itens.get(i);
            if (item != null) {
                String descricao = item.getDescricao();
                descricoes.append(descricao);
                
                boolean adicionarVirgula = i < this.itens.size() - 1; 
                if (adicionarVirgula) {
                    descricoes.append(",");
                }
            }
        }
        
        return descricoes.toString();         
    }
            
    
    public Item getItemComMaiorQuantidade() {
        int indice = 0;
        int maiorQuantidadeParcial = 0;
        for (int i = 0; i < this.itens.size(); i++) {
            if (this.itens.get(i) != null) {
                int qntAtual = this.itens.get(i).getQuantidade();
                if (qntAtual > maiorQuantidadeParcial) {
                    maiorQuantidadeParcial = qntAtual;
                    indice = i;
                }
            }
        }
        
        return this.itens.size() > 0 ? this.obter(indice) : null;       
    }
    
    public ArrayList<Item> inverter() {
        ArrayList<Item> listaInvertida = new ArrayList<>();
        
        for (int i = this.itens.size() - 1; i >= 0; i--) {
            listaInvertida.add(this.itens.get(i));
        }
        
        return listaInvertida;
    }
    
    public void ordernarItens() {
        Item aux;
        for(int i = 0; i < this.itens.size(); i++) {
            for(int j = 0; j < this.itens.size(); j++) {
                if(this.itens.get(i).getQuantidade() < this.itens.get(j).getQuantidade()) {
                    aux = this.itens.get(j);
                    this.itens.set(j, this.itens.get(i));
                    this.itens.set(i, aux);
                }
            }
        }
    }
    
    public void ordernarItens(OrdenacaoEnum ordenacao) {
        Item aux;       
        for(int i = 0; i < this.itens.size(); i++) {
            for(int j = 0; j < this.itens.size(); j++) {
                if(ordenacao.equals(OrdenacaoEnum.ASC) && this.itens.get(i).getQuantidade() < this.itens.get(j).getQuantidade()) {
                    aux = this.itens.get(j);
                    this.itens.set(j, this.itens.get(i));
                    this.itens.set(i, aux);
                } else if(ordenacao.equals(OrdenacaoEnum.DESC) && this.itens.get(i).getQuantidade() > this.itens.get(j).getQuantidade()) {
                	aux = this.itens.get(j);
                    this.itens.set(j, this.itens.get(i));
                    this.itens.set(i, aux);
                }
            }
        }
    }     
}

    
    
