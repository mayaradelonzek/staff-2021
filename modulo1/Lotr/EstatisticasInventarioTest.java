import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class EstatisticasInventarioTest {
    @Test
    public void calcularMediaInventarioVazio() {
        Inventario inventario = new Inventario();
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMedia();
        assertTrue(Double.isNaN(resultado));
    }
    
    @Test
    public void calcularMediaDeUmItem() {
        Inventario inventario = new Inventario();
        Item escudo = new Item(5, "Escudo");        
        inventario.adicionar(escudo);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMedia();
        assertEquals(5, resultado, 0.00001);
    }
    
    @Test
    public void calcularMediaItensComQuantidadesIguais() {
        Inventario inventario = new Inventario();
        Item escudo = new Item(1, "Escudo");  
        Item espada = new Item(1, "Espada");
        inventario.adicionar(escudo);
        inventario.adicionar(espada);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMedia();
        assertEquals(1, resultado, 0.00001);
        
    }
    
    @Test
    public void calcularMediaItensComQuantidadesDiferentes() {
        Inventario inventario = new Inventario();
        Item escudo = new Item(1, "Escudo");  
        Item espada = new Item(3, "Espada");
        inventario.adicionar(escudo);
        inventario.adicionar(espada);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMedia();
        assertEquals(2, resultado, 0.00001);
        
    }
    
    @Test
    public void calcularMedianaInventarioVazio() {
        Inventario inventario = new Inventario();
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMediana();
        assertTrue(Double.isNaN(resultado));
    }
    
    @Test
    public void calcularMedianaDeUmItem() {
        Inventario inventario = new Inventario();
        Item escudo = new Item(2, "Escudo");  
        inventario.adicionar(escudo);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMediana();
        assertEquals(2, resultado, 0.00001);
    }
    
    @Test 
    public void calcularMedianaQuantidadePar() {
        Inventario inventario = new Inventario();
        Item escudo = new Item(10, "Escudo"); 
        Item espada = new Item(20, "Espada");
        inventario.adicionar(escudo);
        inventario.adicionar(espada);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMediana();
        assertEquals(15, resultado, 0.00001);
    }
    
    @Test 
    public void calcularMedianaQuantidadeImpar() {
        Inventario inventario = new Inventario();
        Item escudo = new Item(10, "Escudo"); 
        Item espada = new Item(20, "Espada");
        Item lanca = new Item(30, "Lanca");
        inventario.adicionar(escudo);
        inventario.adicionar(espada);
        inventario.adicionar(lanca);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMediana();
        assertEquals(20, resultado, 0.00001);
    }
    
    @Test
    public void quantidadeItensAcimaDaMediaComUmItem() {
        Inventario inventario = new Inventario();
        Item escudo = new Item(5, "Escudo"); 
        inventario.adicionar(escudo);   
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.quantidadeItensAcimaMedia();
        assertEquals(0, resultado, 0.00001);
    }
    
    //TODO arrumar os testes abaixo
    /*@Test
    public void quantidadeItensAcimaDaMediaComVariosItens() {
        Inventario inventario = new Inventario();
        Item escudo = new Item(5, "Escudo"); 
        Item escudo2 = new Item(10, "Escudo"); 
        Item escudo3 = new Item(20, "Escudo"); 
        Item escudo4 = new Item(30, "Escudo"); 
        inventario.adicionar(escudo);   
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.quantidadeItensAcimaMedia();
        assertEquals(2, resultado, 0.00001);
    }
    
    @Test
    public void quantidadeItensAcimaDaMediaComItensIgualMedia() {
        Inventario inventario = new Inventario();   
        Item escudo = new Item(1, "Escudo"); 
        Item escudo2 = new Item(2, "Escudo"); 
        Item escudo3 = new Item(3, "Escudo"); 
        Item escudo4 = new Item(4, "Escudo"); 
        Item escudo5 = new Item(5, "Escudo"); 
        inventario.adicionar(escudo);   
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.quantidadeItensAcimaMedia();
        assertEquals(2, resultado, 0.00001);
    }*/
}
