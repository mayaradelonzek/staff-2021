import java.util.*;

public class ElfoVerde extends Elfo {  
    
    //array estatico usa final
    private final ArrayList<String> DESCRICOES_VALIDAS = new ArrayList<>(
        Arrays.asList(  
            "Espada de aço valiriano",
            "Arco de vidro",
            "Flecha de vidro"   
        )
    ); 
    
    public ElfoVerde(String nome) {
        super(nome);
        super.experienciaPorAtaque = 2;
    }   
    
    private boolean isDescricaoValida(String descricao) {
        return DESCRICOES_VALIDAS.contains(descricao);
    }
        
    @Override
    public void ganharItem(Item item) {
        if (isDescricaoValida(item.getDescricao())) {
            super.inventario.adicionar(item);
        }       
    }
    
    @Override
    public void perderItem(Item item) {
        if (isDescricaoValida(item.getDescricao())) {
            super.inventario.remover(item);
        }          
    }
}
