
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class DwarfBarbaLongaTest {
    @Test
    public void DwarfDevePerderVida66PorCento() {
        DadoFalso dado = new DadoFalso();
        dado.simularValor(4);
        DwarfBarbaLonga balin = new DwarfBarbaLonga("Balin", dado);
        balin.sofrerDano();
        assertEquals(100.0, balin.getVida(), 0.00005);
    }
    
     @Test
    public void DwarfDevePerderVida33PorCento() {
        DadoFalso dado = new DadoFalso();
        dado.simularValor(5);
        DwarfBarbaLonga balin = new DwarfBarbaLonga("Balin", dado);
        balin.sofrerDano();
        assertEquals(110.0, balin.getVida(), 0.00005);
    }
}
