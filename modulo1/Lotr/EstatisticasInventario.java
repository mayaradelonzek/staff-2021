public class EstatisticasInventario {
    private Inventario inventario;
    
    public EstatisticasInventario(Inventario inventario) {
        this.inventario = inventario;
    }
    
    private boolean EVazio() {
        return this.inventario.getItens().isEmpty();                   
    }
    
    public double calcularMedia() {   
        if (this.EVazio()) {
            return Double.NaN;
        }
        
        double somaQuantidades = 0;
        for (Item item : this.inventario.getItens()) {
            somaQuantidades += item.getQuantidade();
        }
        
        return somaQuantidades / inventario.getItens().size();
    }
    
    public double calcularMediana() {
        if (this.EVazio()) {
            return Double.NaN;
        }
        
        int quantidadeItens = this.inventario.getItens().size();
        int meio = quantidadeItens / 2;
        int quantidadeMeio = this.inventario.obter(meio).getQuantidade();
        boolean quantidadeImpar = quantidadeItens % 2 != 0;
        if (quantidadeImpar) {
            return quantidadeMeio;  
        }
        
        int quantidadeMeioMenosUm = this.inventario.obter(meio - 1).getQuantidade();
        return (quantidadeMeio + quantidadeMeioMenosUm ) / 2;
    }
    
    public int quantidadeItensAcimaMedia() {
        double media = this.calcularMedia();
        int quantidadeAcima = 0;
        
        for (Item item : this.inventario.getItens()) {
            if (item.getQuantidade() > media) {
                quantidadeAcima++;
            }
        }
        
        return quantidadeAcima;
    }
}