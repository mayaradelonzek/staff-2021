public class Anao extends Personagem {
    private boolean escudoEquipado;
    
    public Anao (String nome) {
        super(nome);
        this.vida = 110.0;
        this.inventario = new Inventario();
        this.inventario.adicionar(new Item(1, "Escudo"));
        this.escudoEquipado = false;
        super.qtdDano = 10;
    }      
            
    public void equiparEDesequiparEscudo() {        
        this.escudoEquipado = !this.escudoEquipado;
        super.qtdDano = this.escudoEquipado ? 5 : 10;
    }    
    
    public String imprimirNomeClasse() {
        return "Anao";
    }
}