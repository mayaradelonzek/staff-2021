public class ElfoNoturno extends Elfo {
    public ElfoNoturno(String nome) {
        super(nome);
        super.experienciaPorAtaque = 3;
        this.qtdDano = 15;
    }
}
