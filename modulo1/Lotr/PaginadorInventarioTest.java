import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.*;

public class PaginadorInventarioTest {
    
    @Test
    public void pular4limitar3ComTamanhoDaList5() {
        Inventario inventario = new Inventario();
        PaginadorInventario paginadorInventario = new PaginadorInventario(inventario);
        ArrayList<Item> resultadoEsperado = new ArrayList<>();
        Item adaga = new Item (1, "Adaga");
        Item item2 = new Item (2, "Item2");
        Item item3 = new Item (1, "Item3");
        Item item4 = new Item (3, "Item4");
        Item item5 = new Item (1, "Item5");
        
        inventario.adicionar(adaga);
        inventario.adicionar(item2);
        inventario.adicionar(item3);
        inventario.adicionar(item4);
        inventario.adicionar(item5);
        
        paginadorInventario.pular(4);  
        ArrayList<Item> resultadoDeFato = paginadorInventario.limitar(3); 
        
        resultadoEsperado.add(item5);
        
        assertEquals(resultadoEsperado, resultadoDeFato );
        
    }
    
    @Test
    public void pular0Limitar1ComTamanhoDaList1() {
        Inventario inventario = new Inventario();
        PaginadorInventario paginadorInventario = new PaginadorInventario(inventario);
        ArrayList<Item> resultadoEsperado = new ArrayList<>();
        Item adaga = new Item (1, "Adaga");   
        
        inventario.adicionar(adaga);       
        
        paginadorInventario.pular(0);  
        ArrayList<Item> resultadoDeFato = paginadorInventario.limitar(1); 
        
        resultadoEsperado.add(adaga);
        
        assertEquals(resultadoEsperado, resultadoDeFato );
        
    }
    
      @Test
    public void pular3limitar2ComTamanhoDaList5() {
        Inventario inventario = new Inventario();
        PaginadorInventario paginadorInventario = new PaginadorInventario(inventario);
        ArrayList<Item> resultadoEsperado = new ArrayList<>();
        Item adaga = new Item (1, "Adaga");
        Item item2 = new Item (2, "Item2");
        Item item3 = new Item (1, "Item3");
        Item item4 = new Item (3, "Item4");
        Item item5 = new Item (1, "Item5");
        
        inventario.adicionar(adaga);
        inventario.adicionar(item2);
        inventario.adicionar(item3);
        inventario.adicionar(item4);
        inventario.adicionar(item5);
        
        paginadorInventario.pular(3);  
        ArrayList<Item> resultadoDeFato = paginadorInventario.limitar(2); 
        
        resultadoEsperado.add(item4);
        resultadoEsperado.add(item5);        
        
        assertEquals(resultadoEsperado, resultadoDeFato );          
        
    }
}
