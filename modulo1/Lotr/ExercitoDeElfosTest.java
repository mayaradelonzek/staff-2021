
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;


public class ExercitoDeElfosTest {
    
        ExercitoDeElfos exercito;
	
	@BeforeEach
	public void init() {
        	exercito = new ExercitoDeElfos();
	}
	
	@Test
	public void testeExercitoAtaqueIntercalado() {
		ArrayList<Elfo> atacantes = new ArrayList<>();
		
		atacantes.add(new ElfoVerde("elfoVerde1"));
		atacantes.add(new ElfoVerde("elfoVerde2"));
		atacantes.add(new ElfoNoturno("elfoNoturno1"));
		atacantes.add(new ElfoNoturno("elfoNoturno2"));
		atacantes.add(new ElfoNoturno("elfoNoturno3"));
		atacantes.add(new ElfoVerde("elfoVerde3"));
		atacantes.add(new ElfoVerde("elfoVerde4"));
		
		ArrayList<Elfo> resultadoEsperado = new ArrayList<>();
		
		resultadoEsperado.add(new ElfoVerde("elfoVerde1"));
		resultadoEsperado.add(new ElfoNoturno("elfoNoturno1"));
		resultadoEsperado.add(new ElfoVerde("elfoVerde2"));
		resultadoEsperado.add(new ElfoNoturno("elfoNoturno2"));
		resultadoEsperado.add(new ElfoVerde("elfoVerde3"));
		resultadoEsperado.add(new ElfoNoturno("elfoNoturno3"));
		
		ArrayList<Elfo> resultadoObtido = exercito.getOrdemDeAtaque(atacantes, OrdemAtaqueEnum.ATAQUE_INTERCALADO);
		
		assertEquals(resultadoEsperado, resultadoObtido);
	}
	
	@Test
	public void testeExercitoAtaqueIntercaladoComElfoMorto() {
		ArrayList<Elfo> atacantes = new ArrayList<>();
		
		Elfo elfoMorto = new ElfoNoturno("elfoNoturno2");
		elfoMorto.status = Status.MORTO;
		
		atacantes.add(new ElfoVerde("elfoVerde1"));
		atacantes.add(new ElfoVerde("elfoVerde2"));
		atacantes.add(new ElfoNoturno("elfoNoturno1"));
		atacantes.add(elfoMorto);
		atacantes.add(new ElfoNoturno("elfoNoturno3"));
		atacantes.add(new ElfoVerde("elfoVerde3"));
		atacantes.add(new ElfoVerde("elfoVerde4"));
		
		ArrayList<Elfo> resultadoEsperado = new ArrayList<>();
		
		resultadoEsperado.add(new ElfoVerde("elfoVerde1"));
		resultadoEsperado.add(new ElfoNoturno("elfoNoturno1"));
		resultadoEsperado.add(new ElfoVerde("elfoVerde2"));
		resultadoEsperado.add(new ElfoNoturno("elfoNoturno3"));
		
		ArrayList<Elfo> resultadoObtido = exercito.getOrdemDeAtaque(atacantes, OrdemAtaqueEnum.ATAQUE_INTERCALADO);
		
		assertEquals(resultadoEsperado, resultadoObtido);
	}
	
	@Test
	public void testeExercitoAtaqueNoturnosPorUltimo() {
		ArrayList<Elfo> atacantes = new ArrayList<>();
		
		atacantes.add(new ElfoVerde("elfoVerde1"));
		atacantes.add(new ElfoVerde("elfoVerde2"));
		atacantes.add(new ElfoNoturno("elfoNoturno1"));
		atacantes.add(new ElfoNoturno("elfoNoturno2"));
		atacantes.add(new ElfoNoturno("elfoNoturno3"));
		atacantes.add(new ElfoVerde("elfoVerde3"));
		atacantes.add(new ElfoVerde("elfoVerde4"));
		
		ArrayList<Elfo> resultadoEsperado = new ArrayList<>();
		
		resultadoEsperado.add(new ElfoVerde("elfoVerde1"));
		resultadoEsperado.add(new ElfoVerde("elfoVerde2"));
		resultadoEsperado.add(new ElfoVerde("elfoVerde3"));
		resultadoEsperado.add(new ElfoVerde("elfoVerde4"));
		resultadoEsperado.add(new ElfoNoturno("elfoNoturno1"));
		resultadoEsperado.add(new ElfoNoturno("elfoNoturno2"));
		resultadoEsperado.add(new ElfoNoturno("elfoNoturno3"));
		
		ArrayList<Elfo> resultadoObtido = exercito.getOrdemDeAtaque(atacantes, OrdemAtaqueEnum.NOTURNOS_POR_ULTIMO);
		
		assertEquals(resultadoEsperado, resultadoObtido);
	}
	
	@Test
	public void testeExercitoAtaqueNoturnosPorUltimoComElfoMorto() {
		ArrayList<Elfo> atacantes = new ArrayList<>();
		
		Elfo elfoMorto = new ElfoNoturno("elfoNoturno2");
		elfoMorto.status = Status.MORTO;
		
		atacantes.add(new ElfoVerde("elfoVerde1"));
		atacantes.add(new ElfoVerde("elfoVerde2"));
		atacantes.add(new ElfoNoturno("elfoNoturno1"));
		atacantes.add(elfoMorto);
		atacantes.add(new ElfoNoturno("elfoNoturno3"));
		atacantes.add(new ElfoVerde("elfoVerde3"));
		atacantes.add(new ElfoVerde("elfoVerde4"));
		
		ArrayList<Elfo> resultadoEsperado = new ArrayList<>();
		
		resultadoEsperado.add(new ElfoVerde("elfoVerde1"));
		resultadoEsperado.add(new ElfoVerde("elfoVerde2"));
		resultadoEsperado.add(new ElfoVerde("elfoVerde3"));
		resultadoEsperado.add(new ElfoVerde("elfoVerde4"));
		resultadoEsperado.add(new ElfoNoturno("elfoNoturno1"));
		resultadoEsperado.add(new ElfoNoturno("elfoNoturno3"));
		
		ArrayList<Elfo> resultadoObtido = exercito.getOrdemDeAtaque(atacantes, OrdemAtaqueEnum.NOTURNOS_POR_ULTIMO);
		
		assertEquals(resultadoEsperado, resultadoObtido);
	}
	
	@Test
	public void testeExercitoEstrategiaCustomizada() {
		ArrayList<Elfo> atacantes = new ArrayList<>();
		
		Elfo elfoSemFlecha = new ElfoVerde("Verde1");
		elfoSemFlecha.getFlecha().setQuantidade(0);
		
		atacantes.add(new ElfoNoturno("Noturno1"));
		atacantes.add(new ElfoNoturno("Noturno2"));
		atacantes.add(elfoSemFlecha);
		atacantes.add(new ElfoVerde("Verde2"));
		atacantes.add(new ElfoVerde("Verde3"));
		
		ArrayList<Elfo> resultadoEsperado = new ArrayList<>();
		
		resultadoEsperado.add(new ElfoVerde("Verde2"));
		resultadoEsperado.add(new ElfoVerde("Verde3"));
		resultadoEsperado.add(new ElfoNoturno("Noturno1"));
		
		ArrayList<Elfo> resultadoObtido = exercito.getOrdemDeAtaque(atacantes, OrdemAtaqueEnum.ESTRATEGIA_CUSTOMIZADA);
		
		assertEquals(resultadoEsperado, resultadoObtido);
        }
}
