public class Item {
    protected int quantidade;
    private String descricao;
    
    public Item(int quantidade, String descricao) {
        this.setQuantidade(quantidade);
        this.descricao = descricao;
    }
    
    //get && set
    public int getQuantidade() {
        return this.quantidade;
    }
    
    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }
    
    public String getDescricao() {
        return this.descricao;
    }
    
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    
    @Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Item other = (Item) obj;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (quantidade != other.quantidade)
			return false;
		return true;
	}

    
    /*public void perderQuantidade(int valor) {
        this.quantidade -= valor; 
    } */
}
