

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class AnaoTest {
    
    @Test
    public void anaoDeveNascerCom110DeVida() {
        Anao anao = new Anao("Gimli");
        assertEquals(110, anao.getVida(), 0.00001);   
        assertEquals(Status.RECEM_CRIADO, anao.getStatus());
    }
    
    @Test
    public void anaoSofreDanoEFicaCom100DeVida() {
        Anao anao = new Anao("Gimli");
        anao.sofrerDano();
        assertEquals(100, anao.getVida(), 0.00001);     
        assertEquals(Status.SOFREU_DANO, anao.getStatus());
    }
    
    @Test
    public void anaoSofreDano12VezesEFicaCom0DeVida() {
        Anao anao = new Anao("Gimli");
        for (int i = 0; i < 12; i++) {
            anao.sofrerDano();
        }        
        assertEquals(0, anao.getVida(), 0.00001);
        assertEquals(Status.MORTO, anao.getStatus());
    }  
    
    @Test
    public void anaoNasceComUmEscudoNoInventario() {
        Anao anao = new Anao("Gimli");
        Item escudo = new Item(1, "Escudo");
        Item resultado = anao.getInventario().obter(0);
        assertEquals(escudo, resultado);        
    }
    
    @Test
    public void anaoPerde5DeVidaComEscudoEquipado() {
        Anao anao = new Anao("Gimli");
        anao.equiparEDesequiparEscudo();
        anao.sofrerDano();        
        assertEquals(105, anao.getVida());        
    }
    
    @Test
    public void anaoPerde10DeVidaSemEscudoEquipado() {
        Anao anao = new Anao("Gimli");        
        anao.sofrerDano();        
        assertEquals(100, anao.getVida());        
    }
}
