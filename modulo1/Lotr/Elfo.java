public class Elfo extends Personagem {    
    private int indiceFlecha;
    private static int qtdElfos = 0;
       
    public Elfo(String nome) { 
        super(nome);
        this.inventario = new Inventario();
        this.indiceFlecha = 0;
        this.vida = 100;        
        this.inventario.adicionar(new Item(2, "Flecha"));
        this.inventario.adicionar(new Item(1, "Arco"));
        Elfo.qtdElfos++;
    }
    
    public static int getQuantidade(){
        return Elfo.qtdElfos;
    }
    
    public void finalize() throws Throwable { //garbage coletor. para funcionar os testes
        Elfo.qtdElfos--;
    }
    
    public Item getFlecha() {
        return this.inventario.obter(indiceFlecha);
    }
    
    public int getQtdFlecha() {
        return this.getFlecha().getQuantidade();
    }    
      
    private boolean podeAtirar() {
        return this.getQtdFlecha() > 0;
    }
    
    public void atirarFlecha(Anao anao) {
        
        if(this.podeAtirar()) {
            int qntAtual = this.getQtdFlecha();
            this.getFlecha().setQuantidade(qntAtual - 1);
            this.aumentarXP();
            anao.sofrerDano();   
            super.sofrerDano();
        }
    }
    
    public void atirarMultiplasFlechas(int vezes, Anao anao) {
        for(int i = 0; i < vezes; i++) {
            this.atirarFlecha(anao);
        }
    }
    
    public String imprimirNomeClasse() {
        return "Elfo";
    }
    
    
    
    
    
    
    
    
    
    /* Padroes de projeto: solid e clean code */
    
}
