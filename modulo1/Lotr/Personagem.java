public abstract class Personagem {
    protected String nome;
    protected double vida;
    protected Status status;
    protected Inventario inventario;
    protected int experiencia;
    protected int experienciaPorAtaque;
    protected double qtdDano;
    
    public Personagem(String nome) {
        this.nome = nome;
        this.status = Status.RECEM_CRIADO;
        this.experiencia = 0;
        this.inventario = new Inventario();
        this.experienciaPorAtaque = 1;
        this.qtdDano = 0.0;
    }
            
    public String getNome() {
        return this.nome;
    }
    
    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public Status getStatus() {
        return this.status;
    }
    
    public Inventario getInventario() {
        return this.inventario;
    }
    
    public double getVida() {
        return this.vida;
    }
    
    protected void aumentarXP() {
        this.experiencia += experienciaPorAtaque;
    }
    
    public int getExperiencia() {
        return this.experiencia;    
    }
    
    private Status validacaoStatus() {
        return this.vida == 0 ? Status.MORTO : Status.SOFREU_DANO;
    }
    
    private boolean podeSofrerDano() {
        return this.vida > 0;
    }
    
    protected void sofrerDano() {        
        if (this.podeSofrerDano()) {
            this.vida = this.vida >= this.qtdDano ? this.vida - this.qtdDano : 0;
            this.status = this.validacaoStatus();
        }
    }
    
    public void ganharItem(Item item) {
        this.inventario.adicionar(item);
    }
    
    public void perderItem(Item item) {
        this.inventario.remover(item);
    }    
    
    @Override
    public boolean equals(Object obj) {
                if (this == obj)
                return true;
                if (obj == null)
                return false;
                if (getClass() != obj.getClass())
            return false;
        Personagem other = (Personagem) obj;
        if (experiencia != other.experiencia)
            return false;
        if (experienciaPorAtaque != other.experienciaPorAtaque)
            return false;
        if (nome == null) {
            if (other.nome != null)
                return false;
        } else if (!nome.equals(other.nome))
            return false;
        if (Double.doubleToLongBits(qtdDano) != Double.doubleToLongBits(other.qtdDano))
            return false;
        if (status != other.status)
            return false;
        if (Double.doubleToLongBits(vida) != Double.doubleToLongBits(other.vida))
            return false;
        return true;
    }
        
    public abstract String imprimirNomeClasse();
}
