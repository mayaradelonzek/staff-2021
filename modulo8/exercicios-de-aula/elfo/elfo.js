class Elfo {
  constructor(nome) {
    this._nome = nome; // nao é para ser usado diretamente. para usar ele é só com get. CONVENCAO    
  }

  get nome() {
    return this._nome;
  }

  matarElfo() {
    this.status = "morto";    
  }

  get estaMorto() {    
    return this.status = "morto";   
  }  

  atacarComFlecha() {
    setTimeout( () => {
      console.log("Atacou");      
    }, 2000);    
  }

}

let legolas = new Elfo("Legolas");
legolas.matarElfo();
legolas.estaMorto;
legolas.atacarComFlecha();

