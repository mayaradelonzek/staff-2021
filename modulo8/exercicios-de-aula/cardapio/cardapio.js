function cardapioIFood(veggie = true, comLactose = false) {
  let cardapio = [
    'enroladinho de salsicha',
    'cuca de uva'
  ];  

  if (comLactose) {
    cardapio.push('pastel de queijo');
  }  

  cardapio = [...cardapio,
    'pastel de carne',
    'empada de legumes marabijosa'
  ];

  if (veggie) {    
    cardapio.splice(cardapio.indexOf('enroladinho de salsicha'), 1);
    cardapio.splice(cardapio.indexOf('pastel de carne'), 1);
  }

  return cardapio.map(resultadoFinal => resultadoFinal.toUpperCase());  
}

console.log(cardapioIFood(true, true)); // esperado: [ 'CUCA DE UVA', 'PASTEL DE QUEIJO', 'EMPADA DE LEGUMES MARABIJOSA' ]
console.log(cardapioIFood(true, false)); // esperado: [ 'CUCA DE UVA', 'EMPADA DE LEGUMES MARABIJOSA' ]
console.log(cardapioIFood(false, true)); // esperado: [ 'ENROLADINHO DE SALSICHA', 'CUCA DE UVA' 'PASTEL DE QUEIJO', 'PASTEL DE CARNE', 'EMPADA DE LEGUMES MARABIJOSA' ]
console.log(cardapioIFood(false, false)); // esperado: [ 'ENROLADINHO DE SALSICHA', 'CUCA DE UVA', 'PASTEL DE CARNE', 'EMPADA DE LEGUMES MARABIJOSA' ]