class Jogador {
  constructor(nome, numero) {
    this._nome = nome;
    this._numero = numero;
  }

  get nome() {
    return this._nome;
  }

  get numero() {
    return this._numero;
  }

  set nome() {
    this._nome = nome;
  }

  set numero() {
    this._numero = numero;
  }
}

class Time {
  constructor(nome, tipoEsporte, status, liga) {
    this._nome = nome;
    this._tipoEsporte = tipoEsporte;
    this._status = "Ativo";
    this._liga = liga;
    this._jogadores = [];
  }

  adicionarJogador(jogador) {
    this._jogadores.push(jogador);
  }

  buscarJogadorPorNome(jogador) {
    return this._jogadores.find(jogador => jogador._nome == nome);
  }

  buscarJogadorPorNumero(numero) {
    return this._jogadores.find( jogador => jogador._numero == numero );
  }
}