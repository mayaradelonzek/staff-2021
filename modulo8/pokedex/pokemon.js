class Pokemon { // eslint-disable-line no-unused-vars
  constructor( objDaApi ) {
    this._nome = objDaApi.name;
    this._id = objDaApi.id;
    this._imagem = objDaApi.sprites.front_default;
    this._altura = objDaApi.height;
    this._peso = objDaApi.weight;
    this._tipos = objDaApi.types;
    this._estatisticas = objDaApi.stats;
  }

  get nome() {
    return this._nome;
  }

  get id() {
    return this._id;
  }

  get imagem() {
    return this._imagem;
  }

  get altura() {
    return `${ this._altura * 10 } cm`
  }

  get peso() {
    return `${ this._peso / 10 } kg`
  }

  get tipo() {
    return this._tipos.map( item => item.type.name );
  }

  get estatistica() {
    return this._estatisticas.map( item => [item.stat.name, item.base_stat] );
  }
}
