const pokeApi = new PokeApi();

/* eslint-disable no-undef */
renderizar = ( pokemon ) => { // eslint-disable-line no-unused-vars
  let dadosPokemon = document.getElementById( 'poke-container' );

  const id = dadosPokemon.querySelector( '.id' )
  id.innerHTML = pokemon.id;

  const nome = dadosPokemon.querySelector( '.nome' );
  nome.innerHTML = pokemon.nome.toUpperCase();

  const imagem = dadosPokemon.querySelector( '.thumb' );
  imagem.src = pokemon.imagem;

  const altura = dadosPokemon.querySelector( '.altura' );
  altura.innerHTML = pokemon.altura;

  const peso = dadosPokemon.querySelector( '.peso' );
  peso.innerHTML = pokemon.peso;

  const tipo = dadosPokemon.querySelector( '.tipo' );
  tipo.innerHTML = pokemon.tipo;

  dadosPokemon = document.getElementById( 'stat' );

  const stats = document.querySelector( '.estatisticas' );
  const statsData = pokemon.estatistica.map( item => `${ item[0] }:${ item[1] }` ).join( '\n' );
  console.log( statsData );
  stats.innerHTML = statsData;
}

/* eslint-disable no-undef */
const buscarPokemon = ( id, api ) => { // eslint-disable-line no-unused-vars
  if ( !( id >= 1 && id <= 893 ) ) {
    // eslint-disable-next-line no-alert
    alert( 'Digite um id válido' );
    return;
  }

  if ( id !== localStorage.getItem( 'atual' ) ) {
    api
      .buscarEspecifico( id )
      .then( pokemon => {
        const poke = new Pokemon( pokemon );

        localStorage.clear( 'atual' );
        localStorage.setItem( 'atual', pokemon.id );

        renderizar( poke );
      } );
  }
}

const buscarAleatorio = ( api ) => {
  const numeroAleatorio = Math.floor( Math.random() * 893 ) + 1;

  api
    .buscarEspecifico( numeroAleatorio )
    .then( pokemon => {
      localStorage.clear( 'atual' );
      localStorage.setItem( 'atual', pokemon.id );

      const poke = new Pokemon( pokemon );

      renderizar( poke );
    } );
}

window.onload = window.localStorage.clear();

const input = document.querySelector( 'input' );
const estouComSorte = document.querySelector( '.btn-aleatorio' );

input.addEventListener( 'blur', () => buscarPokemon( input.value, pokeApi ) );

estouComSorte.addEventListener( 'click', () => buscarAleatorio( pokeApi ) );
