import { Image } from 'antd';

function Imagem() {
  return (
    <Image
      width={35}
      preview={false}
      src="https://bitbucket.org/mayaradelonzek/staff-2021/raw/0ed2ca6d3d16d813a809ee8e31d7b04ddaf4f5b5/modulo8/react/trabalho-final-modulo-8/src/Assets/img/logo.png"
    />
  );
}

export default Imagem;