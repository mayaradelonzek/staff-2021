import api from '../../api'

export default class EspacoService { 

    constructor() {
        this.espacos = '/espacos'
    }

    buscarEspacos() {
        return api.get(this.espacos, {
            headers: {
                'Authorization': `Bearer ${JSON.parse(localStorage.getItem('token'))}`
            }
        })
            .then(e => {
                return e.data
            })
            .catch(err => {
                throw err
            })
    }

}