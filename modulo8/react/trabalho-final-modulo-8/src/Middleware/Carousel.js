import { Carousel, Col, Image, Row } from 'antd';

function CarouselSec() {

  const cw01 = '/img/cw01.jpg'
  const cw02 = '/img/cw02.jpg'
  const cw03 = '/img/cw03.jpg'

  const imagens = [cw01, cw02, cw03]

  return (
    <Row align={'middle'} justify={'center'}>
      <Col span={18}>
        <Image.PreviewGroup>
          <Carousel autoplay infinite>
            {imagens.map(
              (image, index) => {
                return <Image key={index} src={image} preview={false} />;
              }
            )}
          </Carousel>
        </Image.PreviewGroup>
      </Col>
    </Row>
  )
}

export default CarouselSec