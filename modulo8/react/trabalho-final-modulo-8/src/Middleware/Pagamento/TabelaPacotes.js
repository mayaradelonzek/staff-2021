import { Table, Space, Button, Select } from 'antd';
import { Option } from 'antd/lib/mentions';
import { useEffect, useState } from 'react';
import TiposPagamento from '../../Constants/tiposPagamento';

export default function TabelaPacotes(data) {

    const [formaPagamento, setFormaPagamento] = useState(undefined);
    const [arrayTiposPagamento, setArrayTiposPagamento] = useState([]);

    useEffect(() => {
        setArrayTiposPagamento(TiposPagamento)
      }, [])

    const expandedRowRender = (record) => {
        const columns = [
            { title: 'Espaco', dataIndex: 'espaco', key: 'espaco', align: 'center' },
            { title: 'Tipo', dataIndex: 'tipo', key: 'tipo', align: 'center' },
            { title: 'Quantidade Espaco', dataIndex: 'quantidadeEspaco', key: 'quantidadeEspaco', align: 'center' },
            { title: 'Quantidade Cliente', dataIndex: 'quantidadeCliente', key: 'quantidadeCliente', align: 'center' },
            { title: 'Prazo', dataIndex: 'prazo', key: 'prazo', align: 'center' }
        ];

        return <Table columns={columns} dataSource={record.nestedTable} pagination={false} />;
    };

    const columns = [
        { title: 'IdPacote', dataIndex: 'id', key: 'id', align: 'center' },
        { title: 'Valor', dataIndex: 'valor', key: 'valor', align: 'center' },
        {
            title: 'Forma de Pagamento',
            key: 'formaPagamento',
            align: 'center',
            render: (record) => (
                <Space size="middle">
                    <Select onSelect={(value) => setFormaPagamento(value)} placeholder="Selecione um tipo de pagamento">
                        {arrayTiposPagamento.map(tc => <Option key={tc} value={tc}>{tc}</Option>)}
                    </Select>
                </Space>
            ),
        },
        {
            title: 'Ações',
            key: 'action',
            align: 'center',
            render: (record) => (
                <Space size="middle">
                    <Button type="primary" onClick={() => data.onPagamento(record.idClientePacote, formaPagamento)} >Pagar</Button>
                </Space>
            ),
        }
    ];

    return (
        <Table
            className="components-table-demo-nested"
            columns={columns}
            expandable={{ expandedRowRender }}
            dataSource={data.info}
        />
    );
}