import { Table, Space, Button, Select } from 'antd';
import { Option } from 'antd/lib/mentions';
import { useEffect, useState } from 'react';
import TiposPagamento from '../../Constants/tiposPagamento';

export default function TabelaContratacoes(data) {

    const [formaPagamento, setFormaPagamento] = useState(undefined);
    const [arrayTiposPagamento, setArrayTiposPagamento] = useState([]);

    useEffect(() => {
        setArrayTiposPagamento(TiposPagamento)
      }, [])

    const columns = [
        { title: 'Espaco', dataIndex: 'espaco', key: 'espaco', align: 'center' },
        { title: 'Tipo', dataIndex: 'tipo', key: 'tipo', align: 'center' },
        { title: 'Quantidade', dataIndex: 'quantidade', key: 'quantidade', align: 'center' },
        { title: 'Desconto', dataIndex: 'desconto', key: 'desconto', align: 'center' },
        { title: 'Prazo', dataIndex: 'prazo', key: 'prazo', align: 'center' },
        {
            title: 'Forma de Pagamento',
            key: 'formaPagamento',
            align: 'center',
            render: (record) => (
                <Space size="middle">
                    <Select onSelect={(value) => setFormaPagamento(value)} placeholder="Selecione um tipo de pagamento">
                        {arrayTiposPagamento.map(tc => <Option key={tc} value={tc}>{tc}</Option>)}
                    </Select>
                </Space>
            ),
        },
        {
            title: 'Ações',
            key: 'action',
            align: 'center',
            render: (record) => (
                <Space size="middle">
                    <Button type="primary" onClick={() => data.onPagamento(record.id, formaPagamento)} >Pagar</Button>
                </Space>
            ),
        }
    ];

    return (
        <Table
            className="components-table-demo-nested"
            columns={columns}
            dataSource={data.info}
        />
    );
}