import api from '../../../api'

export default class PagamentoService {

    constructor() {
        this.pagamentos = '/pagamentos'
    }

    pagarPacote(idClientePacote, formaPagamento) {
        return api.post(this.pagamentos + '/', { clientePacoteEntity: { id: idClientePacote }, tipoPagamento: formaPagamento },
            {
                headers: {
                    'Authorization': `Bearer ${JSON.parse(localStorage.getItem('token'))}`
                }
            })
            .then(e => {
                console.log(e)
                return e.data
            })
            .catch(err => {
                console.log(err)
                throw err
            })
    }

    pagarContratacao(idContratacao, formaPagamento) {
        return api.post(this.pagamentos + '/', { contratacao: { id: idContratacao }, tipoPagamento: formaPagamento },
            {
                headers: {
                    'Authorization': `Bearer ${JSON.parse(localStorage.getItem('token'))}`
                }
            })
            .then(e => {
                console.log(e)
                return e.data
            })
            .catch(err => {
                console.log(err)
                throw err
            })
    }

}
