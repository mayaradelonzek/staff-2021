import { Form, Select, Divider, Row, Col, Spin, Radio } from 'antd';
import { useEffect, useState } from 'react';
import BreadcrumbSec from '../Breadcrumb'
import { Layout } from 'antd';
import ClienteService from '../Cliente/ClienteService';
import PacoteService from '../Pacote/Service/PacoteService';
import ContratacaoService from '../Contratacao/Service/ContratacaoService';
import TabelaPacotes from './TabelaPacotes';
import TabelaContratacoes from './TabelaContratacoes';
import PagamentoService from './Service/PagamentoService';

const { Option } = Select;

const cs = new ContratacaoService()
const ps = new PacoteService()
const clis = new ClienteService()
const pags = new PagamentoService()

function ContentPagamento() {
  const [form] = Form.useForm();

  const { Content } = Layout;

  const [arrayClientes, setArrayClientes] = useState([])

  const [spin, setSpin] = useState(false);

  const [loadingClientes, setLoadingClientes] = useState(false);

  const [clienteSelect, setClienteSelected] = useState(false);

  const [tipoSelected, setTipoSelected] = useState(false);

  const [listPacotes, setListPacotes] = useState([]);

  const [listContratacoes, setListContratacoes] = useState([]);

  function pagar(id, formaPagamento) {
    setSpin(true)

    if(tipoSelected === 'pacote') {
      pags.pagarPacote(id, formaPagamento)
      .then(resp => {
        setSpin(false)
        console.log(resp)
      })
      .catch(err => {
        setSpin(false)
        console.log(err)
      })
    } else {
      pags.pagarContratacao(id, formaPagamento)
      .then(resp => {
        setSpin(false)
        console.log(resp)
      })
      .catch(err => {
        setSpin(false)
        console.log(err)
      })
    }
  }

  function carregarTabela(values) {
    if (values.tipo === 'pacote') {

      ps.buscarPacotesPorCliente(values.cliente)
        .then(tableInfo => {

          const tabela = tableInfo.map((rowInfo, index) => {
            const pac = {}
            pac['key'] = index
            pac['id'] = rowInfo.id
            pac['valor'] = rowInfo.valor
            pac['idClientePacote'] = rowInfo.clientes[0].id

            const nestedQuantidadeClienteColumns = rowInfo.clientes.map(clientePacote => {
              return clientePacote.quantidade;
            })
            const nestedOtherColumns = rowInfo.espacos.map((espacotePacote, index) => {
              const columns = {}
              columns['key'] = index
              columns['espaco'] = espacotePacote.espacoEntity.nome
              columns['tipo'] = espacotePacote.tipoContratacao
              columns['quantidadeEspaco'] = espacotePacote.quantidade
              columns['prazo'] = espacotePacote.prazo

              return columns
            })

            nestedQuantidadeClienteColumns.forEach((qtdCliente, index) => {
              nestedOtherColumns[index]['quantidadeCliente'] = qtdCliente
            })

            pac['nestedTable'] = nestedOtherColumns;
            return pac;
          });
          setListPacotes(tabela)
        })

        setTipoSelected('pacote')
    } else {
      cs.buscarContratacoesPorCliente(values.cliente)
        .then(contratacoes => {
          console.log(contratacoes)
          const tabela = contratacoes.map((contratacao, index) => {
            const linha = {}
            linha['key'] = index
            linha['id'] = contratacao.id
            linha['espaco'] = contratacao.espacoEntity.nome
            linha['tipo'] = contratacao.tipoContratacao
            linha['quantidade'] = contratacao.quantidade
            linha['desconto'] = contratacao.desconto
            linha['prazo'] = contratacao.prazo

            return linha
          })

          setListContratacoes(tabela)
        })

      setTipoSelected('contratacao')
    }
  }

  function submitForm() {
    setListPacotes([])
    setListContratacoes([])
    form.submit()
  }

  function renderTipo() {
    return <Row gutter={24} justify={"center"}>
      <Col span={8}>
        <Form.Item name="tipo" label="Tipo" rules={[{ required: true }]}>
          <Radio.Group name="radiogroup" onChange={() => submitForm()}>
            <Radio value={"contratacao"}>Contratação</Radio>
            <Radio value={"pacote"}>Pacote</Radio>
          </Radio.Group>
        </Form.Item>
      </Col>
    </Row>
  }

  useEffect(() => {
    setLoadingClientes(true)
    clis.buscarClientes().then(clientes => {
      setLoadingClientes(false)
      setArrayClientes(clientes)
    });
  }, [])

  return (
    <Spin spinning={spin} delay={500}>
      <Content style={{ margin: '0 16px' }}>
        <BreadcrumbSec />
        <div className="site-layout-background" style={{ padding: 24, minHeight: 360 }}>
          <h3>Pagamento</h3>

          <Divider />

          <Form
            name="control-ref"
            onFinish={(value) => carregarTabela(value)}
            layout="vertical"
            form={form}>
            <Row gutter={24} justify={"center"}>
              <Col span={8}>
                <Form.Item name="cliente" label="Cliente" rules={[{ required: true }]}>
                  <Select loading={loadingClientes} onSelect={() => setClienteSelected(true)} placeholder="Selecione o cliente desejado">
                    {arrayClientes.map(e => <Option key={e.id} value={e.id}>{e.nome}</Option>)}
                  </Select>
                </Form.Item>
              </Col>
            </Row>

            {clienteSelect ? renderTipo() : ''}

            {tipoSelected === 'pacote' && clienteSelect ? <TabelaPacotes onPagamento={(idClientePacote, formaPagamento) => pagar(idClientePacote, formaPagamento)} info={listPacotes} /> : ''}
            {tipoSelected === 'contratacao' && clienteSelect ? <TabelaContratacoes onPagamento={(idContratacao, formaPagamento) => pagar(idContratacao, formaPagamento)} info={listContratacoes} /> : ''}

          </Form>
        </div>
      </Content>
    </Spin>
  );
}

export default ContentPagamento