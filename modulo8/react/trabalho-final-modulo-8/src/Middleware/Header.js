import { Button, Col, Layout, Row } from 'antd';
import { useContext } from 'react';
import { Context } from '../Context/AuthContext'

const { Header } = Layout;

function HeaderSec() {

    const { handleLogout, authenticated } = useContext(Context);
    if (!authenticated) return null;

    return (
        <Header className="site-layout-background">
            <Row justify={"end"}>
                <Col>
                    <Button type="primary" size="large" onClick={handleLogout}>Logout</Button>
                </Col>
            </Row>
        </Header>
    )

}

export default HeaderSec
