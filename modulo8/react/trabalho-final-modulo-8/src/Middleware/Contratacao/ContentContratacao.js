import { Form, Input, Button, Select, Divider, Row, Col, Spin } from 'antd';
import { useEffect, useState } from 'react';
import tiposContratacao from '../../Constants/tiposContratacao';
import ContratacaoService from './Service/ContratacaoService';
import NovaContratacaoDto from '../../DTOs/NovaContratacaoDto'
import BreadcrumbSec from '../Breadcrumb'
import { Layout } from 'antd';
import EspacoService from '../Espaco/EspacoService';
import ClienteService from '../Cliente/ClienteService';

const { Option } = Select;

const cs = new ContratacaoService()
const es = new EspacoService()
const clis = new ClienteService()

function ContentContratacao() {
  const [form] = Form.useForm();

  const { Content } = Layout;

  const [arrayTiposContratacao] = useState(tiposContratacao)

  const [arrayEspacos, setArrayEspacos] = useState([{ id: 1, nome: 'Carregando...' }])

  const [arrayClientes, setArrayClientes] = useState([{ id: 1, nome: 'Carregando...' }])

  const [spin, setSpin] = useState(false);

  function cadastrarContratacao(values) {
    const contratacao = new NovaContratacaoDto(values)

    setSpin(true)
    cs.cadastrarContratacao(contratacao)
      .then(res => {
        console.log('res:', res)
        setSpin(false)
      })
      .catch(err => {
        console.log('error: ', err)
        setSpin(false)
        throw err
      })
  }

  useEffect(() => {
    es.buscarEspacos().then(espacos => setArrayEspacos(espacos));
    clis.buscarClientes().then(clientes => setArrayClientes(clientes));
  }, [])

  return (
    <Spin spinning={spin} delay={500}>
      <Content style={{ margin: '0 16px' }}>
        <BreadcrumbSec />
        <div className="site-layout-background" style={{ padding: 24, minHeight: 360 }}>
          <h3>Contratação</h3>

          <Divider />

          <Form
            name="control-ref"
            onFinish={cadastrarContratacao}
            layout="vertical"
            form={form}>

            <Row gutter={24} justify={"center"}>
              <Col span={8}>
                <Form.Item name="espaco" label="Espaco" rules={[{ required: true }]}>
                  <Select placeholder="Selecione o espaço desejado">
                    {arrayEspacos.map(e => <Option key={e.id} value={e.id}>{e.nome}</Option>)}
                  </Select>
                </Form.Item>
              </Col>

              <Col span={8}>
                <Form.Item name="tipo" label="Tipo" rules={[{ required: true }]}>
                  <Select placeholder="Selecione um tipo de contratação">
                    {arrayTiposContratacao.map(tc => <Option key={tc} value={tc}>{tc}</Option>)}
                  </Select>
                </Form.Item>
              </Col>

            </Row>

            <Row gutter={24} justify={"center"}>
              <Col span={8}>
                <Form.Item name="cliente" label="Cliente" rules={[{ required: true }]}>
                  <Select placeholder="Selecione o cliente da contratação">
                    {arrayClientes.map(cli => <Option key={cli.id} value={cli.id}>{cli.nome}</Option>)}
                  </Select>
                </Form.Item>
              </Col>


              <Col span={8}>
                <Form.Item name="quantidade" label="Quantidade" rules={[{ required: true }]}>
                  <Input placeholder="Insira quantidade" />
                </Form.Item>
              </Col>
            </Row>

            <Row gutter={24}>
              <Col span={8} offset={4} >
                <Form.Item name="prazo" label="Prazo" rules={[{ required: true }]}>
                  <Input placeholder="Insira o prazo" />
                </Form.Item>
              </Col>
            </Row>

            <Row justify={"end"}>
              <Col>
                <Form.Item label=" ">
                  <Button type="primary" htmlType="submit" size="large">
                    Contratar
                  </Button>
                </Form.Item>
              </Col>
            </Row>

          </Form>
        </div>
      </Content>
    </Spin>

  );
}

export default ContentContratacao