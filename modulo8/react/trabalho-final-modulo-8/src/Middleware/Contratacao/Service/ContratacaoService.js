import TiposContratacao from '../../../Constants/tiposContratacao';
import TiposPagamento from '../../../Constants/tiposPagamento';

import api from '../../../api'

export default class ContratacaoService {

    constructor() {
        this.contratacoes = '/contratacoes'
    }

    cadastrarContratacao({ espacoEntity, clienteEntity, tipoContratacao, quantidade, prazo }) {
        return api.post(this.contratacoes + '/', { espacoEntity, clienteEntity, tipoContratacao, quantidade, prazo },
            {
                headers: {
                    'Authorization': `Bearer ${JSON.parse(localStorage.getItem('token'))}`
                }
            })
            .then(e => {
                console.log(e)
                return e.data
            })
            .catch(err => {
                console.log(err)
                throw err
            })
    }

    buscarContratacoesPorCliente(clienteId) {
        return api.get(this.contratacoes + '/cliente/' + clienteId,
            {
                headers: {
                    'Authorization': `Bearer ${JSON.parse(localStorage.getItem('token'))}`
                }
            }
        )
        .then(e => {
            console.log(e)
            return e.data
        })
        .catch(err => {
            console.log(err)
            throw err
        })
    }

}
