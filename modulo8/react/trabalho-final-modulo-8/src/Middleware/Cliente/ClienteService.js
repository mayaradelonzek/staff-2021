import api from '../../api'

export default class ClienteService { 

    constructor() {
        this.clientes = '/clientes'
    }

    buscarClientes() {
        return api.get(this.clientes + '/', {
            headers: {
                'Authorization': `Bearer ${JSON.parse(localStorage.getItem('token'))}`
            }
        })
            .then(e => {
                return e.data
            })
            .catch(err => {
                throw err
            })
    }

}