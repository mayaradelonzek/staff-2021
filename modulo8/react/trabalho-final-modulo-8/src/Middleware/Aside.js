import { Layout, Menu } from 'antd';
import { useContext, useState } from 'react';
import { Context } from '../Context/AuthContext'
import {
    ShoppingFilled,
    DollarOutlined,
    FormOutlined
  } from '@ant-design/icons';
import { Link } from 'react-router-dom';
import Imagem from './Image';

function AsideSec() {

    const { authenticated } = useContext(Context);

    const { Sider } = Layout;

    const [collapsed, setCollapsed] = useState(false);

    if (!authenticated) return null;

    return (
        <Sider collapsible collapsed={collapsed} onCollapse={setCollapsed}>
            <Link to='/home'>
                <div className="logo">
                    <Imagem></Imagem>
                    <span className="logo-name">Coworking</span>
                </div>
            </Link>
            <Menu theme="dark" mode="inline">
                <Menu.Item key="1" icon={<FormOutlined />}>
                    <Link to='/nova/contratacao'>Contratações</Link>
                </Menu.Item>
                <Menu.Item key="2" icon={<ShoppingFilled />}>
                    <Link to='/novo/pacote'>Pacotes</Link>
                </Menu.Item>
                <Menu.Item key="3"  icon={<DollarOutlined />}>
                    <Link to='/novo/pagamento'>Pagamentos</Link>
                </Menu.Item>
            </Menu>
        </Sider>
    )

}

export default AsideSec
