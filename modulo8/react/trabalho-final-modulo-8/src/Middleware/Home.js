import BreadcrumbSec from '../Middleware/Breadcrumb'
import { Divider, Layout } from 'antd';
import CarouselSec from './Carousel'

function Home() {
    const { Content } = Layout;

    return (
        <Content style={{ margin: '0 16px' }}>
            <BreadcrumbSec />
            <div className="site-layout-background" style={{ padding: 24, minHeight: 360 }}>
                <h3>Conheça os nossos espaços</h3>
                <Divider />
                <CarouselSec />
            </div>
        </Content>
    )
}

export default Home
