import { Breadcrumb } from 'antd';
import { useContext, useState } from 'react';
import { Context } from '../Context/AuthContext'
import history from '../history'

function BreadcrumbSec() {

    const { authenticated } = useContext(Context);
    const [ path ] = useState(history.location.pathname.split('/').filter(path => path !== ''));

    if (!authenticated) return null;

    return (
        <Breadcrumb style={{ margin: '16px 0' }}>
            {path.map(p => <Breadcrumb.Item key={p}>{p.charAt(0).toUpperCase() + p.slice(1)}</Breadcrumb.Item>)}
        </Breadcrumb>
    )

}

export default BreadcrumbSec
