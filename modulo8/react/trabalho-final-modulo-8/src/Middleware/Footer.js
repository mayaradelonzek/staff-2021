import { Layout } from 'antd';

const { Footer } = Layout;

function FooterSec() {

    return (
        <Footer style={{ textAlign: 'center' }}>May Design ©2021 Created by Mayara Delonzek</Footer>
    )

}

export default FooterSec
