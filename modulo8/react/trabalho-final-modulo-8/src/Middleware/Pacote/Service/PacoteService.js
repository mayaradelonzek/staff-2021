import api from '../../../api'
import EspacoService from '../../Espaco/EspacoService'

export default class PacoteService {

    constructor() {
        this.pacotes = '/pacotes'
    }

    cadastrarPacote(arrayPacotes) {
        return api.post(this.pacotes + '/', arrayPacotes,
            {
                headers: { 'Authorization': `Bearer ${JSON.parse(localStorage.getItem('token'))}` }
            })
            .then(e => {
                return e.data
            })
            .catch(err => {
                throw err
            })
    }

    buscarPacotesPorCliente(clienteId) {
        return api.get(this.pacotes + '/cliente/' + clienteId, {
            headers: { 'Authorization': `Bearer ${JSON.parse(localStorage.getItem('token'))}` }
        })
        .then(e => {
            return e.data
        })
        .catch(err => {
            throw err
        })
    }

}
