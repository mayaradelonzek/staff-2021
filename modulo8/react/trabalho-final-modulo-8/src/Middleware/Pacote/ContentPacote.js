import { Form, Input, Button, Select, Divider, Row, Col, Spin } from 'antd';
import { useEffect, useState } from 'react';
import PacoteService from './Service/PacoteService.js';
import BreadcrumbSec from '../Breadcrumb'
import { Layout } from 'antd';
import TiposContratacao from '../../Constants/tiposContratacao';
import EspacoService from '../Espaco/EspacoService.js';
import ClienteService from '../Cliente/ClienteService.js';
import TabelaPacotesAdicionados from './TabelaPacotesAdicionados.js';
import axios from 'axios';
import NovoPacoteDto from '../../DTOs/NovoPacoteDto'

const { Option } = Select;

const es = new EspacoService()
const clis = new ClienteService()
const ps = new PacoteService()

function ContentPacote() {
  const [form] = Form.useForm();

  const { Content } = Layout;

  const [arrayTiposPacote] = useState(TiposContratacao)

  const [arrayEspacos, setArrayEspacos] = useState([{ id: 1, nome: 'Carregando...' }])

  const [arrayClientes, setArrayClientes] = useState([{ id: 1, nome: 'Carregando...' }])

  const [arrayPacotes, setArrayPacotes] = useState([])

  const [spin, setSpin] = useState(false);

  function adicionarPacote(values) {
    const isEspacoJaRegistrado = arrayPacotes.some(c => {
      const espaco = arrayEspacos.filter(ae => ae.id === values.espaco);
      return espaco[0].nome === c.espaco
    })

    if (isEspacoJaRegistrado) return;

    const [cliente] = arrayClientes.filter(c => c.id === values.cliente)
    const [espaco] = arrayEspacos.filter(e => e.id === values.espaco)

    values['cliente'] = cliente
    values['espaco'] = espaco

    const newArray = [...arrayPacotes]
    newArray.push(values)

    console.log(newArray)

    setArrayPacotes(newArray)
  }

  function setarArrayPacotes(espaco) {
    setArrayPacotes(arrayPacotes.filter(t => t.name !== espaco.name))
  }

  function contratarPacote() {
    const pacotes = arrayPacotes.map(p => {
      p['espaco'] = arrayEspacos.filter(e => e.nome === p.espaco)[0].id;
      p['cliente'] = arrayClientes.filter(c => c.nome === p.cliente)[0].id;
      delete p.key
      console.log(p)
      return new NovoPacoteDto(p);
    })

    setSpin(true)
    ps.cadastrarPacote(pacotes)
      .then(resp => {
        setSpin(false)
        setArrayPacotes([])
      })
      .catch(err => { 
        setSpin(false)
        throw err 
      })
  }

  useEffect(() => {
    axios.all([es.buscarEspacos(), clis.buscarClientes()])
      .then(resp => {
        setArrayEspacos(resp[0])
        setArrayClientes(resp[1])
      })
      .catch(err => {
        throw new Error('Erro ao carregar os selects')
      })
  }, [])

  return (
    <Spin spinning={spin} delay={500}>
      <Content style={{ margin: '0 16px' }}>
        <BreadcrumbSec />
        <div className="site-layout-background" style={{ padding: 24, minHeight: 360 }}>

          <Row justify={"space-between"}>
            <Col>
              <h3>Pacotes</h3>
            </Col>
            <Col>
              <Button type="primary" size="large" onClick={contratarPacote} >
                Contratar Pacote
              </Button>
            </Col>
          </Row>

          <Divider />

          <Form
            name="control-ref"
            onFinish={adicionarPacote}
            layout="vertical"
            form={form}>

            <Row gutter={24} justify={"center"}>
              <Col span={8}>
                <Form.Item name="espaco" label="Espaco" rules={[{ required: true }]}>
                  <Select placeholder="Selecione o espaço desejado">
                    {arrayEspacos.map(e => <Option key={e.id} value={e.id}>{e.nome}</Option>)}
                  </Select>
                </Form.Item>
              </Col>

              <Col span={8}>
                <Form.Item name="tipo" label="Tipo" rules={[{ required: true }]}>
                  <Select placeholder="Selecione um tipo de contratação">
                    {arrayTiposPacote.map(tc => <Option key={tc} value={tc}>{tc}</Option>)}
                  </Select>
                </Form.Item>
              </Col>
            </Row>

            <Row gutter={24} justify={"center"}>
              <Col span={8}>
                <Form.Item name="cliente" label="Cliente" rules={[{ required: true }]}>
                  <Select placeholder="Selecione o cliente da contratação">
                    {arrayClientes.map(cli => <Option key={cli.id} value={cli.id}>{cli.nome}</Option>)}
                  </Select>
                </Form.Item>
              </Col>

              <Col span={8}>
                <Form.Item name="quantidadeEspaco" label="Quantidade espaco" rules={[{ required: true }]}>
                  <Input placeholder="Quantidade espaco pacote" />
                </Form.Item>
              </Col>
            </Row>

            <Row gutter={24} justify={"center"} >
              <Col span={8} >
                <Form.Item name="quantidadeCliente" label="Quantidade cliente" rules={[{ required: true }]}>
                  <Input placeholder="Quantidade cliente pacote" />
                </Form.Item>
              </Col>
              <Col span={8} >
                <Form.Item name="prazo" label="Prazo" rules={[{ required: true }]}>
                  <Input placeholder="Insira o prazo" />
                </Form.Item>
              </Col>
            </Row>

            <Row justify={"center"}>
              <Col span={8}>
                <Form.Item label=" ">
                  <Button type="primary" htmlType="submit" size="large" block>
                    Adicionar espaço ao pacote
                  </Button>
                </Form.Item>
              </Col>
            </Row>
          </Form>

          <TabelaPacotesAdicionados onRemoveEspaco={setarArrayPacotes} pacoteList={arrayPacotes} />
        </div>
      </Content>
    </Spin>
  );
}

export default ContentPacote;