import { Table, Space, Button } from 'antd';
import { useEffect, useState } from 'react';

function TabelaPacotesAdicionados(data) {

  const columns = [
    {
      title: 'Espaco',
      dataIndex: 'espaco',
      key: 'espaco',
    },
    {
      title: 'Tipo',
      dataIndex: 'tipo',
      key: 'tipo',
    },
    {
      title: 'Cliente',
      dataIndex: 'cliente',
      key: 'cliente',
    },
    {
      title: 'Quantidade Espaco',
      dataIndex: 'quantidadeEspaco',
      key: 'quantidadeEspaco',
    },
    {
      title: 'Quantidade Cliente',
      dataIndex: 'quantidadeCliente',
      key: 'quantidadeCliente',
    },
    {
      title: 'Prazo',
      dataIndex: 'prazo',
      key: 'prazo',
    },
    {
      title: ' ',
      key: 'action',
      render: (record) => (
        <Space size="middle">
          <Button type="primary" onClick={() => data.onRemoveEspaco(record)} >Deletar</Button>
        </Space>
      ),
    }
  ];

  function formatarDados(pacotes) {

    const teste = pacotes.map(p => {
      p['key'] = p.espaco.id ? p.espaco.id : p['key']
      p['cliente'] = p.cliente.nome ? p.cliente.nome : p['cliente']
      p['espaco'] = p.espaco.nome ? p.espaco.nome : p['espaco']
      return p
    })

    return teste
  }

  const [tableContent, setTableContent] = useState([])

  useEffect(() => {
    setTableContent(formatarDados(data.pacoteList))
  }, [data])

  return (
    <Table columns={columns} dataSource={tableContent} />
  )

}

export default TabelaPacotesAdicionados



