import { Layout } from 'antd';

import { Context } from '../Context/AuthContext'

import FooterSec from '../Middleware/Footer';

import HeaderSec from '../Middleware/Header';
import ContentSec from './Content';
import AsideSec from './Aside';
import { useContext } from 'react';

function LayoutSec() {
  
  const { authenticated } = useContext(Context);
  if(!authenticated) return null;

  return (
    <Layout style={{ minHeight: '100vh' }}>
        <AsideSec />
      <Layout className="site-layout">
        <HeaderSec />
        <FooterSec />
      </Layout>
    </Layout>
  );
}

export default LayoutSec