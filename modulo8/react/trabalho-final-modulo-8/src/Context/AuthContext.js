import React, { createContext, useEffect, useState } from 'react';
import LoginService from '../Container/Login/LoginService'
import history from '../history'
import api from '../api'


const Context = createContext();

function AuthProvider({ children }) {
    const [authenticated, setAuthenticated] = useState(false);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        const token = localStorage.getItem('token')

        if (token) {
            api.defaults.headers.Authorization = `Bearer ${JSON.parse(token)}`
            setAuthenticated(true);
            setLoading(true);
        }
        setLoading(false);
    }, []);

    function handleLogin({ login, senha }) {
        const loginService = new LoginService()
        loginService.buscarToken(login, senha)
            .then(res => {
                const token = res.headers.authorization
                localStorage.setItem('token', JSON.stringify(token))
                api.defaults.headers.Authorization = `Bearer ${token}`
                setAuthenticated(true)
                history.push('/home')
            })
            .catch(err => {
                console.log('Erro: ', err)
                return err
            })
    }

    function handleLogout() {
        setAuthenticated(false)
        localStorage.removeItem('token')
        api.defaults.headers.Authorization = undefined
        history.push('/')
    }

    return (
        <Context.Provider value={{ authenticated, handleLogin, handleLogout, loading }}>
            {children}
        </Context.Provider>
    );
}

export { Context, AuthProvider };