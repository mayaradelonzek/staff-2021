import React, { useContext } from 'react';

import { Form, Input, Button, Row, Col } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import { Context } from '../../Context/AuthContext';

import './Login.css';

function Login() {

  const { handleLogin } = useContext(Context);

  const [form] = Form.useForm();

  return(
    <Row type='flex' align='middle' justify='center' >
    <Col span={7}>
      <Form
        form={form}
        onFinish={handleLogin}
        className="login-container"
        initialValues={{ remember: true }}
      >
        <h1 className="login-title" type='flex' align='middle' justify='center'><strong>Login</strong></h1>

        <Row type='flex' align='middle' justify='center' >
          <Col span={15}>
            <Form.Item
              name="login"
              rules={[{ required: true, message: 'Informe o seu login!' }]}
            >
              <Input name="login" placeholder="Nome de usuário" prefix={<UserOutlined />} />
            </Form.Item>
          </Col>
        </Row>
        <Row type='flex' align='middle' justify='center' >
          <Col span={15}>
            <Form.Item
              name="senha"
              rules={[{ required: true, message: 'Informe a sua senha!' }]}
            >
              <Input.Password name="senha" placeholder="Senha" />
            </Form.Item>
          </Col>
        </Row>
        <Row type='flex' align='middle' justify='center' >
          <Col >
            <Form.Item>
                <Button type="primary" htmlType="submit" danger >
                  LOGIN
                </Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Col>
  </Row >
  )

}

export default Login