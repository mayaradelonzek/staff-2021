import { Breadcrumb } from "antd";
import React from "react";
import Login from "./Login";

export default class LoginPai extends React.Component {

    render() {
        return (
            <React.Fragment>
                <Breadcrumb style={{ margin: '15vh 0' }} />
                <Login />
            </React.Fragment>
        )
    }

}