import api from '../../api'

const loginPath = '/login'

export default class LoginService {

    buscarToken(login, senha) {
        return api.post(loginPath, {login: login, senha: senha})
    }


}