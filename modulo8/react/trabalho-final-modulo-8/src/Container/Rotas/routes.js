import React, { useContext } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { Context } from '../../Context/AuthContext'
import LoginPai from '../Login/LoginPai';
import HeaderSec from '../../Middleware/Header';
import AsideSec from '../../Middleware/Aside';
import FooterSec from '../../Middleware/Footer';
import Home from '../../Middleware/Home'
import '../../Components/Home/App.css';
import ContentContratacao from '../../Middleware/Contratacao/ContentContratacao';
import { Layout } from 'antd';
import ContentPacote from '../../Middleware/Pacote/ContentPacote';
import ContentPagamento from '../../Middleware/Pagamento/ContentPagamento';

function CustomRoute({ isPrivate, ...rest }) {

  const { authenticated, loading } = useContext(Context);

  if (loading) {
    return <h1>Loading...</h1>
  }

  if (isPrivate && !authenticated) {
    return <Redirect to="/" />
  }

  if (authenticated && rest.path === '/') {
    return <Redirect to="/home" />
  }

  return <Route {...rest} />
}

export default function Routes() {

  return (
    <React.Fragment>
      <Layout style={{ minHeight: '100vh' }}>

        <AsideSec />

        <Layout className="site-layout">

          <HeaderSec />

          <Switch>
            <CustomRoute exact path="/" component={LoginPai} />
            <CustomRoute isPrivate exact path="/home" component={Home} />
            <CustomRoute isPrivate exact path="/nova/contratacao" component={ContentContratacao} />
            <CustomRoute isPrivate exact path="/novo/pacote" component={ContentPacote} />
            <CustomRoute isPrivate exact path="/novo/pagamento" component={ContentPagamento} />
          </Switch>

          <FooterSec />

        </Layout>
      </Layout>
    </React.Fragment>
  );
}
