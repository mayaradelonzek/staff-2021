export default class NovoPacoteDto {

    constructor({ espaco, cliente, prazo, quantidadeCliente, quantidadeEspaco, tipo }) {
        this.espacoId = espaco
        this.clienteId = cliente
        this.quantidadeClientePacote = quantidadeCliente
        this.quantidadeEspacoPacote = quantidadeEspaco
        this.prazoEspacoPacote = prazo
        this.tipoContratacao = tipo.toUpperCase()
        console.log(this)
        this.validarAtributos()
    }

    validarAtributos() {
        const areAtributosUndefined = !(this.espacoId && this.clienteId && this.quantidadeClientePacote
            && this.quantidadeEspacoPacote && this.prazoEspacoPacote && this.tipoContratacao)

        if (areAtributosUndefined) {
            throw new Error('O pacote contém valores inválidos!')
        }
    }
}
