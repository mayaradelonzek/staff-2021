export default class NovaContratacaoDto {

    constructor({ espaco, cliente, tipo, quantidade, prazo }) {
        this.espacoEntity = { id: espaco }
        this.clienteEntity = { id: cliente }
        this.tipoContratacao = tipo.toUpperCase()
        this.quantidade = quantidade
        this.prazo = prazo
        console.log(this)
        this.validarAtributos()
    }

    validarAtributos() {
        const areAtributosUndefined = !(this.espacoEntity && this.clienteEntity && this.tipoContratacao
            && this.quantidade && this.prazo)
        const isEspacoEntityNotValid = !(this.espacoEntity && this.espacoEntity.id)
        const isClienteEntityNotValid = !(this.clienteEntity && this.clienteEntity.id)

        if (areAtributosUndefined || isEspacoEntityNotValid || isClienteEntityNotValid) {
            throw new Error('A contratação contém valores inválidos!')
        }
    }
}
