import React from 'react';
import { AuthProvider } from '../../Context/AuthContext';
import { Router } from 'react-router-dom';
import Routes from '../../Container/Rotas/routes';
import history from '../../history';


function App() {
  return (
    <AuthProvider>
      <Router history={history}>
        <Routes />
      </Router>
    </AuthProvider>
  )
}

export default App

