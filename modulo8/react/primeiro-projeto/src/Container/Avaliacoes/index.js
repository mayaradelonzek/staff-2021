import React from 'react';
import { Link } from 'react-router-dom';

import BotaoUi from '../../Components/BotaoUi';
import ListaEpisodios from '../../Components/ListaEpisodios';

const ListaAvaliacoes = props => {
  const { listaEpisodios } = props.location.state;

  return (
    <React.Fragment>
      <div className="pagina-avaliacoes">
        <BotaoUi classe="verde" link="/" nome="Página Inicial" />
        <ListaEpisodios listaEpisodios={listaEpisodios.avaliados} />
      </div>
    </React.Fragment>
  );
}

export default ListaAvaliacoes;