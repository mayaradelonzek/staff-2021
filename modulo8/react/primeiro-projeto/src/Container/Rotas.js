import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import App from './App';
import ListaAvaliacoes from './Avaliacoes';
import DetalhesEpisodios from './DetalhesEpisodios';
import TodosEpisodios from './TodosEpisodios';

export default class Rotas extends Component {
  render(){
    return (
      <Router>
        <Route path="/" exact component={ App } />
        <Route path="/avaliacoes" exact component={ ListaAvaliacoes } />
        <Route path="/episodios" exact component={ TodosEpisodios } />
        <Route path="/detalhes/:id" component={ DetalhesEpisodios } />
      </Router>
    );
  }
}