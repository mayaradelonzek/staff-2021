/** Exercicio 01 */
console.log("Exercício 01");

function calcularCirculo(raioCircunferencia, tipoCalculo) {
  const parametros = {
    raio: raioCircunferencia,
    tipo: tipoCalculo
  };

  if (tipoCalculo === "A") {
    return Math.PI * raioCircunferencia * raioCircunferencia;
  }

  if (tipoCalculo === "C") {
    return 2 * Math.PI * raioCircunferencia;
  }

}

console.log("Resultado: " + calcularCirculo(3, "A"))

/** Exercicio 01 Corrigido */
console.log("Exercício 01 Corrigido");
let circulo = {
  raio: 3,
  tipoCalculo: "A"
};

function calcularCirculoCorrigido({raio, tipoCalculo:tipo}) {
  return Math.ceil(tipo == "A" ? Math.PI * Math.pow(raio, 2) : 2 * Math.PI * raio);
}

console.log(calcularCirculoCorrigido(circulo));

/** Exercicio 02 */
console.log("Exercício 02");

function naoBissexto(numero) {
  let isLeap = {};

  if ((numero % 4 === 0) && ((numero % 100 !== 0) || (numero % 400 === 0))) {
    isLeap['isLeap'] = true;
  } else {
    isLeap['isLeap'] = false;
  }
  return isLeap;
}

console.log(naoBissexto(2016));
console.log(naoBissexto(2017));

/** Exercicio 02 Corrigido */
console.log("Exercício 02 Corrigido");

function bissextoCorrigido(ano) {
  return (ano % 400 == 0) || (ano % 4 == 0 && ano % 100 != 0) ? true : false;
}

console.log(bissextoCorrigido(2016));
console.log(bissextoCorrigido(2017));

/** Exercicio 03 */
console.log("Exercício 03");

let numeros = [1, 56, 4.34, 6, -2];

function somarPares(numeros) {
  let somatoria = 0;

  for (let i = 0; i < numeros.length; i++) {
    if (numeros[i] % 2 === 0) {
      somatoria += numeros[i];
    }
  }
  return somatoria;
}

console.log(somarPares(numeros));

/** Exercicio 03 Corrigido*/
console.log("Exercício 03 Corrigido");
function somarParesCorrigido(numeros) {
  let resultado = 0;
  for (let i = 0; i < numeros.length; i+=2) {   
    resultado += numeros[i];           
  }
  return resultado;
}

console.log(somarParesCorrigido([1, 56, 4.34, 6, -2]));


/** Exercicio 04 */
console.log("Exercício 04");

function adicionar(numero1) {
  return function (numero2) {
    return numero1 + numero2;
  }
}

console.log(adicionar(3)(4));

/** Exercicio 04 Corrigido */
console.log("Exercício 04 Corrigido");
let adicionarCorrigido = valor1 => valor2 => valor1 + valor2;
console.log(adicionarCorrigido(3)(4));

/** Exercicio 05 */
// Corrigido no arquivo moedas.js



/** Exercicio Extra */
//currying
//2 -> 4, 6, 8, 10

/* let divisivel = (divisor, numero) => (numero / divisor);
const divisor = 2;
console.log(divisivel(divisor, 4));
console.log(divisivel(divisor, 6));
console.log(divisivel(divisor, 8));
console.log(divisivel(divisor, 10)); */

const divisao = divisor => numero => (numero / divisor);
const divisivelPor = divisao(2);

console.log(divisivelPor(4));
console.log(divisivelPor(6));
console.log(divisivelPor(8));
console.log(divisivelPor(10));



