//Lista 7 só com js puro, sem usar proptypes ainda

class ListaSeries {
  constructor() {

    this.series = JSON.parse(`[
      {
        "titulo": "Stranger Things",
        "anoEstreia": 2016,
        "diretor": [
          "Matt Duffer",
          "Ross Duffer"
        ],
        "genero": [
          "Suspense",
          "Ficcao Cientifica",
          "Drama"
        ],
        "elenco": [
          "Winona Ryder",
          "David Harbour",
          "Finn Wolfhard",
          "Millie Bobby Brown",
          "Gaten Matarazzo",
          "Caleb McLaughlin",
          "Natalia Dyer",
          "Charlie Heaton",
          "Cara Buono",
          "Matthew Modine",
          "Noah Schnapp"
        ],
        "temporadas": 2,
        "numeroEpisodios": 17,
        "distribuidora": "Netflix"
      },
      {
        "titulo": "Game Of Thrones",
        "anoEstreia": 2011,
        "diretor": [
          "David Benioff",
          "D. B. Weiss",
          "Carolyn Strauss",
          "Frank Doelger",
          "Bernadette Caulfield",
          "George R. R. Martin"
        ],
        "genero": [
          "Fantasia",
          "Drama"
        ],
        "elenco": [
          "Peter Dinklage",
          "Nikolaj Coster-Waldau",
          "Lena Headey",
          "Emilia Clarke",
          "Kit Harington",
          "Aidan Gillen",
          "Iain Glen ",
          "Sophie Turner",
          "Maisie Williams",
          "Alfie Allen",
          "Isaac Hempstead Wright"
        ],
        "temporadas": 7,
        "numeroEpisodios": 67,
        "distribuidora": "HBO"
      },
      {
        "titulo": "The Walking Dead",
        "anoEstreia": 2010,
        "diretor": [
          "Jolly Dale",
          "Caleb Womble",
          "Paul Gadd",
          "Heather Bellson"
        ],
        "genero": [
          "Terror",
          "Suspense",
          "Apocalipse Zumbi"
        ],
        "elenco": [
          "Andrew Lincoln",
          "Jon Bernthal",
          "Sarah Wayne Callies",
          "Laurie Holden",
          "Jeffrey DeMunn",
          "Steven Yeun",
          "Chandler Riggs ",
          "Norman Reedus",
          "Lauren Cohan",
          "Danai Gurira",
          "Michael Rooker ",
          "David Morrissey"
        ],
        "temporadas": 9,
        "numeroEpisodios": 122,
        "distribuidora": "AMC"
      },
      {
        "titulo": "Band of Brothers",
        "anoEstreia": 20001,
        "diretor": [
          "Steven Spielberg",
          "Tom Hanks",
          "Preston Smith",
          "Erik Jendresen",
          "Stephen E. Ambrose"
        ],
        "genero": [
          "Guerra"
        ],
        "elenco": [
          "Damian Lewis",
          "Donnie Wahlberg",
          "Ron Livingston",
          "Matthew Settle",
          "Neal McDonough"
        ],
        "temporadas": 1,
        "numeroEpisodios": 10,
        "distribuidora": "HBO"
      },
      {
        "titulo": "The JS Mirror",
        "anoEstreia": 2017,
        "diretor": [
          "Lisandro",
          "Jaime",
          "Edgar"
        ],
        "genero": [
          "Terror",
          "Caos",
          "JavaScript"
        ],
        "elenco": [
          "Amanda de Carli",
          "Alex Baptista",
          "Gilberto Junior",
          "Gustavo Gallarreta",
          "Henrique Klein",
          "Isaias Fernandes",
          "João Vitor da Silva Silveira",
          "Arthur Mattos",
          "Mario Pereira",
          "Matheus Scheffer",
          "Tiago Almeida",
          "Tiago Falcon Lopes"
        ],
        "temporadas": 5,
        "numeroEpisodios": 40,
        "distribuidora": "DBC"
      },
      {
        "titulo": "Mr. Robot",
        "anoEstreia": 2018,
        "diretor": [
          "Sam Esmail"
        ],
        "genero": [
          "Drama",
          "Techno Thriller",
          "Psychological Thriller"
        ],
        "elenco": [
          "Rami Malek",
          "Carly Chaikin",
          "Portia Doubleday",
          "Martin Wallström",
          "Christian Slater"
        ],
        "temporadas": 3,
        "numeroEpisodios": 32,
        "distribuidora": "USA Network"
      },
      {
        "titulo": "Narcos",
        "anoEstreia": 2015,
        "diretor": [
          "Paul Eckstein",
          "Mariano Carranco",
          "Tim King",
          "Lorenzo O Brien"
        ],
        "genero": [
          "Documentario",
          "Crime",
          "Drama"
        ],
        "elenco": [
          "Wagner Moura",
          "Boyd Holbrook",
          "Pedro Pascal",
          "Joann Christie",
          "Mauricie Compte",
          "André Mattos",
          "Roberto Urbina",
          "Diego Cataño",
          "Jorge A. Jiménez",
          "Paulina Gaitán",
          "Paulina Garcia"
        ],
        "temporadas": 3,
        "numeroEpisodios": 30,
        "distribuidora": null
      },
      {
        "titulo": "Westworld",
        "anoEstreia": 2016,
        "diretor": [
          "Athena Wickham"
        ],
        "genero": [
          "Ficcao Cientifica",
          "Drama",
          "Thriller",
          "Acao",
          "Aventura",
          "Faroeste"
        ],
        "elenco": [
          "Anthony I. Hopkins",
          "Thandie N. Newton",
          "Jeffrey S. Wright",
          "James T. Marsden",
          "Ben I. Barnes",
          "Ingrid N. Bolso Berdal",
          "Clifton T. Collins Jr.",
          "Luke O. Hemsworth"
        ],
        "temporadas": 2,
        "numeroEpisodios": 20,
        "distribuidora": "HBO"
      },
      {
        "titulo": "Breaking Bad",
        "anoEstreia": 2008,
        "diretor": [
          "Vince Gilligan",
          "Michelle MacLaren",
          "Adam Bernstein",
          "Colin Bucksey",
          "Michael Slovis",
          "Peter Gould"
        ],
        "genero": [
          "Acao",
          "Suspense",
          "Drama",
          "Crime",
          "Humor Negro"
        ],
        "elenco": [
          "Bryan Cranston",
          "Anna Gunn",
          "Aaron Paul",
          "Dean Norris",
          "Betsy Brandt",
          "RJ Mitte"
        ],
        "temporadas": 5,
        "numeroEpisodios": 62,
        "distribuidora": "AMC"
      }
    ]`);

    //console.log(this.series[1].titulo)

  }

  invalidas = () => {
    let ano = new Date();
    ano = ano.getFullYear();
    const titulosInvalidos = 'Series invalidas:'
    const titulos = this.series.map(serie => {
      let nulo = false;
      this.series.forEach(function (posicao) {
        if (serie[posicao] === null)
          nulo = true
      });
      if ((serie.anoEstreia > ano || nulo)) {
        return ` ${serie.titulo} -`;
      }
    });    
    return titulosInvalidos + (titulos.join(''));
  }

  filtrarPorAno = ano => {
    const seriesAnoDesejado = this.series.filter(serie => serie.anoEstreia >= ano);
    return seriesAnoDesejado;
  }

  procurarPorNome = nome => {
    let resultado = false;
    this.series.forEach(serie => {
      let validacao = serie.elenco.includes(nome);

      if (validacao) {
        resultado = true;
      }
    })
    return resultado;
  }

  mediaDeEpisodios = () => {
    let totalDeEp = 0;
    const quantidade = this.series.length;
    this.series.map(serie => totalDeEp += serie.numeroEpisodios);
    return totalDeEp / quantidade;
  }

  totalSalarios = posicao => {
    const salarioDiretor = 100.000;
    const salarioOperarios = 40.000;
    const qtdDiretores = this.series[posicao].diretor.length;
    const qtdElenco = this.series[posicao].elenco.length;
    return (salarioDiretor * qtdDiretores) + (salarioOperarios * qtdElenco);
  }

  queroGenero = (genero) => {
    let titulos = [];
    this.series.filter(serie => {
      let validacao = serie.genero.includes(genero);
      if (validacao) {
        titulos.push(serie.titulo);
      }
    })
    return titulos;
  }

  queroTitulo = (titulo) => {
    let titulos = [];
    this.series.filter(serie => {
      let validacao = serie.titulo.includes(titulo);
      if (validacao) {
        titulos.push(serie.titulo);
      }
    })
    return titulos;
  }  

  verificarAbreviacao = elenco => elenco.includes('.');

  hashtag = () => {
    let hashtag = '#';
    this.series.forEach(serie => {
      let verificacao = serie.elenco.map(ator => this.verificarAbreviacao(ator));

      if (!verificacao.includes(false)) {
        serie.elenco.map(ator => {
          let posicao = ator.indexOf('.') - 1;
          hashtag += ator.substr(posicao, 1);
        })
      }
    });
    return hashtag;
  }
}

const series = new ListaSeries();

const exercicio1 = series.invalidas();
console.log('Ex 01 - Séries Inválidas')
console.log(exercicio1)

const exercicio2 = series.filtrarPorAno(2017);
console.log('Ex 02 - Séries a partir de um determinado ano')
console.log(exercicio2)

const exercicio3 = series.procurarPorNome('Mayara');
console.log('Ex 03 - Eu sou um ator de séries?')
console.log(exercicio3)

const exercicio4 = series.mediaDeEpisodios();
console.log('Ex 04 - Média de Episódios')
console.log(exercicio4)

const exercicio5 = series.totalSalarios(0);
console.log('Ex 05 - Mascada em Série')
console.log(exercicio5)

const exercicio6A = series.queroGenero('Caos');
console.log('Ex 06A - Buscas! A')
console.log(exercicio6A)

const exercicio6B = series.queroTitulo('The');
console.log('Ex 06B - Buscas! B')
console.log(exercicio6B)

const exercicio8 = series.hashtag();
console.log('Ex 08 - hashtag secreta')
console.log(exercicio8)