db.createCollection("banco")

bancoAlfa = {
    codigo: 11,
    nome: "Alfa",
    agencia: [
        {
            codigo: 0001,
            nome: "Web",
            endereco: {
                Logradouro: "Rua Testando",
                numero: 55,
                complemento: "Loja 1",
                bairro: "NA",
                cidade: "NA",
                estado: "NA",
                pais: "Brasil"
            }
        },
        {
            codigo: 0002,
            nome: "California",
            endereco: {
                Logradouro: "Rua Testing",
                numero: 122,
                complemento: "NA",
                bairro: "Between Hyde and Powell Streets",
                cidade: "San Francisco",
                estado: "California",
                pais: "EUA"
            }
        },
        {
            codigo: 0101,
            nome: "Londres",
            endereco: {
                Logradouro: "Rua Tesing",
                numero: 525,
                complemento: "NA",
                bairro: "Croydon",
                cidade: "Londres",
                estado: "Boroughs",
                pais: "England"
            }               
        }
    ]    
};

bancoBeta = {
    codigo: 241,
    nome: "Beta",
    agencia: [
        {
             codigo: 0001,
             nome: "Web",
             endereco: {
                 Logradouro: "Rua Testando",
                 numero: 55,
                 complemento: "Loja 2",
                 bairro: "NA",
                 cidade: "NA",
                 estado: "NA",
                 pais: "Brasil"
             }
        }
    ]    
};

bancoOmega = {
    codigo: 307,
    nome: "Omega",
    agencia: [
        {
            codigo: 0001,
            nome: "Web",
            endereco: {
                Logradouro: "Rua Testando",
                numero: 55,
                complemento: "Loja 3",
                bairro: "NA",
                cidade: "NA",
                estado: "NA",
                pais: "Brasil"
            }
        },
        {
            codigo: 8761,
            nome: "Itu",
            endereco: {
                Logradouro: "Rua do Meio",
                numero: 2233,
                complemento: "NA",
                bairro: "Qualquer",
                cidade: "Itu",
                estado: "São Paulo",
                pais: "Brasil"
            }
        },
        {
            codigo: 4567,
            nome: "Hermana",
            endereco: {
                Logradouro: "Rua do Boca",
                numero: 222,
                complemento: "NA",
                bairro: "Caminito",
                cidade: "Buenos Aires",
                estado: "Buenos Aires",
                pais: "Argentina"
            }
        }
    ]    
};   

     
db.banco.insert(bancoAlfa);
db.banco.insert(bancoBeta);
db.banco.insert(bancoOmega);        

