package br.com.dbcompany.coworkinglog.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.LocalDateTime;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.util.CollectionUtils;

import br.com.dbcompany.coworkinglog.entity.LogEntity;
import ch.qos.logback.classic.Level;


@DataMongoTest
public class LogRespositoryTest {

	@Autowired
	private LogRepository logRepository;
	
	@Test
	public void deveInserirUmLogComSucesso() {
		LogEntity log = new LogEntity(Level.INFO.levelStr, "123456", "teste", LocalDateTime.now());
		LogEntity logSalvo = this.logRepository.save(log);
		assertEquals(log, logSalvo);
	}
	
	@Test
	public void deveBuscarTodos() {
		List<LogEntity> logs = this.logRepository.findAll();
		
		assertNotNull(logs);
		assertFalse(CollectionUtils.isEmpty(logs));
	}
	
	@Test
	public void deveBuscarPorSeverity() {
		List<LogEntity> logs = this.logRepository.findAll();
		
		assertNotNull(logs);
		assertFalse(CollectionUtils.isEmpty(logs));
	}
	
	@Test
	public void deveBuscarPorSeverityError() {
		final String PID = "265404";
		List<LogEntity> logs = this.logRepository.findAllBySeverityAndPid(Level.ERROR.levelStr, PID);
		boolean resultadoIncorreto = false;
		
		for(LogEntity log : logs) {
			if (!log.getSeverity().equals(Level.ERROR.levelStr) && log.getPid().equals(PID)) {
				resultadoIncorreto = true;
				break;
			}
		}
		
		assertNotNull(logs);
		assertFalse(CollectionUtils.isEmpty(logs));
		assertFalse(resultadoIncorreto);
	}
	
}
