package br.com.dbcompany.coworkinglog.controller;

import java.net.URI;
import java.time.LocalDateTime;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.dbcompany.coworkinglog.entity.LogEntity;
import ch.qos.logback.classic.Level;


@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class LogControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void retornar200QuandoConsultarPorLogs() throws Exception {
		URI uri = new URI("/logs/");
		
		mockMvc.perform(MockMvcRequestBuilders
					.get(uri)
					.contentType(MediaType.APPLICATION_JSON)
				).andExpect(MockMvcResultMatchers
						.status()
						.is(200)
				);
	}
	
	@Test
	public void retornar200QuandoConsultarPorLogUnico() throws Exception {
		URI uri = new URI("/logs/unico?tipo=WARN&codigo=265404");
		
		mockMvc.perform(MockMvcRequestBuilders
					.get(uri)
					.contentType(MediaType.APPLICATION_JSON)
				).andExpect(MockMvcResultMatchers
						.status()
						.is(200)
				);
	}
	
	@Test
	public void retornar201QuandoCriarLog() throws Exception {
		URI uri = new URI("/logs/");
		ObjectMapper mapper = new ObjectMapper();
		LogEntity log = new LogEntity(Level.INFO.levelStr, "12456", "teste de criação!", LocalDateTime.now());
		
		mockMvc.perform(MockMvcRequestBuilders
					.post(uri)
					.contentType(MediaType.APPLICATION_JSON)
					.content(mapper.writeValueAsString(log))
				).andExpect(MockMvcResultMatchers
						.status()
						.is(201)
				);
	}
	
}
