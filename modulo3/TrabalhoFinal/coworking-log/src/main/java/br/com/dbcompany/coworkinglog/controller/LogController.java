package br.com.dbcompany.coworkinglog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.dbcompany.coworkinglog.entity.LogEntity;
import br.com.dbcompany.coworkinglog.service.LogService;

@RestController
@RequestMapping("/logs")
public class LogController {

	@Autowired
	private LogService logService;
	
	@GetMapping("/")
	public ResponseEntity<?> buscar() {
		return ResponseEntity.ok(this.logService.buscarTodos());
	}
	
	@GetMapping("/unico")
	public ResponseEntity<?> buscar(@RequestParam String tipo, @RequestParam String codigo) {
		return ResponseEntity.ok(this.logService.buscarPorTipoAndCodigo(tipo, codigo));
	}
	
	@PostMapping("/")
	public ResponseEntity<?> salvar(@RequestBody LogEntity log) {
		return ResponseEntity.status(HttpStatus.CREATED).body(this.logService.salvar(log));
	}
	
}
