package br.com.dbcompany.coworkinglog.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.dbcompany.coworkinglog.entity.LogEntity;
import br.com.dbcompany.coworkinglog.repository.LogRepository;

@Service
public class LogService {
	
	@Autowired
	private LogRepository logRepository;
	
	private static final Logger logger = LoggerFactory.getLogger(LogService.class);
	
	
	public LogEntity salvar(LogEntity log) {
		logger.info("Salvando Log...");
		return this.logRepository.save(log);
	}
	
	public List<LogEntity> buscarTodos() {
		return this.logRepository.findAll();
	}
	
	public List<LogEntity> buscarPorTipoAndCodigo(String tipo, String codigo) {
		return this.logRepository.findAllBySeverityAndPid(tipo, codigo);
	}
	
}
