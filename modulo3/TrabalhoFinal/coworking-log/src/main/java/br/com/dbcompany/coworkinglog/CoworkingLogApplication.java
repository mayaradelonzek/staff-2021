package br.com.dbcompany.coworkinglog;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import br.com.dbcompany.coworkinglog.entity.LogEntity;
import br.com.dbcompany.coworkinglog.repository.LogRepository;
import ch.qos.logback.classic.Level;

@SpringBootApplication
public class CoworkingLogApplication implements CommandLineRunner {

	@Autowired
	private LogRepository logRepository;

	public static void main(String[] args) {
		SpringApplication.run(CoworkingLogApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		logRepository.deleteAll();

		final String PID = "265404";
		
		logRepository.save(new LogEntity(Level.ERROR.levelStr, PID, "Deu erro!", LocalDateTime.now()));
		logRepository.save(new LogEntity(Level.INFO.levelStr, PID, "Deu info!", LocalDateTime.now()));
		logRepository.save(new LogEntity(Level.WARN.levelStr, PID, "Deu warn!", LocalDateTime.now()));

		System.out.println("Logs found with findAll():");
		System.out.println("-------------------------------");
		for (LogEntity log : logRepository.findAll()) {
			System.out.println(log);
		}
		System.out.println();

		System.out.println("Logs found with findAllBySeverityAndPid(" + Level.WARN.levelStr + ", " + PID + "):");
		System.out.println("--------------------------------");
		System.out.println(logRepository.findAllBySeverityAndPid(Level.WARN.levelStr, PID));
	}

}
