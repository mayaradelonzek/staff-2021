package br.com.dbcompany.coworkinglog.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.dbcompany.coworkinglog.entity.LogEntity;

public interface LogRepository extends MongoRepository<LogEntity, String> {

	public List<LogEntity> findAllBySeverityAndPid(String severity, String pid);
	
}
