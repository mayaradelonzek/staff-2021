package br.com.dbccompany.coworking.repository;

import br.com.dbccompany.coworking.entity.EspacoEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


@DataJpaTest
public class EspacoRepositoryTest {

    @Autowired
    EspacoRepository espacoRepository;

    @Test
    public void deveSalvarUmEspaco() {
        EspacoEntity espacoEntity = new EspacoEntity();
        espacoEntity.setNome("sala5");
        espacoEntity.setValor(20);
        espacoEntity.setQtdPessoas(2);
        EspacoEntity espacoSalvo = espacoRepository.save(espacoEntity);

        assertEquals(espacoEntity.getNome(), espacoSalvo.getNome());
        assertEquals(espacoEntity.getValor(), espacoSalvo.getValor());
        assertEquals(espacoEntity.getQtdPessoas(), espacoSalvo.getQtdPessoas());
    }

    @Test
    public void deveBuscarUmEspacoPorId() {
        EspacoEntity espacoEntity = new EspacoEntity();
        Optional<EspacoEntity> espacoBuscado = espacoRepository.findById(1);
        assertTrue(espacoBuscado.isPresent());
    }
}
