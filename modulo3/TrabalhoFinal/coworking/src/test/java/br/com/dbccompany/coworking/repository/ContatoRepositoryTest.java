package br.com.dbccompany.coworking.repository;

import br.com.dbccompany.coworking.entity.ContatoEntity;
import br.com.dbccompany.coworking.entity.TipoContatoEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
public class ContatoRepositoryTest {

    @Autowired
    ContatoRepository contatoRepository;

    @Test
    public void deveSalvarUmContato() {
        ContatoEntity contatoEntity = new ContatoEntity();
        contatoEntity.setValor("email");
        TipoContatoEntity tipoContatoEntity = new TipoContatoEntity();
        tipoContatoEntity.setId(2);
        contatoEntity.setTipoContato(tipoContatoEntity);

        ContatoEntity contatoSalvo = contatoRepository.save(contatoEntity);

        assertEquals(contatoEntity.getValor(), contatoSalvo.getValor());
    }

    @Test
    public void deveBuscarUmContatoPorId() {
        ContatoEntity contatoEntity = new ContatoEntity();
        Optional<ContatoEntity> contatoBuscado = contatoRepository.findById(2);

        assertTrue(contatoBuscado.isPresent());
    }

}
