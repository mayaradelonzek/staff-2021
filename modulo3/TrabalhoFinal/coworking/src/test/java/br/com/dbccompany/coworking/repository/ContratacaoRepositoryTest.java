package br.com.dbccompany.coworking.repository;

import br.com.dbccompany.coworking.entity.ClienteEntity;
import br.com.dbccompany.coworking.entity.ContratacaoEntity;
import br.com.dbccompany.coworking.entity.EspacoEntity;
import br.com.dbccompany.coworking.entity.TipoContratacaoEnum;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
public class ContratacaoRepositoryTest {

    @Autowired
    ContratacaoRepository contratacaoRepository;

    @Test
    public void deveSalvarUmaContratacao() {
        ContratacaoEntity contratacaoEntity = new ContratacaoEntity();
        EspacoEntity espacoEntity = new EspacoEntity();
        espacoEntity.setId(1);
        ClienteEntity clienteEntity = new ClienteEntity();
        clienteEntity.setId(1);
        contratacaoEntity.setTipoContratacao(TipoContratacaoEnum.DIÁRIA);
        contratacaoEntity.setClienteEntity(clienteEntity);
        contratacaoEntity.setEspacoEntity(espacoEntity);
        contratacaoEntity.setPrazo(6);
        contratacaoEntity.setQuantidade(2);

        ContratacaoEntity contratacaoSalva = contratacaoRepository.save(contratacaoEntity);

        assertEquals(contratacaoEntity.getEspacoEntity().getId(), contratacaoSalva.getEspacoEntity().getId());
        assertEquals(contratacaoEntity.getTipoContratacao(), contratacaoSalva.getTipoContratacao());
        assertEquals(contratacaoEntity.getClienteEntity().getId(), contratacaoSalva.getClienteEntity().getId());
        assertEquals(contratacaoEntity.getQuantidade(), contratacaoSalva.getQuantidade());
        assertEquals(contratacaoEntity.getPrazo(), contratacaoSalva.getPrazo());
    }

    @Test
    public void deveBuscarUmaContratacao() {
        ContratacaoEntity contratacaoEntity = new ContratacaoEntity();
        Optional<ContratacaoEntity> contratacaoBuscada = contratacaoRepository.findById(1);
        assertTrue(contratacaoBuscada.isPresent());
    }
}
