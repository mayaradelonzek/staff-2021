package br.com.dbccompany.coworking.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import br.com.dbccompany.coworking.entity.ClienteEntity;
import br.com.dbccompany.coworking.entity.ContatoEntity;
import br.com.dbccompany.coworking.entity.TipoContatoEntity;

@DataJpaTest
public class ClienteRepositoryTest {

	@Autowired
	private ClienteRepository clienteRepository;
	
	
	@Test
	public void deveInserirUmClienteComSucesso() {
		ClienteEntity cliente = new ClienteEntity();
		cliente.setCpf("98565608026");
		cliente.setDataNascimento(LocalDate.now());
		cliente.setNome("Testerson");
		
		TipoContatoEntity tipoContatoTelefone = new TipoContatoEntity();
		tipoContatoTelefone.setId(1);
		
		TipoContatoEntity tipoContatoEmail = new TipoContatoEntity();
		tipoContatoEmail.setId(2);
		
		ContatoEntity contatoTelefone = new ContatoEntity();
		contatoTelefone.setValor("3390-1012");
		contatoTelefone.setTipoContato(tipoContatoTelefone);
		
		ContatoEntity contatoEmail = new ContatoEntity();
		contatoEmail.setValor("Joao@jojo.com");
		contatoEmail.setTipoContato(tipoContatoEmail);
		
		List<ContatoEntity> listaContato = new ArrayList<>();
		listaContato.add(contatoTelefone);
		listaContato.add(contatoEmail);
		
		cliente.setContatos(listaContato);
		
		ClienteEntity clienteSalvo = this.clienteRepository.save(cliente);
		
		assertEquals(cliente.getCpf(), clienteSalvo.getCpf());
	}
	
	@Test
	public void deveBuscarUmClientePorCpfComSucesso() {
		final String CPF_TESTE = "20893017086";
		
		ClienteEntity clienteNoBanco = this.clienteRepository.findByCpf(CPF_TESTE);
		
		assertEquals(CPF_TESTE, clienteNoBanco.getCpf());
	}
	
}
