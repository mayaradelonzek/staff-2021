package br.com.dbccompany.coworking.controller;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;
import java.util.Date;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@WithMockUser(username = "teste", password = "senha")
public class ContratacaoControllerTest {

    @Autowired
    private MockMvc mockMvc;

    private String JWT;

    @BeforeEach
    public void setup() {
        JWT = Jwts.builder()
                .setSubject("teste")
                .setExpiration(new Date( System.currentTimeMillis() + 860_000_000 ))
                .signWith( SignatureAlgorithm.HS512, "secret")
                .compact();
    }

    @Test
    public void retornar200QuandoConsultarContratacoesPorId() throws Exception {
        URI uri = new URI("/contratacoes/1");

        mockMvc.perform(MockMvcRequestBuilders
                .get(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + JWT)
        ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void retornar200QuandoConsultarPorContratacoes() throws Exception {
        URI uri = new URI("/contratacoes/");

        mockMvc.perform(MockMvcRequestBuilders
                .get(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + JWT)
        ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }
}
