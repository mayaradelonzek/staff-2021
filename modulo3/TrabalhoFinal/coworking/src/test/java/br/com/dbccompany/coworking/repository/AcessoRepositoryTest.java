package br.com.dbccompany.coworking.repository;

import br.com.dbccompany.coworking.entity.AcessoEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
public class AcessoRepositoryTest {

    @Autowired
    private AcessoRepository acessoRepository;

    @Test
    public void deveSalvarUmaDataDeAcesso() {
        AcessoEntity acesso = new AcessoEntity();
        acesso.setData(LocalDateTime.now());
        AcessoEntity acessoSalvo = acessoRepository.save(acesso);
        assertEquals(acessoSalvo.getData(), acesso.getData());
    }

    @Test
    public void deveBuscarUmaDataDeAcesso() {
        AcessoEntity acesso = new AcessoEntity();
        assertNotNull(acesso);
    }
}
