package br.com.dbccompany.coworking.repository;

import br.com.dbccompany.coworking.entity.UsuarioEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
public class UsuarioRepositoryTest {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Test
    public void deveSalvarUmUsuarioComSucesso() {
        UsuarioEntity usuario = new UsuarioEntity();
        usuario.setNome("Mariola");
        usuario.setEmail("mariola@ola.com");
        usuario.setSenha("ola123ola");
        usuario.setLogin("maola");

        UsuarioEntity usuariosalvo = this.usuarioRepository.save(usuario);

        assertEquals(usuario.getLogin(), usuariosalvo.getLogin());
        assertEquals(usuario.getPassword(), usuariosalvo.getPassword());
    }

    @Test
    public void deveBuscarTodos() {
        Iterable<UsuarioEntity> usuarios = usuarioRepository.findAll();
        assertNotNull(usuarios);
    }

    @Test
    public void deveBuscarUsuarioPorId() {
        Optional<UsuarioEntity> usuario = usuarioRepository.findById(1);
        assertTrue(usuario.isPresent());
    }
}
