package br.com.dbccompany.coworking.repository;

import br.com.dbccompany.coworking.entity.TipoContatoEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
public class TipoContatoRepositoryTest {
    @Autowired
    TipoContatoRepository tipoContatoRepository;

    @Test
    public void deveSalvarTipoContato() {
        TipoContatoEntity tipoContatoEntity = new TipoContatoEntity();
        tipoContatoEntity.setNome("telefone");
        TipoContatoEntity tipoContatoSalvo = tipoContatoRepository.save(tipoContatoEntity);
        assertEquals(tipoContatoEntity.getNome(), tipoContatoSalvo.getNome());
    }

    @Test
    public void deveBuscarTipoContatoPorId() {
        TipoContatoEntity tipoContatoEntity = new TipoContatoEntity();
        Optional<TipoContatoEntity> tipoContatoBuscado = tipoContatoRepository.findById(1);
        assertTrue(tipoContatoBuscado.isPresent());
    }
}
