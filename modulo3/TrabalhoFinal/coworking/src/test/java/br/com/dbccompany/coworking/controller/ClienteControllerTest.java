package br.com.dbccompany.coworking.controller;

import java.net.URI;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.dbccompany.coworking.entity.ClienteEntity;
import br.com.dbccompany.coworking.entity.ContatoEntity;
import br.com.dbccompany.coworking.entity.TipoContatoEntity;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@WithMockUser(username = "teste", password = "senha")
public class ClienteControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	private String JWT;
	
	@BeforeEach
	public void setup() {
		JWT = Jwts.builder()
		        .setSubject("teste")
		        .setExpiration(new Date( System.currentTimeMillis() + 860_000_000 ))
		        .signWith( SignatureAlgorithm.HS512, "secret")
		        .compact();
	}

	@Test
	public void retornar200QuandoConsultarClientePorId() throws Exception {
		URI uri = new URI("/clientes/1");

		mockMvc.perform(MockMvcRequestBuilders
					.get(uri)
					.contentType(MediaType.APPLICATION_JSON)
					.header("Authorization", "Bearer " + JWT)
				).andExpect(MockMvcResultMatchers
						.status()
						.is(200)
				);
	}

	@Test
	public void retornar200QuandoConsultarPorClientes() throws Exception {
		URI uri = new URI("/clientes/");

		mockMvc.perform(MockMvcRequestBuilders
				.get(uri)
				.contentType(MediaType.APPLICATION_JSON)
				.header("Authorization", "Bearer " + JWT)
		).andExpect(MockMvcResultMatchers
				.status()
				.is(200)
		);
	}


}
