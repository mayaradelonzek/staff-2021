package br.com.dbccompany.coworking.repository;

import br.com.dbccompany.coworking.entity.PacoteEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
public class PacoteRepositoryTest {

    @Autowired
    PacoteRepository pacoteRepository;

    @Test
    public void deveSalvarUmPacote() {
        PacoteEntity pacoteEntity = new PacoteEntity();
        pacoteEntity.setValor(70);

        PacoteEntity pacoteSalvo = pacoteRepository.save(pacoteEntity);
        assertEquals(pacoteEntity.getValor(), pacoteSalvo.getValor());
    }

    @Test
    public void deveBuscarUmPacotePorId() {
        PacoteEntity pacoteEntity = new PacoteEntity();
        Optional<PacoteEntity> pacoteBuscado = pacoteRepository.findById(1);
        assertTrue(pacoteBuscado.isPresent());
    }
}
