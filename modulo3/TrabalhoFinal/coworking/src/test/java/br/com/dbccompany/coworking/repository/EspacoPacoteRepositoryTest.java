package br.com.dbccompany.coworking.repository;

import br.com.dbccompany.coworking.entity.EspacoEntity;
import br.com.dbccompany.coworking.entity.EspacoPacoteEntity;
import br.com.dbccompany.coworking.entity.PacoteEntity;
import br.com.dbccompany.coworking.entity.TipoContratacaoEnum;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
public class EspacoPacoteRepositoryTest {

    @Autowired
    EspacoPacoteRepository espacoPacoteRepository;

    @Test
    public void deveSalvarUmEspacoPacote() {
        EspacoPacoteEntity espacoPacoteEntity = new EspacoPacoteEntity();
        EspacoEntity espacoEntity = new EspacoEntity();
        espacoEntity.setId(1);
        PacoteEntity pacoteEntity = new PacoteEntity();
        pacoteEntity.setId(1);
        espacoPacoteEntity.setEspacoEntity(espacoEntity);
        espacoPacoteEntity.setPacoteEntity(pacoteEntity);
        espacoPacoteEntity.setPrazo(2);
        espacoPacoteEntity.setQuantidade(4);
        espacoPacoteEntity.setTipoContratacao(TipoContratacaoEnum.MÊS);

        EspacoPacoteEntity espacoPacoteSalvo = espacoPacoteRepository.save(espacoPacoteEntity);

        assertEquals(espacoPacoteEntity.getPrazo(), espacoPacoteSalvo.getPrazo());
        assertEquals(espacoPacoteEntity.getQuantidade(), espacoPacoteSalvo.getQuantidade());
        assertEquals(espacoPacoteEntity.getTipoContratacao(), espacoPacoteSalvo.getTipoContratacao());
        assertEquals(espacoPacoteEntity.getEspacoEntity().getId(), espacoPacoteSalvo.getPacoteEntity().getId());
        assertEquals(espacoPacoteEntity.getPacoteEntity().getId(), espacoPacoteSalvo.getPacoteEntity().getId());
    }

    @Test
    public void deveBuscarUmEspacoPacotePorId() {
        EspacoPacoteEntity espacoPacoteEntity = new EspacoPacoteEntity();
        Optional<EspacoPacoteEntity> espacoPacoteBuscado = espacoPacoteRepository.findById(1);

        assertTrue(espacoPacoteBuscado.isPresent());
    }
}
