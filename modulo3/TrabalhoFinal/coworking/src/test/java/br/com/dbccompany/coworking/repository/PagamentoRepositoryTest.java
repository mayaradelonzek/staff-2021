package br.com.dbccompany.coworking.repository;

import br.com.dbccompany.coworking.entity.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
public class PagamentoRepositoryTest {

    @Autowired
    PagamentoRepository pagamentoRepository;

    @Test
    public void deveSalvarUmPagamento() {
        PagamentoEntity pagamentoEntity = new PagamentoEntity();
        ClientePacoteEntity clientePacoteEntity = new ClientePacoteEntity();
        ContratacaoEntity contratacaoEntity = new ContratacaoEntity();
        pagamentoEntity.setTipoPagamento(TipoPagamentoEnum.DÉBITO);
        pagamentoEntity.setClientePacoteEntity(clientePacoteEntity);
        pagamentoEntity.setContratacao(contratacaoEntity);
        contratacaoEntity.setId(1);
        clientePacoteEntity.setId(1);

        PagamentoEntity pagamentoSalvo = pagamentoRepository.save(pagamentoEntity);
        assertEquals(pagamentoEntity.getTipoPagamento(), pagamentoSalvo.getTipoPagamento());
        assertEquals(pagamentoEntity.getClientePacoteEntity().getId(), pagamentoSalvo.getClientePacoteEntity().getId());
        assertEquals(pagamentoEntity.getContratacao().getId(), pagamentoSalvo.getContratacao().getId());
    }

    @Test
    public void deveBuscarPagamentoPorId() {
        PagamentoEntity pagamentoEntity = new PagamentoEntity();
        Optional<PagamentoEntity> pagamentoBuscado = pagamentoRepository.findById(1);

        assertTrue(pagamentoBuscado.isPresent());
    }
}
