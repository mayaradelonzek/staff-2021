package br.com.dbccompany.coworking.repository;

import br.com.dbccompany.coworking.entity.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalDate;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
public class SaldoClienteRepositoryTest {

    @Autowired
    SaldoClienteRepository saldoClienteRepository;

    @Test
    public void deveSalvarSaldoCliente() {
        SaldoClienteKey saldoClienteKey = new SaldoClienteKey();
        SaldoClienteEntity saldoClienteEntity = new SaldoClienteEntity();
        ClienteEntity clienteEntity = new ClienteEntity();
        clienteEntity.setId(1);
        EspacoEntity espacoEntity = new EspacoEntity();
        espacoEntity.setId(1);
        saldoClienteEntity.setId(saldoClienteKey);
        saldoClienteEntity.setQuantidade(3);
        saldoClienteEntity.setVencimento(LocalDate.now());
        saldoClienteEntity.setClienteEntity(clienteEntity);
        saldoClienteEntity.setEspacoEntity(espacoEntity);
        saldoClienteEntity.setTipoContratacao(TipoContratacaoEnum.DIÁRIA);
        SaldoClienteEntity saldoClienteSalvo = saldoClienteRepository.save(saldoClienteEntity);

        assertEquals(saldoClienteEntity.getClienteEntity().getId(), saldoClienteSalvo.getClienteEntity().getId());
        assertEquals(saldoClienteEntity.getEspacoEntity().getId(), saldoClienteSalvo.getEspacoEntity().getId());
        assertEquals(saldoClienteEntity.getQuantidade(), saldoClienteSalvo.getQuantidade());
        assertEquals(saldoClienteEntity.getTipoContratacao(), saldoClienteSalvo.getTipoContratacao());
        assertEquals(saldoClienteEntity.getVencimento(), saldoClienteSalvo.getVencimento());
    }

    @Test
    public void deveBuscarSaldoClientePorId() {
        SaldoClienteKey saldoClienteKey = new SaldoClienteKey();
        saldoClienteKey.setClienteId(1);
        saldoClienteKey.setEspacoId(1);
        SaldoClienteEntity saldoClienteEntity = new SaldoClienteEntity();
        Optional<SaldoClienteEntity> saldoClienteBuscado = saldoClienteRepository.findById(saldoClienteKey);

        assertTrue(saldoClienteBuscado.isPresent());
    }
}
