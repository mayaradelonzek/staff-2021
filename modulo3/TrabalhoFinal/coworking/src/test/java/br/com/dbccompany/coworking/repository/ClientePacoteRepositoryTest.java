package br.com.dbccompany.coworking.repository;

import br.com.dbccompany.coworking.entity.ClientePacoteEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
public class ClientePacoteRepositoryTest {

    @Autowired
    ClientesPacotesRepository clientesPacotes;

    @Test
    public void deveSalvarPacotesEmQuantidade() {
        ClientePacoteEntity clientePacote = new ClientePacoteEntity();
        clientePacote.setQuantidade(3);
        ClientePacoteEntity quantidadeSalva = clientesPacotes.save(clientePacote);

        assertEquals(quantidadeSalva.getQuantidade(), clientePacote.getQuantidade());
    }

    @Test
    public void deveBuscarPacotes() {
        ClientePacoteEntity clientePacoteEntity = new ClientePacoteEntity();
        assertNotNull(clientePacoteEntity);
    }
}
