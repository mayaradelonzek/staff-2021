INSERT INTO Usuarios(Nome, Email, Login, Senha) VALUES ('Maria', 'Maria@yahoo.com.br', 'teste', '$2a$10$H0h9zytI382OVflvrRGoV.c5DddCzLRQfzX242cFWsRSt5WZ3msgS');

INSERT INTO Tipo_contato(nome) VALUES ('Telefone');

INSERT INTO Tipo_contato(nome) VALUES ('Email');

INSERT INTO Contato(id_Tipo_contato, valor) VALUES (1, '3356-9076');

INSERT INTO Contato(id_Tipo_contato, valor) VALUES (2, 'Maria@yahoo.com.br');

INSERT INTO Clientes(nome, cpf, data_nascimento) VALUES ('Maria', '20893017086', '1992-06-12');

INSERT INTO Clientes_Contatos(id_Clientes, id_Contatos) VALUES (1, 1);

INSERT INTO Clientes_Contatos(id_Clientes, id_Contatos) VALUES (1, 2);

INSERT INTO Pacotes(valor) VALUES (150);

INSERT INTO Espacos(nome, qtd_pessoas, valor) VALUES ('sala1', 5, 50);
INSERT INTO Espacos(nome, qtd_pessoas, valor) VALUES ('sala2', 15, 100);
INSERT INTO Espacos(nome, qtd_pessoas, valor) VALUES ('sala3', 25, 150);

INSERT INTO Espacos_Pacotes(id_Espacos, id_Pacotes, tipo_contratacao, quantidade, prazo) VALUES (1, 1, 'DIÁRIA', 5, 20);

INSERT INTO Clientes_Pacotes(id_Clientes, id_Pacotes, quantidade) VALUES (1, 1, 20);

INSERT INTO Contratacao(id_Espacos, id_Clientes, tipo_contratacao, quantidade, desconto, prazo) VALUES (1, 1, 'DIÁRIA', 5, 3, 2);

INSERT INTO Pagamentos(id_Clientes_Pacotes, id_Contratacao, tipo_pagamento) VALUES (1, 1, 'CRÉDITO');

INSERT INTO Saldo_cliente(id_Clientes, id_Espacos, tipo_contratacao, quantidade, vencimento) VALUES (1, 1, 'DIÁRIA', 20, '2022-06-17');

INSERT INTO Acessos(id_Clientes_Saldo_cliente, id_Espacos_Saldo_cliente, is_entrada, data, is_excecao) VALUES (1, 1, 'FALSE', '2021-06-12', 'TRUE');

