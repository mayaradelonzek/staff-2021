package br.com.dbccompany.coworking.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class SaldoInsuficienteException extends ResponseStatusException {

	private static final long serialVersionUID = 8015062659821858329L;
	
	public SaldoInsuficienteException(HttpStatus status, String reason) {
		super(status, reason);
	}

}
