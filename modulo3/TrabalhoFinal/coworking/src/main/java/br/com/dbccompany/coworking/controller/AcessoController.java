package br.com.dbccompany.coworking.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.dbccompany.coworking.entity.AcessoEntity;
import br.com.dbccompany.coworking.service.AcessoService;

@RestController
@RequestMapping("/acessos")
public class AcessoController {

	@Autowired
	private AcessoService acessoService;
	
	@GetMapping("/")
	public List<AcessoEntity> buscarTodos() {
		return this.acessoService.buscarTodos();
	}
	
	@GetMapping("/{id}")
	public AcessoEntity buscarTodos(@PathVariable Integer id) {
		return this.acessoService.buscarPorId(id);
	}
	
	@PostMapping("/entrar")
	public AcessoEntity registrarEntrada(@RequestBody AcessoEntity acesso) {
		return this.acessoService.entrar(acesso);
	}
	
	@PostMapping("/sair")
	public AcessoEntity registrarSaida(@RequestBody AcessoEntity acesso) {
		return this.acessoService.sair(acesso);
	}
	
}
