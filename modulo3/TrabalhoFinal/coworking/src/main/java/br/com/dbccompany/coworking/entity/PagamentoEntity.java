package br.com.dbccompany.coworking.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.dbccompany.coworking.entity.enums.TipoPagamentoEnum;

@Entity
@Table(name = "Pagamentos")
public class PagamentoEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name = "id_Clientes_Pacotes")
	private ClientePacoteEntity clientePacoteEntity;
	
	@ManyToOne
	@JoinColumn(name = "id_Contratacao")
	private ContratacaoEntity contratacao;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "tipo_pagamento")
	private TipoPagamentoEnum tipoPagamento;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ClientePacoteEntity getClientePacoteEntity() {
		return clientePacoteEntity;
	}

	public void setClientePacoteEntity(ClientePacoteEntity clientePacoteEntity) {
		this.clientePacoteEntity = clientePacoteEntity;
	}

	public ContratacaoEntity getContratacao() {
		return contratacao;
	}

	public void setContratacao(ContratacaoEntity contratacao) {
		this.contratacao = contratacao;
	}

	public TipoPagamentoEnum getTipoPagamento() {
		return tipoPagamento;
	}

	public void setTipoPagamento(TipoPagamentoEnum tipoPagamento) {
		this.tipoPagamento = tipoPagamento;
	}
	
}
