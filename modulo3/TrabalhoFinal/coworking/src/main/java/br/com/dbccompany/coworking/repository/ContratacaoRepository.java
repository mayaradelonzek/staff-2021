package br.com.dbccompany.coworking.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.dbccompany.coworking.entity.ContratacaoEntity;

@Repository
public interface ContratacaoRepository extends CrudRepository<ContratacaoEntity, Integer> {

	@Override
	@Query("from ContratacaoEntity c join fetch c.clienteEntity join fetch c.espacoEntity where c.id = :id")
	public Optional<ContratacaoEntity> findById(@Param(value = "id") Integer id);
	
	@Query("from ContratacaoEntity c join fetch c.clienteEntity cli join fetch c.espacoEntity e where cli.id = :id")
	public List<ContratacaoEntity> buscarPorCliente(@Param(value = "id") Integer clienteId);

}
