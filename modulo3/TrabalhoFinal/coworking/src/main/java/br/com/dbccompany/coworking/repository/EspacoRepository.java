package br.com.dbccompany.coworking.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.dbccompany.coworking.entity.EspacoEntity;

@Repository
public interface EspacoRepository extends CrudRepository<EspacoEntity, Integer> {

}
