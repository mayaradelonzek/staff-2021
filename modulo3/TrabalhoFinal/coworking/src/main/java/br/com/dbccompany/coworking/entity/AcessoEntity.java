package br.com.dbccompany.coworking.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Acessos")
public class AcessoEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "is_Entrada")
	private Boolean isEntrada;
	
	LocalDateTime data;
	
	@Column(name = "is_excecao")
	private Boolean isExcecao;
	
	@ManyToOne
	@JoinColumns(value = {
			@JoinColumn(name = "id_Clientes_Saldo_cliente"),
			@JoinColumn(name = "id_Espacos_Saldo_cliente")})
	SaldoClienteEntity saldoCliente;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getIsEntrada() {
		return isEntrada;
	}

	public void setIsEntrada(Boolean isEntrada) {
		this.isEntrada = isEntrada;
	}

	public LocalDateTime getData() {
		return data;
	}

	public void setData(LocalDateTime data) {
		this.data = data;
	}

	public Boolean getIsExcecao() {
		return isExcecao;
	}

	public void setIsExcecao(Boolean isExcecao) {
		this.isExcecao = isExcecao;
	}

	public SaldoClienteEntity getSaldoCliente() {
		return saldoCliente;
	}

	public void setSaldoCliente(SaldoClienteEntity saldoCliente) {
		this.saldoCliente = saldoCliente;
	}
	
	
	
}
