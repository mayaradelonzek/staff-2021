package br.com.dbccompany.coworking.dto;

import br.com.dbccompany.coworking.entity.ClienteEntity;
import br.com.dbccompany.coworking.entity.ClientePacoteEntity;
import br.com.dbccompany.coworking.entity.EspacoEntity;
import br.com.dbccompany.coworking.entity.EspacoPacoteEntity;
import br.com.dbccompany.coworking.entity.enums.TipoContratacaoEnum;

public class ContratarPacotesDTO {

	private Integer clienteId;
	
	private Integer espacoId;
	
	private Integer quantidadeClientePacote;
	
	private Integer quantidadeEspacoPacote;
	
	private Integer prazoEspacoPacote;
	
	private TipoContratacaoEnum tipoContratacao;
	
	public ContratarPacotesDTO() {}

	public ContratarPacotesDTO(Integer clienteId, Integer espacoId, Integer quantidadeClientePacote, Integer quantidadeEspacoPacote,
			Integer prazoEspacoPacote, TipoContratacaoEnum tipoContratacao) {
		super();
		this.clienteId = clienteId;
		this.espacoId = espacoId;
		this.quantidadeClientePacote = quantidadeClientePacote;
		this.quantidadeEspacoPacote = quantidadeEspacoPacote;
		this.prazoEspacoPacote = prazoEspacoPacote;
		this.tipoContratacao = tipoContratacao;
	}

	public Integer getClienteId() {
		return clienteId;
	}

	public void setClienteId(Integer clienteId) {
		this.clienteId = clienteId;
	}

	public Integer getEspacoId() {
		return espacoId;
	}

	public void setEspacoId(Integer espacoId) {
		this.espacoId = espacoId;
	}

	public Integer getQuantidadeEspacoPacote() {
		return quantidadeEspacoPacote;
	}

	public void setQuantidadeEspacoPacote(Integer quantidadeEspacoPacote) {
		this.quantidadeEspacoPacote = quantidadeEspacoPacote;
	}

	public Integer getPrazoEspacoPacote() {
		return prazoEspacoPacote;
	}

	public void setPrazoEspacoPacote(Integer prazoEspacoPacote) {
		this.prazoEspacoPacote = prazoEspacoPacote;
	}

	public TipoContratacaoEnum getTipoContratacao() {
		return tipoContratacao;
	}

	public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
		this.tipoContratacao = tipoContratacao;
	}
	
	public Integer getQuantidadeClientePacote() {
		return quantidadeClientePacote;
	}

	public void setQuantidadeClientePacote(Integer quantidadeClientePacote) {
		this.quantidadeClientePacote = quantidadeClientePacote;
	}

	public ClientePacoteEntity toClientesPacotes() {
		ClientePacoteEntity clientePacote = new ClientePacoteEntity();
		clientePacote.setClienteEntity(new ClienteEntity(this.clienteId));
		clientePacote.setQuantidade(quantidadeClientePacote);
		return clientePacote;
	}
	
	public EspacoPacoteEntity toEspacosPacotes() {
		EspacoPacoteEntity espacoPacote = new EspacoPacoteEntity();
		espacoPacote.setEspacoEntity(new EspacoEntity(this.espacoId));
		espacoPacote.setTipoContratacao(tipoContratacao);
		espacoPacote.setQuantidade(quantidadeEspacoPacote);
		espacoPacote.setPrazo(prazoEspacoPacote);
		return espacoPacote;
	}
	
}
