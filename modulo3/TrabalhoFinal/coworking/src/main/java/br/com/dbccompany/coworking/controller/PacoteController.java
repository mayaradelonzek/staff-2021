package br.com.dbccompany.coworking.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.dbccompany.coworking.dto.ContratarPacotesDTO;
import br.com.dbccompany.coworking.entity.PacoteEntity;
import br.com.dbccompany.coworking.service.PacoteService;

@RestController
@RequestMapping("/pacotes")
public class PacoteController {

	@Autowired
	private PacoteService pacoteService;
	
	@GetMapping("/")
	public List<PacoteEntity> buscarTodos() {
		return this.pacoteService.buscarTodos();
	}
	
	@GetMapping("/{id}")
	public PacoteEntity buscarPorId(@PathVariable Integer id) {
		return this.pacoteService.buscarPorId(id);
	}
	
	@GetMapping("/cliente/{id}")
	public List<PacoteEntity> buscarPacotesPorCliente(@PathVariable Integer id) {
		return this.pacoteService.buscarPacotesPorIdCliente(id);
	}
	
	@PostMapping("/")
	public Integer cadastrar(@RequestBody List<ContratarPacotesDTO> contratarPacotesDTOs) {
		return pacoteService.salvar(contratarPacotesDTOs);
	}
	
}
