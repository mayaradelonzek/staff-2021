package br.com.dbccompany.coworking.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.dbccompany.coworking.dto.LogApiDTO;

@Service
public class CoworkingLogApiService {
	
	private final String POST_LOGS_URL = "http://localhost:8081/logs/"; 
	
	private static final Logger logger = LoggerFactory.getLogger(CoworkingLogApiService.class);

	public void sendLogToApi(LogApiDTO log) {
		ObjectMapper mapper = new ObjectMapper();
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		try {
			HttpEntity<String> request = new HttpEntity<String>(mapper.writeValueAsString(log), headers);
			restTemplate.postForEntity(POST_LOGS_URL, request, String.class);
		} catch (JsonProcessingException e) {
			logger.error("Erro ao fazer o parser do LogApiDTO ao enviar para API!");
			System.err.println(e.getMessage());
		} catch (RestClientException e) {
			logger.error("Erro ao se conectar com a API de Log!");
		} catch (Exception e) {
			logger.error("TRETA!");
		}
		
	}

}
