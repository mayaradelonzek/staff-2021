package br.com.dbccompany.coworking.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.dbccompany.coworking.entity.PagamentoEntity;
import br.com.dbccompany.coworking.service.PagamentoService;

@RestController
@RequestMapping("/pagamentos")
public class PagamentoController {

	@Autowired
	private PagamentoService pagamentoService;
	
	@GetMapping("/")
	public List<PagamentoEntity> buscarTodos() {
		return this.pagamentoService.buscarTodos();
	}
	
	@GetMapping("/{id}")
	public PagamentoEntity buscarPorId(@PathVariable Integer id) {
		return this.pagamentoService.buscarPorId(id);
	}
	
	@PostMapping("/")
	public PagamentoEntity pagar(@RequestBody PagamentoEntity pagamento) {
		return this.pagamentoService.salvar(pagamento);
	}
	
}
