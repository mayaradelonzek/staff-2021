package br.com.dbccompany.coworking.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.dbccompany.coworking.entity.ContratacaoEntity;
import br.com.dbccompany.coworking.entity.EspacoEntity;
import br.com.dbccompany.coworking.repository.ContratacaoRepository;

@Service
public class ContratacaoService extends AbstractService<ContratacaoEntity, ContratacaoRepository, Integer> {

	@Autowired
	private ContratacaoRepository contratacaoRepository;
	
	@Autowired
	private EspacoService espacoService;
	
	public ContratacaoService() {
		super(ContratacaoService.class);
	}
	
	public List<ContratacaoEntity> buscarTodos() {
		return buscarTodosAndLogar(contratacaoRepository);
	}
	
	public ContratacaoEntity buscarPor(Integer id) {
		return buscarPorIdAndLogar(id, contratacaoRepository);
	}
	
	public List<ContratacaoEntity> buscarPorCliente(Integer clienteId) {
		return this.contratacaoRepository.buscarPorCliente(clienteId);
	}
	
	@Transactional
	public Integer salvar(ContratacaoEntity contratacao) {
		EspacoEntity espaco = espacoService.buscarPorId(contratacao.getEspacoEntity().getId());
		contratacao = salvarAndLogar(contratacao, contratacaoRepository);
		return contratacao.getQuantidade() * espaco.getValor();
	}

	@Override
	protected void aplicarRegras() {
		// TODO Auto-generated method stub
		
	}
	
	
}
