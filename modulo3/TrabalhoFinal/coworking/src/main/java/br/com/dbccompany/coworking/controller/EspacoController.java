package br.com.dbccompany.coworking.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.dbccompany.coworking.entity.EspacoEntity;
import br.com.dbccompany.coworking.service.EspacoService;

@RestController
@RequestMapping("/espacos")
public class EspacoController {

	@Autowired
	private EspacoService espacoService;
	
	@GetMapping
	public List<EspacoEntity> buscarTodos() {
		return this.espacoService.buscarTodos();
	}
	
	@GetMapping("/{id}")
	public EspacoEntity buscarEspaco(@PathVariable Integer id) {
		return this.espacoService.buscarPorId(id);
	}
	
}
