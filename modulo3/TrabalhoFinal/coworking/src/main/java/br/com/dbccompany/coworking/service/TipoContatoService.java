package br.com.dbccompany.coworking.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.dbccompany.coworking.entity.TipoContatoEntity;
import br.com.dbccompany.coworking.repository.TipoContatoRepository;

@Service
public class TipoContatoService extends AbstractService<TipoContatoEntity, TipoContatoRepository, Integer> {

	public TipoContatoService() {
		super(TipoContatoService.class);
	}

	@Autowired
	private TipoContatoRepository tipoContatoRepository;

	public TipoContatoEntity buscarPorId(Integer id) {
		return buscarPorIdAndLogar(id, tipoContatoRepository);
	}

	@Override
	protected void aplicarRegras() {
		// TODO Auto-generated method stub

	}
}
