package br.com.dbccompany.coworking.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import br.com.dbccompany.coworking.entity.ClientePacoteEntity;
import br.com.dbccompany.coworking.entity.ContratacaoEntity;
import br.com.dbccompany.coworking.entity.EspacoPacoteEntity;
import br.com.dbccompany.coworking.entity.PagamentoEntity;
import br.com.dbccompany.coworking.entity.SaldoClienteEntity;
import br.com.dbccompany.coworking.entity.SaldoClienteKey;
import br.com.dbccompany.coworking.exception.RecursoNaoEncontradoException;
import br.com.dbccompany.coworking.repository.ClientesPacotesRepository;
import br.com.dbccompany.coworking.repository.PagamentoRepository;
import br.com.dbccompany.coworking.repository.SaldoClienteRepository;

@Service
public class PagamentoService extends AbstractService<PagamentoEntity, PagamentoRepository, Integer> {

	@Autowired
	private PagamentoRepository pagamentoRepository;

	@Autowired
	private ContratacaoService contratacaoService;

	@Autowired
	private SaldoClienteRepository saldoClienteRepository;

	@Autowired
	private ClientesPacotesRepository clientePacotesRepository;
	
	public PagamentoService() {
		super(PagamentoService.class);
	}
	
	public List<PagamentoEntity> buscarTodos() {
		return buscarTodosAndLogar(pagamentoRepository);
	}
	
	public PagamentoEntity buscarPorId(Integer id) {
		return buscarPorIdAndLogar(id, pagamentoRepository);
	}

	@Transactional
	public PagamentoEntity salvar(PagamentoEntity pagamento) {
		if (pagamento.getContratacao() == null) {
			pagarPacotes(pagamento).forEach(saldoCliente -> this.saldoClienteRepository.save(saldoCliente));
		} else {
			this.saldoClienteRepository.save(pagarIndividual(pagamento));
		}
		
		return this.pagamentoRepository.save(pagamento);
	}

	private List<SaldoClienteEntity> pagarPacotes(PagamentoEntity pagamento) {
		ClientePacoteEntity clientePacote = this.clientePacotesRepository
				.buscarInfoParaSaldoCliente(pagamento.getClientePacoteEntity().getId());
		List<SaldoClienteEntity> saldoClientes = new ArrayList<>();

		if (clientePacote == null) {
			throw new RecursoNaoEncontradoException(HttpStatus.BAD_REQUEST,
					"O recurso clientePacote não foi encontrado!");
		}

		for (int i = 0; i < clientePacote.getPacoteEntity().getEspacos().size(); i++) {
			EspacoPacoteEntity espacoPacote = clientePacote.getPacoteEntity().getEspacos().get(i);

			saldoClientes.add(new SaldoClienteEntity(
					new SaldoClienteKey(clientePacote.getClienteEntity().getId(), espacoPacote.getId()),
					clientePacote.getClienteEntity(), espacoPacote.getEspacoEntity(),
					Integer.valueOf(clientePacote.getQuantidade() * espacoPacote.getEspacoEntity().getValor()),
					LocalDate.now().plusDays(espacoPacote.getPrazo().longValue()), espacoPacote.getTipoContratacao()));
		}

		return saldoClientes;
	}

	private SaldoClienteEntity pagarIndividual(PagamentoEntity pagamento) {
		ContratacaoEntity contratacao = this.contratacaoService.buscarPor(pagamento.getContratacao().getId());

		return new SaldoClienteEntity(
				new SaldoClienteKey(contratacao.getClienteEntity().getId(), contratacao.getEspacoEntity().getId()),
				contratacao.getClienteEntity(), contratacao.getEspacoEntity(), calcularQuantidade(contratacao),
				LocalDate.now().plusDays(contratacao.getPrazo().longValue()), contratacao.getTipoContratacao());
	}

	private Integer calcularQuantidade(ContratacaoEntity contratacao) {
		return contratacao.getDesconto() != null ? 
					contratacao.getQuantidade() * (contratacao.getEspacoEntity().getValor() - contratacao.getDesconto()) : 
					contratacao.getQuantidade() * (contratacao.getEspacoEntity().getValor());
	}

	@Override
	protected void aplicarRegras() {
		// TODO Auto-generated method stub
		
	}

}
