package br.com.dbccompany.coworking.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "O cliente não possui Email e Telefone!")
public class ClienteComContatoInsuficienteException extends RuntimeException {

	private static final long serialVersionUID = -7356182587886627891L;

	public ClienteComContatoInsuficienteException() {
		super();
	}
	
}
