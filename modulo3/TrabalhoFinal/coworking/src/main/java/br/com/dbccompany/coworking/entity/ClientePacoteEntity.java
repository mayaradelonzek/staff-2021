package br.com.dbccompany.coworking.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "Clientes_Pacotes")
public class ClientePacoteEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name = "id_Clientes")
	private ClienteEntity clienteEntity;
	
	@ManyToOne
	@JoinColumn(name = "id_Pacotes")
	@JsonIgnoreProperties(value =  "clientes")
	private PacoteEntity pacoteEntity;
	
	private Integer quantidade;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ClienteEntity getClienteEntity() {
		return clienteEntity;
	}

	public void setClienteEntity(ClienteEntity clienteEntity) {
		this.clienteEntity = clienteEntity;
	}

	public PacoteEntity getPacoteEntity() {
		return pacoteEntity;
	}

	public void setPacoteEntity(PacoteEntity pacoteEntity) {
		this.pacoteEntity = pacoteEntity;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}
	
}
