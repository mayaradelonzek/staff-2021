package br.com.dbccompany.coworking.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.dbccompany.coworking.entity.enums.TipoContratacaoEnum;

@Entity
@Table(name = "Espacos_Pacotes")
public class EspacoPacoteEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name = "id_Espacos")
	private EspacoEntity espacoEntity; 
	
	@ManyToOne
	@JoinColumn(name = "id_Pacotes")
	@JsonIgnoreProperties(value = {"espacos", "clientes"})
	private PacoteEntity pacoteEntity;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "tipo_contratacao")
	private TipoContratacaoEnum tipoContratacao;
	
	private Integer quantidade;
	
	private Integer prazo;
	
	public EspacoPacoteEntity() {}

	public EspacoPacoteEntity(Integer id) {
		super();
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public EspacoEntity getEspacoEntity() {
		return espacoEntity;
	}

	public void setEspacoEntity(EspacoEntity espacoEntity) {
		this.espacoEntity = espacoEntity;
	}

	public PacoteEntity getPacoteEntity() {
		return pacoteEntity;
	}

	public void setPacoteEntity(PacoteEntity pacoteEntity) {
		this.pacoteEntity = pacoteEntity;
	}

	public TipoContratacaoEnum getTipoContratacao() {
		return tipoContratacao;
	}

	public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
		this.tipoContratacao = tipoContratacao;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public Integer getPrazo() {
		return prazo;
	}

	public void setPrazo(Integer prazo) {
		this.prazo = prazo;
	}
	
}
