package br.com.dbccompany.coworking.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.dbccompany.coworking.entity.ContratacaoEntity;
import br.com.dbccompany.coworking.service.ContratacaoService;

@RestController
@RequestMapping("/contratacoes")
public class ContratacaoController {

	@Autowired
	private ContratacaoService contratacaoService;
	
	@GetMapping("/")
	public List<ContratacaoEntity> buscarTodos() {
		return contratacaoService.buscarTodos();
	}
	
	@GetMapping("/{id}")
	public ContratacaoEntity buscarPorId(@PathVariable Integer id) {
		return contratacaoService.buscarPor(id);
	}
	
	@GetMapping("/cliente/{id}")
	public List<ContratacaoEntity> buscarContratacoesPorCliente(@PathVariable Integer id) {
		return contratacaoService.buscarPorCliente(id);
	}
	
	@PostMapping("/")
	public Integer registrarOrcamento(@RequestBody ContratacaoEntity contratacao) {
		return contratacaoService.salvar(contratacao);
	}
	
}
