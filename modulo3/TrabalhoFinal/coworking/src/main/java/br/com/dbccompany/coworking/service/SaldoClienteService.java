package br.com.dbccompany.coworking.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.dbccompany.coworking.entity.SaldoClienteEntity;
import br.com.dbccompany.coworking.entity.SaldoClienteKey;
import br.com.dbccompany.coworking.repository.SaldoClienteRepository;

@Service
public class SaldoClienteService extends AbstractService<SaldoClienteEntity, SaldoClienteRepository, SaldoClienteKey> {

	public SaldoClienteService() {
		super(SaldoClienteService.class);
	}

	@Autowired
	private SaldoClienteRepository saldoClienteRepository;
	
	@Override
	protected void aplicarRegras() {}
	
	public SaldoClienteEntity salvar(SaldoClienteEntity saldoCliente) {
		return salvarAndLogar(saldoCliente, saldoClienteRepository);
	}
	
	public SaldoClienteEntity buscarPorId(SaldoClienteKey id) {
		return buscarPorIdAndLogar(id, saldoClienteRepository);
	}

}
