package br.com.dbccompany.coworking.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Pacotes")
public class PacoteEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@OneToMany(mappedBy = "pacoteEntity")
	private List<EspacoPacoteEntity> espacos;
	
	@OneToMany(mappedBy = "pacoteEntity")
	private List<ClientePacoteEntity> clientes;
	
	private Integer valor;
	
	public PacoteEntity() {}

	public PacoteEntity(Integer valor) {
		super();
		this.valor = valor;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getValor() {
		return valor;
	}

	public void setValor(Integer valor) {
		this.valor = valor;
	}

	public List<EspacoPacoteEntity> getEspacos() {
		return espacos;
	}

	public void setEspacos(List<EspacoPacoteEntity> espacos) {
		this.espacos = espacos;
	}

	public List<ClientePacoteEntity> getClientes() {
		return clientes;
	}

	public void setClientes(List<ClientePacoteEntity> clientes) {
		this.clientes = clientes;
	}
	
}
