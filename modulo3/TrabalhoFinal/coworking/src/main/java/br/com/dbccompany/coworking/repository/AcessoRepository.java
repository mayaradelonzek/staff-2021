package br.com.dbccompany.coworking.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import br.com.dbccompany.coworking.entity.AcessoEntity;
import br.com.dbccompany.coworking.entity.SaldoClienteKey;

public interface AcessoRepository extends CrudRepository<AcessoEntity, Integer> {
	
	@Query("from AcessoEntity a join fetch a.saldoCliente sc where a.isEntrada = 'true' and sc.id = :saldoClienteId")
	public AcessoEntity buscarAcessosDoCliente(@Param("saldoClienteId") SaldoClienteKey saldoClienteId);
	
}
