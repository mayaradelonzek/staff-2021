package br.com.dbccompany.coworking.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import br.com.dbccompany.coworking.entity.enums.TipoContratacaoEnum;

@Entity
@Table(name = "Saldo_cliente")
public class SaldoClienteEntity {

	@EmbeddedId
	private SaldoClienteKey id;
	
	@ManyToOne
	@JoinColumn(name = "id_Clientes")
	@MapsId("clienteId")
	private ClienteEntity clienteEntity;
	
	@ManyToOne
	@JoinColumn(name = "id_Espacos")
	@MapsId("espacoId")
	private EspacoEntity espacoEntity;
	
	private Integer quantidade;
	
	private LocalDate vencimento;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "tipo_contratacao")
	private TipoContratacaoEnum tipoContratacao;
	
	public SaldoClienteEntity() {}
	
	public SaldoClienteEntity(SaldoClienteKey id, ClienteEntity clienteEntity, EspacoEntity espacoEntity, Integer quantidade,
			LocalDate vencimento, TipoContratacaoEnum tipoContratacao) {
		super();
		this.id = id;
		this.clienteEntity = clienteEntity;
		this.espacoEntity = espacoEntity;
		this.quantidade = quantidade;
		this.vencimento = vencimento;
		this.tipoContratacao = tipoContratacao;
	}

	public SaldoClienteKey getId() {
		return id;
	}

	public void setId(SaldoClienteKey id) {
		this.id = id;
	}

	public ClienteEntity getClienteEntity() {
		return clienteEntity;
	}

	public void setClienteEntity(ClienteEntity clienteEntity) {
		this.clienteEntity = clienteEntity;
	}

	public EspacoEntity getEspacoEntity() {
		return espacoEntity;
	}

	public void setEspacoEntity(EspacoEntity espacoEntity) {
		this.espacoEntity = espacoEntity;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public LocalDate getVencimento() {
		return vencimento;
	}

	public void setVencimento(LocalDate vencimento) {
		this.vencimento = vencimento;
	}

	public TipoContratacaoEnum getTipoContratacao() {
		return tipoContratacao;
	}

	public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
		this.tipoContratacao = tipoContratacao;
	}
	
}
