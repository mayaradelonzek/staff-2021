package br.com.dbccompany.coworking.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class SaldoClienteKey implements Serializable {

	private static final long serialVersionUID = -7294576235421396116L;

	@Column(name = "id_Clientes")
	private Integer clienteId;
	
	@Column(name = "id_Espacos")
	private Integer espacoId;
	
	public SaldoClienteKey() {}

	public SaldoClienteKey(Integer clienteId, Integer espacoId) {
		super();
		this.clienteId = clienteId;
		this.espacoId = espacoId;
	}

	public Integer getClienteId() {
		return clienteId;
	}

	public void setClienteId(Integer clienteId) {
		this.clienteId = clienteId;
	}

	public Integer getEspacoId() {
		return espacoId;
	}

	public void setEspacoId(Integer espacoId) {
		this.espacoId = espacoId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((clienteId == null) ? 0 : clienteId.hashCode());
		result = prime * result + ((espacoId == null) ? 0 : espacoId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SaldoClienteKey other = (SaldoClienteKey) obj;
		if (clienteId == null) {
			if (other.clienteId != null)
				return false;
		} else if (!clienteId.equals(other.clienteId))
			return false;
		if (espacoId == null) {
			if (other.espacoId != null)
				return false;
		} else if (!espacoId.equals(other.espacoId))
			return false;
		return true;
	}
	
}
