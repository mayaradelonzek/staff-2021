package br.com.dbccompany.coworking.dto;

import java.time.LocalDateTime;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

public class LogApiDTO {

	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private LocalDateTime dataHoraCriacao;
	
	private String severity;
	
	private String pid;
	
	private String mensagem;

	public LogApiDTO() {
		super();
	}

	public LogApiDTO(LocalDateTime dataHoraCriacao, String pid, String severity, String mensagem) {
		super();
		this.pid = pid;
		this.dataHoraCriacao = dataHoraCriacao;
		this.severity = severity;
		this.mensagem = mensagem;
	}

	public LocalDateTime getDataHoraCriacao() {
		return dataHoraCriacao;
	}

	public void setDataHoraCriacao(LocalDateTime dataHoraCriacao) {
		this.dataHoraCriacao = dataHoraCriacao;
	}

	public String getSeverity() {
		return severity;
	}

	public void setSeverity(String severity) {
		this.severity = severity;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}
	
}
