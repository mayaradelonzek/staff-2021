package br.com.dbccompany.coworking.entity.enums;

public enum TipoContratacaoEnum {

	MINUTO, HORA, TURNO, DIÁRIA, SEMANA, MÊS;
	
}
