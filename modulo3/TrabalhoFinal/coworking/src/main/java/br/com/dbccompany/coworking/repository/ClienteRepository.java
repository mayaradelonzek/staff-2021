package br.com.dbccompany.coworking.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.dbccompany.coworking.entity.ClienteEntity;

@Repository
public interface ClienteRepository extends CrudRepository<ClienteEntity, Integer> {

	@Override
	Optional<ClienteEntity> findById(Integer id);
	
	ClienteEntity findByCpf(String cpf);
	
}
