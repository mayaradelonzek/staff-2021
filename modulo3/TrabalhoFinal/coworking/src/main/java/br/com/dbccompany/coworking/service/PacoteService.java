package br.com.dbccompany.coworking.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.coworking.dto.ContratarPacotesDTO;
import br.com.dbccompany.coworking.entity.ClientePacoteEntity;
import br.com.dbccompany.coworking.entity.EspacoEntity;
import br.com.dbccompany.coworking.entity.EspacoPacoteEntity;
import br.com.dbccompany.coworking.entity.PacoteEntity;
import br.com.dbccompany.coworking.repository.ClientesPacotesRepository;
import br.com.dbccompany.coworking.repository.EspacoPacoteRepository;
import br.com.dbccompany.coworking.repository.PacoteRepository;

@Service
public class PacoteService extends AbstractService<PacoteEntity, PacoteRepository, Integer> {

	@Autowired
	private PacoteRepository pacoteRepository;

	@Autowired
	private EspacoPacoteRepository espacoPacoteRepository;
	
	@Autowired
	private ClientesPacotesRepository clientePacoteRepository;

	@Autowired
	private EspacoService espacoService;
	
	public PacoteService() {
		super(PacoteService.class);
	}
	
	public List<PacoteEntity> buscarTodos() {
		return buscarTodosAndLogar(pacoteRepository);
	}
	
	public List<PacoteEntity> buscarPacotesPorIdCliente(Integer idCliente) {
		return this.pacoteRepository.buscarPacotesPorCliente(idCliente);
	}
	
	public PacoteEntity buscarPorId(Integer id) {
		return buscarPorIdAndLogar(id, pacoteRepository);
	}

	@Transactional
	public Integer salvar(List<ContratarPacotesDTO> contratarPacotesDTO) {
		Integer valorTotalPacote = 0;
		
		for(ContratarPacotesDTO dto : contratarPacotesDTO) {
			EspacoEntity espaco = this.espacoService.buscarPorId(dto.getEspacoId());
			
			valorTotalPacote += (espaco.getValor() * dto.getQuantidadeEspacoPacote());
		}
		
		PacoteEntity pacote = salvarAndLogar(new PacoteEntity(valorTotalPacote), pacoteRepository);
		
		contratarPacotesDTO.forEach(dto -> {
			EspacoPacoteEntity ep = dto.toEspacosPacotes();
			ep.setPacoteEntity(pacote);
			this.espacoPacoteRepository.save(ep);
		});
		
		ClientePacoteEntity cp = contratarPacotesDTO.get(0).toClientesPacotes();
		cp.setPacoteEntity(pacote);
		this.clientePacoteRepository.save(cp);
		
		return valorTotalPacote;
	}

	@Override
	protected void aplicarRegras() {
		// TODO Auto-generated method stub
		
	}

}
