package br.com.dbccompany.coworking.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.dbccompany.coworking.entity.UsuarioEntity;

@Repository
public interface UsuarioRepository extends CrudRepository<UsuarioEntity, Integer> {

	public UsuarioEntity findByLogin(String login);
	
}
