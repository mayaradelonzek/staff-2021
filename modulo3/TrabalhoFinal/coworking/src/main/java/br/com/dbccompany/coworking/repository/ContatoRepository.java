package br.com.dbccompany.coworking.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.dbccompany.coworking.entity.ContatoEntity;

@Repository
public interface ContatoRepository extends CrudRepository<ContatoEntity, Integer> {

}
