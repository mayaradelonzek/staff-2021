package br.com.dbccompany.coworking.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.dbccompany.coworking.entity.SaldoClienteEntity;
import br.com.dbccompany.coworking.entity.SaldoClienteKey;

@Repository
public interface SaldoClienteRepository extends CrudRepository<SaldoClienteEntity, SaldoClienteKey> {
	
	@Query("update SaldoClienteEntity cp set quantidade = 0 where cp.id = :id")
	public void zerarSaldoById(@Param("id") SaldoClienteKey id);
	
}
