package br.com.dbccompany.coworking.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.dbccompany.coworking.entity.ClientePacoteEntity;

@Repository
public interface ClientesPacotesRepository extends CrudRepository<ClientePacoteEntity, Integer> {

	@Query("update ClientePacoteEntity cp set quantidade = :quantidade where clienteEntity.id = :id")
	public void atualizarQuantidadePorIdCliente(@Param(value = "quantidade") Integer quantidade,
			@Param(value = "id") Integer id);

	@Query("from ClientePacoteEntity cp join fetch cp.pacoteEntity p join fetch p.espacos e join fetch e.espacoEntity where cp.id = :id")
	public ClientePacoteEntity buscarInfoParaSaldoCliente(@Param(value = "id") Integer id);

}
