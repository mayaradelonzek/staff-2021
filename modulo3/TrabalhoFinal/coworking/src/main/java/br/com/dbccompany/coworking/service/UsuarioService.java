package br.com.dbccompany.coworking.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.dbccompany.coworking.entity.UsuarioEntity;
import br.com.dbccompany.coworking.exception.SenhaInvalidaException;
import br.com.dbccompany.coworking.repository.UsuarioRepository;

@Service
public class UsuarioService extends AbstractService<UsuarioEntity, UsuarioRepository, Integer> {

	@Autowired
	private UsuarioRepository usuarioRepository;
	
	private UsuarioEntity usuario;
	
	public UsuarioService() {
		super(UsuarioService.class);
	}
	
	public List<UsuarioEntity> buscarTodos() {
		return buscarTodosAndLogar(usuarioRepository);
	}
	
	public UsuarioEntity buscarPorId(Integer id) {
		return buscarPorIdAndLogar(id, usuarioRepository);
	}
	
	@Transactional
	public UsuarioEntity salvar(UsuarioEntity usuario) {
		this.usuario = usuario;
		return salvarAndLogar(usuario, usuarioRepository);
	}

	@Override
	protected void aplicarRegras() {
		validarSeContemSomenteAlphaNumerico();
		cifrarSenha();
	}
	
	private void cifrarSenha( ) {
		this.usuario.setSenha(new BCryptPasswordEncoder().encode(usuario.getSenha()));
	}
	
	private void validarSeContemSomenteAlphaNumerico() {
		List<Character> caracteres = this.usuario.getSenha().chars().mapToObj(c -> (char) c).collect(Collectors.toList());

		for(Character caractere : caracteres) {
			Boolean isDigitoOuNumero = Character.isDigit(caractere) || Character.isLetter(caractere);
			
			if(!isDigitoOuNumero) {
				throw new SenhaInvalidaException("A senha possui caracteres inválidos! Caractere inválido: " + caractere);
			}	
		}
	}

}
