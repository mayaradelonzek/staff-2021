package br.com.dbccompany.coworking.util;

import org.slf4j.Logger;
import ch.qos.logback.classic.Level;

public class LogUtils {
	
	public static void log(Logger logger, Level level, String message) {
		if(level.equals(Level.INFO)) {
			logger.info(message);
		} else if(level.equals(Level.WARN)) {
			logger.warn(message);
		} else if (level.equals(Level.ERROR)) {
			logger.error(message);
		}
	}
	
}
