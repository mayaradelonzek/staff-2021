package br.com.dbccompany.coworking.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.dbccompany.coworking.entity.EspacoEntity;
import br.com.dbccompany.coworking.repository.EspacoRepository;

@Service
public class EspacoService extends AbstractService<EspacoEntity, EspacoRepository, Integer> {

	public EspacoService() {
		super(EspacoService.class);
	}
	
	@Autowired
	private EspacoRepository espacoRepository;
	
	public EspacoEntity buscarPorId(Integer id) {
		return buscarPorIdAndLogar(id, espacoRepository);
	}
	
	public List<EspacoEntity> buscarTodos() {
		return buscarTodosAndLogar(espacoRepository);
	}
	
	
	@Override
	protected void aplicarRegras() {
		// TODO Auto-generated method stub
		
	}

}
