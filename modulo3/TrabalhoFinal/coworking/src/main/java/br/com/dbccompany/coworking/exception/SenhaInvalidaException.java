package br.com.dbccompany.coworking.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class SenhaInvalidaException extends ResponseStatusException  {

	private static final long serialVersionUID = -6913671433300723820L;
	
	public SenhaInvalidaException(String reason) {
		super(HttpStatus.BAD_REQUEST, reason);
	}

}
