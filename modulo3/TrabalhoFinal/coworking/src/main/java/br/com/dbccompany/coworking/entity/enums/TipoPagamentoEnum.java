package br.com.dbccompany.coworking.entity.enums;

public enum TipoPagamentoEnum {

	DÉBITO, CRÉDITO, DINHEIRO, TRANSFERÊNCIA;
	
}
