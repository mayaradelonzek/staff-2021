package br.com.dbccompany.coworking.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.system.ApplicationPid;
import org.springframework.data.repository.CrudRepository;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.coworking.dto.LogApiDTO;
import br.com.dbccompany.coworking.exception.RecursoNaoEncontradoException;
import br.com.dbccompany.coworking.util.LogUtils;
import ch.qos.logback.classic.Level;

public abstract class AbstractService<T, R extends CrudRepository<T, I>, I> {

	@Autowired
	private CoworkingLogApiService coworkingLogApiService;
	
	private Logger logger;
	
	protected abstract void aplicarRegras();
	
	public AbstractService(Class<?> clazz) {
		this.logger = LoggerFactory.getLogger(clazz);
	}
	
	@Transactional
	protected T salvarAndLogar(T t, R r) {
		try {
			log(Level.INFO, "Salvando " + t.getClass().getName());
			aplicarRegras();
			r.save(t);
			log(Level.INFO, t.getClass().getName() + " salvo!");
			return t;
		} catch (Exception e) {
			log(Level.ERROR, e.getMessage());
			throw new RecursoNaoEncontradoException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}
	
	protected T buscarPorIdAndLogar(I i, R r) {
		try {
			log(Level.INFO, "Buscando pelo id: " + i);
			T t = r.findById(i).get();
			log(Level.INFO, t.getClass().getName() + " salvo!");
			return t;
		} catch (Exception e) {
			log(Level.ERROR, e.getMessage());
			throw new RecursoNaoEncontradoException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}
	
	protected List<T> buscarTodosAndLogar(R r) {	    
		try {
			log(Level.INFO, "Buscando por todos");
			List<T> listT = StreamSupport.stream(r.findAll().spliterator(), false).collect(Collectors.toList());
			log(Level.INFO, "Recursos encontrados!");
			return listT;
		} catch (Exception e) {
			log(Level.ERROR, e.getMessage());
			throw new RecursoNaoEncontradoException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}
	
	private void log(Level level, String message) {
		LogUtils.log(this.logger, level, message);
		coworkingLogApiService
		.sendLogToApi(new LogApiDTO(LocalDateTime.now(), new ApplicationPid().toString(), level.levelStr, message));
	}
	
}
