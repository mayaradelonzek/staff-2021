package br.com.dbccompany.coworking.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.dbccompany.coworking.entity.ClienteEntity;
import br.com.dbccompany.coworking.entity.ContatoEntity;
import br.com.dbccompany.coworking.entity.TipoContatoEntity;
import br.com.dbccompany.coworking.exception.ClienteComContatoInsuficienteException;
import br.com.dbccompany.coworking.repository.ClienteRepository;

@Service
public class ClienteService extends AbstractService<ClienteEntity, ClienteRepository, Integer> {

	@Autowired
	private ClienteRepository clienteRepository;
	
	@Autowired
	private TipoContatoService tipoContatoService;
	
	private ClienteEntity cliente;
	
	public ClienteService() {
		super(ClienteService.class);
	}
	
	public List<ClienteEntity> buscarPorTodos() {
		return buscarTodosAndLogar(clienteRepository);
	}

	public ClienteEntity buscarPorId(Integer id) {
		return buscarPorIdAndLogar(id, clienteRepository);
	}
	
	@Override
	protected void aplicarRegras() {
		if(!hasEmailAndTelefone(cliente)) {
			throw new ClienteComContatoInsuficienteException();
		}
	}
	
	public ClienteEntity salvar(ClienteEntity cliente) {
		this.cliente = cliente;
		return salvarAndLogar(cliente, clienteRepository);
	}
	
	private Boolean hasEmailAndTelefone(ClienteEntity cliente) {
		Boolean hasEmail = false;
		Boolean hasTelefone = false;
		
		for(ContatoEntity contato : cliente.getContatos()) {
			TipoContatoEntity tipoContato = tipoContatoService.buscarPorId(contato.getTipoContato().getId());
			if("Telefone".equals(tipoContato.getNome())) {
				hasTelefone = true;
			} else if ("Email".equals(tipoContato.getNome())) {
				hasEmail = true;
			}
		}
		
		return hasTelefone && hasEmail;
	}

}
