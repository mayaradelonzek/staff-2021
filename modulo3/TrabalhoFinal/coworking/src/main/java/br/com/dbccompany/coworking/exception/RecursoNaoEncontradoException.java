package br.com.dbccompany.coworking.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class RecursoNaoEncontradoException extends ResponseStatusException {

	private static final long serialVersionUID = -6051746920550497318L;
	
	public RecursoNaoEncontradoException(HttpStatus status, String mensagem) {
		super(status, mensagem);
	}
	
	public RecursoNaoEncontradoException(String mensagem) {
		super(HttpStatus.NOT_FOUND, mensagem);
	}
	
	public RecursoNaoEncontradoException() {
		super(HttpStatus.NOT_FOUND);
	}

}
