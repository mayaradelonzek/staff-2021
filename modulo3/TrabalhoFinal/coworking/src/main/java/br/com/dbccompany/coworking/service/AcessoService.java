package br.com.dbccompany.coworking.service;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.coworking.entity.AcessoEntity;
import br.com.dbccompany.coworking.exception.RecursoNaoEncontradoException;
import br.com.dbccompany.coworking.exception.SaldoInsuficienteException;
import br.com.dbccompany.coworking.repository.AcessoRepository;

@Service
public class AcessoService extends AbstractService<AcessoEntity, AcessoRepository, Integer> {

	public AcessoService() {
		super(AcessoService.class);
	}

	@Autowired
    private AcessoRepository acessoRepository;

    @Autowired
    private SaldoClienteService saldoClienteService;
    
    private AcessoEntity acesso;
    
    public List<AcessoEntity> buscarTodos() {
    	return buscarTodosAndLogar(acessoRepository);
    }
    
    public AcessoEntity buscarPorId(Integer id) {
    	return buscarPorIdAndLogar(id, acessoRepository);
    }

    @Transactional(noRollbackFor = {RuntimeException.class})
    public AcessoEntity entrar(AcessoEntity entrada){
    	this.acesso = entrada;
    	this.acesso.setSaldoCliente(saldoClienteService.buscarPorId(this.acesso.getSaldoCliente().getId()));
        if(this.acesso.getData() == null){
        	this.acesso.setData(LocalDateTime.now());
        }
        this.acesso.setIsEntrada(true);
        return salvarAndLogar(this.acesso, this.acessoRepository);
    }

    @Transactional
    public AcessoEntity sair(AcessoEntity saida){
    	this.acesso = saida;
        saida.setSaldoCliente(saldoClienteService.buscarPorId(this.acesso.getSaldoCliente().getId()));
        if(saida.getData() == null){
        	saida.setData(LocalDateTime.now());
        }
        saida.setIsEntrada(false);
        AcessoEntity entrada = acessoRepository.buscarAcessosDoCliente(saida.getSaldoCliente().getId());
        saida.getSaldoCliente().setQuantidade(atualizarSaldo(entrada, saida));
        this.saldoClienteService.salvar(saida.getSaldoCliente());
        return salvarAndLogar(saida, this.acessoRepository);
    }
    
    private Integer calcularDiferenca(AcessoEntity entrada, AcessoEntity saida) {
    	return (int) Duration.between(entrada.getData(), saida.getData()).toMinutes();
    }

    private Integer atualizarSaldo(AcessoEntity entrada, AcessoEntity saida){
        return saida.getSaldoCliente().getQuantidade() - calcularDiferenca(entrada, saida);
    }
    
	@Override
	protected void aplicarRegras() {
		if(this.acesso.getIsEntrada()) {
			verificarSeRecursoExiste();
			verificarSeClientePossuiSaldoValido();
		}
	}
	
	public void verificarSeRecursoExiste() {
		if(this.acesso.getSaldoCliente() == null) {
			throw new RecursoNaoEncontradoException();
		}
	}
	
	public void verificarSeClientePossuiSaldoValido() {
		Boolean isSaldoMenorIgualZero = acesso.getSaldoCliente().getQuantidade() <= 0;
		Boolean isSaldoInvalido = acesso.getSaldoCliente().getVencimento().isBefore(LocalDate.now());

		if (isSaldoMenorIgualZero || isSaldoInvalido) {
			if (isSaldoInvalido) {
				acesso.getSaldoCliente().setQuantidade(0);
				this.saldoClienteService.salvar(acesso.getSaldoCliente());	
			}
			throw new SaldoInsuficienteException(HttpStatus.BAD_REQUEST, "Saldo Insuficiente");
		}
	}
    
}
