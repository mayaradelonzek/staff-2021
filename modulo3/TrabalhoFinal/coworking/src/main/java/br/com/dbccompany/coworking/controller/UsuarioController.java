package br.com.dbccompany.coworking.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import javax.validation.Valid;

import br.com.dbccompany.coworking.entity.UsuarioEntity;
import br.com.dbccompany.coworking.service.UsuarioService;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

	@Autowired
	private UsuarioService usuarioService;
	
	@GetMapping("/")
	public List<UsuarioEntity> buscarTodos() {
		return this.usuarioService.buscarTodos();
	}
	
	@GetMapping("/{id}")
	public UsuarioEntity buscarPorId(@PathVariable Integer id) {
		return this.usuarioService.buscarPorId(id);
	}
	
	@PostMapping("/")
	public UsuarioEntity salvar(@RequestBody @Valid UsuarioEntity usuario) {
		return this.usuarioService.salvar(usuario);
	}
	
}
