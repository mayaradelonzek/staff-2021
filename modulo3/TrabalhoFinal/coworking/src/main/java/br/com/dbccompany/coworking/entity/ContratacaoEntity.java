package br.com.dbccompany.coworking.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.dbccompany.coworking.entity.enums.TipoContratacaoEnum;

@Entity
@Table(name = "Contratacao")
public class ContratacaoEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name = "id_Espacos")
	private EspacoEntity espacoEntity;
	
	@ManyToOne
	@JoinColumn(name = "id_Clientes")
	private ClienteEntity clienteEntity;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "tipo_contratacao")
	private TipoContratacaoEnum tipoContratacao;
	
	private Integer quantidade;
	
	@Column(nullable = true)
	private Integer desconto;
	
	private Integer prazo;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public EspacoEntity getEspacoEntity() {
		return espacoEntity;
	}

	public void setEspacoEntity(EspacoEntity espacoEntity) {
		this.espacoEntity = espacoEntity;
	}

	public ClienteEntity getClienteEntity() {
		return clienteEntity;
	}

	public void setClienteEntity(ClienteEntity clienteEntity) {
		this.clienteEntity = clienteEntity;
	}

	public TipoContratacaoEnum getTipoContratacao() {
		return tipoContratacao;
	}

	public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
		this.tipoContratacao = tipoContratacao;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public Integer getDesconto() {
		return desconto;
	}

	public void setDesconto(Integer desconto) {
		this.desconto = desconto;
	}

	public Integer getPrazo() {
		return prazo;
	}

	public void setPrazo(Integer prazo) {
		this.prazo = prazo;
	}
	
}
