package br.com.dbccompany.coworking.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.dbccompany.coworking.entity.PacoteEntity;

@Repository
public interface PacoteRepository extends CrudRepository<PacoteEntity, Integer> {

	@Query("from PacoteEntity p join fetch p.clientes cp join fetch cp.clienteEntity c where c.id = :id")
	public List<PacoteEntity> buscarPacotesPorCliente(@Param(value = "id") Integer clienteId);
	
}
