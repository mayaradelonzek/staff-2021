package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.InventarioDTO;
import br.com.dbccompany.lotr.DTO.Inventario_X_ItemDTO;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.Inventario_X_ItemEntity;
import br.com.dbccompany.lotr.Entity.Inventario_X_ItemId;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import br.com.dbccompany.lotr.Service.Inventario_X_ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/inventario_X_Item")
public class Inventario_X_ItemController {

    @Autowired
    private Inventario_X_ItemService service;

    @GetMapping( value = "/")
    @ResponseBody
    public List<Inventario_X_ItemEntity> trazerTodosOsInventariosItens() {
        return service.trazerTodosOsInventariosItens();
    }

    @GetMapping( value = "/{id}")
    @ResponseBody
    public Inventario_X_ItemEntity trazerInventarioXItemEspecifico(@RequestBody Inventario_X_ItemId id ) {
        return service.buscarPorId(id);
    }

    @PostMapping( value = "/salvar")
    @ResponseBody
    public Inventario_X_ItemEntity salvarInventarioXItem(@RequestBody Inventario_X_ItemDTO inventarioItem ) {
        return service.salvar( inventarioItem.converter() );
    }

    @PutMapping( value = "/editar")
    @ResponseBody
    public Inventario_X_ItemEntity editarInventarioXItem(@RequestBody Inventario_X_ItemDTO inventarioItemDTO) {
        return service.editar(inventarioItemDTO.converter());
    }

    @GetMapping( value = "/quantidade/{quantidade}")
    @ResponseBody
    public Inventario_X_ItemEntity buscarPorQuantidade(@PathVariable Integer quantidade) {
        return service.buscarPorQuantidade(quantidade);
    }

    @GetMapping( value = "/inventarios")
    @ResponseBody
    public List<Inventario_X_ItemEntity> buscarTodosPorInventario(@RequestBody InventarioDTO inventarioDTO) {
        return service.buscarTodosPorInventario(inventarioDTO.converter());
    }

    @GetMapping( value = "/itens")
    @ResponseBody
    public List<Inventario_X_ItemEntity> buscarTodosPorItem(@RequestBody ItemEntity item) {
        return service.buscarTodosPorItem(item);
    }

    @GetMapping( value = "/ids")
    @ResponseBody
    public List<Inventario_X_ItemEntity> buscarTodosPorIdEm(@RequestBody List<Inventario_X_ItemId> ids) {
        return service.buscarTodosPorIdEm(ids);
    }

    @GetMapping( value = "/quantidades/{quantidades}")
    @ResponseBody
    public List<Inventario_X_ItemEntity> buscarTodosPorQuantidade(@PathVariable Integer quantidades) {
        return service.buscarTodosPorQuantidade(quantidades);
    }
}
