package br.com.dbccompany.lotr.Repository;

import br.com.dbccompany.lotr.Entity.Inventario_X_ItemEntity;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ItemRepository extends CrudRepository<ItemEntity, Integer> {
    @Override
    Optional<ItemEntity> findById(Integer id );
    List<ItemEntity> findAllByIdIn(List<Integer> integers);
    ItemEntity findByDescricao(String descricao );
    List<ItemEntity> findAllByDescricaoIn(List<String> descricoes );
    ItemEntity findByInventarioItemIn(List<Inventario_X_ItemEntity> inventarioItem );
}
