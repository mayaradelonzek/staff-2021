package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.*;
import br.com.dbccompany.lotr.Service.AnaoService;
import br.com.dbccompany.lotr.Service.ElfoService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class InventarioDTO {

    @Autowired
    AnaoService anaoService;

    @Autowired
    ElfoService elfoService;

    private Integer id;
    private Integer idPersonagem;

    public InventarioDTO() { }

    public InventarioDTO(InventarioEntity inventarioEntity) {
        if(inventarioEntity != null) {
            this.id = inventarioEntity.getId();
            if (inventarioEntity.getPersonagem() != null) {
                this.idPersonagem = inventarioEntity.getPersonagem().getId();
            }
        }
    }

    public InventarioEntity converter() {
        InventarioEntity inventarioEntity = new InventarioEntity();
        inventarioEntity.setId(this.id);
        return inventarioEntity;
    }

    public static List<InventarioDTO> toListDTO(List<InventarioEntity> inventarios) {
        List<InventarioDTO> inventariosDto = new ArrayList<>();

        for(InventarioEntity inventario : inventarios) {
            inventariosDto.add(new InventarioDTO(inventario));
        }

        return inventariosDto;
    }

    public static List<InventarioEntity> toListEntity(List<InventarioDTO> inventariosDTO) {
        List<InventarioEntity> inventarios = new ArrayList<>();

        for(InventarioDTO inventarioDTO : inventariosDTO) {
            inventarios.add(inventarioDTO.converter());
        }

        return inventarios;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdPersonagem() {
        return idPersonagem;
    }

    public void setIdPersonagem(Integer idPersonagem) {
        this.idPersonagem = idPersonagem;
    }
}
