package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.ElfoDTO;
import br.com.dbccompany.lotr.Entity.AnaoEntity;
import br.com.dbccompany.lotr.Entity.ElfoEntity;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.StatusEnum;
import br.com.dbccompany.lotr.Repository.ElfoRepository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ElfoService extends PersonagemService<ElfoRepository, ElfoEntity> {

    @PersistenceContext
    private EntityManager manager;

    public List<ElfoEntity> trazerTodosOsPersonagens() {
        StringBuilder jpql = new StringBuilder();
        HashMap<String, Object> parametros = new HashMap<>();
        jpql.append("from ElfoEntity where dtype = :dtype ");
        parametros.put("dtype", "elfo");

        TypedQuery<ElfoEntity> query = manager
                .createQuery(jpql.toString(), ElfoEntity.class);

        for (Map.Entry<String, Object> parametro : parametros.entrySet()) {
            query.setParameter(parametro.getKey(), parametro.getValue());
        }

        return query.getResultList();
    }

    public ElfoDTO salvarElfo(ElfoEntity elfoEntity ) {
        ElfoEntity elfoNovo = repository.save(elfoEntity);
        InventarioEntity inventarioEntity = new InventarioEntity();
        inventarioEntity.setPersonagem(elfoNovo);
        inventarioEntity = inventarioRepository.save(inventarioEntity);
        elfoNovo.setInventario(inventarioEntity);
        elfoNovo = repository.save(elfoNovo);
        return new ElfoDTO( elfoNovo );
    }

    public ElfoEntity editar( ElfoEntity elfoEntity, Integer id ) {
        return super.editar( elfoEntity, id );
    }

    public ElfoEntity buscarPorId( Integer id ) {
        return super.buscarPorId(id);
    }

    public ElfoEntity buscarPorExperiencia( Integer exp ) {
        return super.buscarPorExperiencia(exp);
    }

    public ElfoEntity buscarPorInventario( InventarioEntity inventario ) {
        return super.buscarPorInventario(inventario);
    }

    public ElfoEntity buscarPorNome( String nome ) {
        return super.buscarPorNome(nome);
    }

    public ElfoEntity buscarPorQuantidadeDeDano( Double dano ) {
        return super.buscarPorQuantidadeDeDano(dano);
    }

    public ElfoEntity buscarPorStatus(StatusEnum status) {
        return super.buscarPorStatus(status);
    }

    public ElfoEntity buscarPorQuantidadeDeExpPorAtaque(Integer qtdXpAtq) {
        return super.buscarPorQuantidadeDeExpPorAtaque(qtdXpAtq);
    }
    public ElfoEntity buscarPorVida(Double vida) {
        return super.buscarPorVida(vida);
    }

    public List<ElfoEntity> buscarTodosPorExperienciaEm(List<Integer> exps) {
        return super.buscarTodosPorExperienciaEm(exps);
    }

    public List<ElfoEntity> buscarTodosPorIdEm(List<Integer> ids) {
        return super.buscarTodosPorIdEm(ids);
    }

    public List<ElfoEntity> buscarTodosPorNomeEm(List<String> nomes) {
        return super.buscarTodosPorNomeEm(nomes);
    }

    public List<ElfoEntity> buscarTodosPorDanoEm(List<Double> danos) {
        return super.buscarTodosPorDanoEm(danos);
    }

    public List<ElfoEntity> buscarTodosPorStatusEm(List<StatusEnum> status) {
        return super.buscarTodosPorStatusEm(status);
    }

    public List<ElfoEntity> buscarTodosPorQuantidadeDeDanoEm(List<Integer> qtdDano) {
        return super.buscarTodosPorQuantidadeDeDanoEm(qtdDano);
    }

    public List<ElfoEntity> buscarTodosPorVidaEm(List<Double> vidas) {
        return super.buscarTodosPorVidaEm(vidas);
    }
}
