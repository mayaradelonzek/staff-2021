package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.AnaoDTO;
import br.com.dbccompany.lotr.DTO.InventarioDTO;
import br.com.dbccompany.lotr.Entity.AnaoEntity;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.StatusEnum;
import br.com.dbccompany.lotr.Service.AnaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping( value = "/api/anao")
public class AnaoController {

    @Autowired
    private AnaoService service;

    @GetMapping( value = "/")
    @ResponseBody
    public List<AnaoDTO> trazerTodosOsAnoes() {
        return AnaoDTO.toListDTO(service.trazerTodosOsPersonagens());
    }

    @GetMapping( value = "/{id}")
    @ResponseBody
    public AnaoDTO trazerAnaoEspecifico( @PathVariable("id") Integer id ) {
        return new AnaoDTO(service.buscarPorId(id));
    }

    @PostMapping( value = "/salvar")
    @ResponseBody
    public AnaoDTO salvarAnao(@RequestBody AnaoDTO anaoDTO ) {
        return new AnaoDTO(service.salvar( anaoDTO.converter() ));
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public AnaoDTO editarAnao( @PathVariable("id") Integer id, @RequestBody AnaoDTO anaoDTO) {
        return new AnaoDTO(service.editar(anaoDTO.converter(), id));
    }

    @GetMapping( value = "/exp/{exp}")
    @ResponseBody
    public AnaoDTO buscarPorExperiencia(@PathVariable("exp") Integer exp ) {
        return new AnaoDTO(service.buscarPorExperiencia(exp));
    }

    @GetMapping( value = "/inventario")
    @ResponseBody
    public AnaoDTO buscarPorInventario( @RequestBody InventarioDTO inventarioDTO ) {
        return new AnaoDTO(service.buscarPorInventario(inventarioDTO.converter()));
    }

    @GetMapping( value = "/nome/{nome}")
    @ResponseBody
    public AnaoDTO buscarPorNome(@PathVariable("nome") String nome ) {
        return new AnaoDTO(service.buscarPorNome(nome));
    }

    @GetMapping( value = "/dano/{dano}")
    @ResponseBody
    public AnaoDTO buscarPorQuantidadeDeDano( @PathVariable("dano") Double dano ) {
        return new AnaoDTO(service.buscarPorQuantidadeDeDano(dano));
    }

    @GetMapping( value = "/status/{status}")
    @ResponseBody
    public AnaoDTO buscarPorStatus(@PathVariable("status") String status) {
        return new AnaoDTO(service.buscarPorStatus(StatusEnum.valueOf(status)));
    }

    @GetMapping( value = "/qtdXpAtq/{qtdXpAtq}")
    @ResponseBody
    public AnaoDTO buscarPorQuantidadeDeExpPorAtaque(@PathVariable("qtdXpAtq") Integer qtdXpAtq) {
        return new AnaoDTO(service.buscarPorQuantidadeDeExpPorAtaque(qtdXpAtq));
    }

    @GetMapping( value = "/vida/{vida}")
    @ResponseBody
    public AnaoDTO buscarPorVida(@PathVariable("vida") Double vida) {
        return new AnaoDTO(service.buscarPorVida(vida));
    }

    @GetMapping( value = "/exps/{exps}")
    @ResponseBody
    public List<AnaoDTO> buscarTodosPorExperienciaEm(@PathVariable("exps") String exps) {
        List<Integer> expsList = new ArrayList<>();

        for(String expStr : exps.split(",")) {
            expsList.add(Integer.getInteger(expStr));
        }

        return AnaoDTO.toListDTO(service.buscarTodosPorExperienciaEm(expsList));
    }

    @GetMapping( value = "/ids/{ids}")
    @ResponseBody
    public List<AnaoDTO> buscarTodosPorIdEm(@PathVariable String ids) {
        List<Integer> idsList = new ArrayList<>();

        for(String idStr : ids.split(",")) {
            idsList.add(Integer.getInteger(idStr));
        }

        return AnaoDTO.toListDTO(service.buscarTodosPorIdEm(idsList));
    }

    @GetMapping( value = "/nomes/{nomes}")
    @ResponseBody
    public List<AnaoDTO> buscarTodosPorNomeEm(@PathVariable String nomes) {
        List<String> nomesList = new ArrayList<>();

        for(String nome : nomes.split(",")) {
            nomesList.add(nome);
        }

        return AnaoDTO.toListDTO(service.buscarTodosPorNomeEm(nomesList));
    }

    @GetMapping( value = "/danos/{danos}")
    @ResponseBody
    public List<AnaoDTO> buscarTodosPorDanoEm(@PathVariable List<Double> danos) {
        return AnaoDTO.toListDTO(service.buscarTodosPorDanoEm(danos));
    }

    @GetMapping( value = "/status/varios/{status}")
    @ResponseBody
    public List<AnaoDTO> buscarTodosPorStatusEm(@PathVariable("status") String status) {
        List<StatusEnum> statusList = new ArrayList<>();

        for(String statusStr : status.split(",")) {
            statusList.add(StatusEnum.valueOf(statusStr));
        }

        return AnaoDTO.toListDTO(service.buscarTodosPorStatusEm(statusList));
    }

    @GetMapping( value = "/qtdDanos/{qtdDanos}")
    @ResponseBody
    public List<AnaoDTO> buscarTodosPorQuantidadeDeDanoEm(@PathVariable("qtdDanos") String qtdDano) {
        List<Integer> qtdDanoList = new ArrayList<>();

        for(String qtdDanoStr : qtdDano.split(",")) {
            qtdDanoList.add(Integer.getInteger(qtdDanoStr));
        }

        return AnaoDTO.toListDTO(service.buscarTodosPorQuantidadeDeDanoEm(qtdDanoList));
    }

    @GetMapping( value = "/vidas/{vidas}")
    @ResponseBody
    public List<AnaoDTO> buscarTodosPorVidaEm(@PathVariable String vidas) {
        List<Double> vidaList = new ArrayList<>();

        for(String vidaStr : vidas.split(",")) {
            vidaList.add(Double.parseDouble(vidaStr));
        }

        return AnaoDTO.toListDTO(service.buscarTodosPorVidaEm(vidaList));
    }
}
