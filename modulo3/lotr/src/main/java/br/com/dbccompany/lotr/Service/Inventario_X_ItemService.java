package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.Entity.*;
import br.com.dbccompany.lotr.Repository.Inventario_X_ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class Inventario_X_ItemService {

    @Autowired
    private Inventario_X_ItemRepository repository;

    public List<Inventario_X_ItemEntity> trazerTodosOsInventariosItens() {
        return (List<Inventario_X_ItemEntity>) repository.findAll();
    }

    @Transactional( rollbackFor = Exception.class )
    public Inventario_X_ItemEntity salvar( Inventario_X_ItemEntity inventarioItem ) {
        return this.salvarEEditar( inventarioItem );
    }

    @Transactional( rollbackFor = Exception.class )
    public Inventario_X_ItemEntity editar( Inventario_X_ItemEntity inventarioItem) {
        return this.salvarEEditar( inventarioItem );
    }

    public Inventario_X_ItemEntity salvarEEditar( Inventario_X_ItemEntity inventarioItem ) {
        return repository.save( inventarioItem );
    }

    public Inventario_X_ItemEntity buscarPorId( Inventario_X_ItemId id ) {
        Optional<Inventario_X_ItemEntity> inventario = repository.findById(id);
        if ( inventario.isPresent() ) {
            return repository.findById(id).get();
        }
        return null;
    }

    public Inventario_X_ItemEntity buscarPorQuantidade(Integer quantidade) {
        return this.repository.findByQuantidade(quantidade);
    }

    public List<Inventario_X_ItemEntity> buscarTodosPorInventario(InventarioEntity inventario) {
        return this.repository.findAllByInventario(inventario);
    }

    public List<Inventario_X_ItemEntity> buscarTodosPorItem(ItemEntity item) {
        return this.repository.findAllByItem(item);
    }

    public List<Inventario_X_ItemEntity> buscarTodosPorIdEm(List<Inventario_X_ItemId> ids) {
        return this.repository.findAllByIdIn(ids);
    }

    public List<Inventario_X_ItemEntity> buscarTodosPorQuantidade(Integer quantidades) {
        return this.repository.findAllByQuantidade(quantidades);
    }

}
