package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.ItemDTO;
import br.com.dbccompany.lotr.Entity.Inventario_X_Item;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import br.com.dbccompany.lotr.Exception.ItemNaoEncontrado;
import br.com.dbccompany.lotr.Repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ItemService {

    @Autowired
    private ItemRepository repository;

    public List<ItemEntity> trazerTodosOsItens() {
        return (List<ItemEntity>) repository.findAll();
    }

    @Transactional( rollbackFor = Exception.class )
    public ItemEntity salvar(ItemEntity item ) {
        return repository.save(item);
    }

    @Transactional( rollbackFor = Exception.class )
    public ItemDTO editar( ItemEntity item, Integer id ) {
        ItemEntity itemAtual = this.repository.findById(id).get();

        itemAtual.setDescricao(item.getDescricao());
        if(item.getInventarioItem() != null) {
            itemAtual.setInventarioItem(item.getInventarioItem());
        }

        return new ItemDTO(this.salvarEEditar( itemAtual ));
    }

    private ItemEntity salvarEEditar( ItemEntity item ) {
        return repository.save( item );
    }

    public ItemEntity buscarPorId( Integer id ) throws ItemNaoEncontrado {
        Optional<ItemEntity> item = repository.findById(id);

        if (!item.isPresent()) {
            throw new ItemNaoEncontrado();
        }

        return item.get();
    }

    public ItemDTO buscaPorDescricao(String descricao) {
        return new ItemDTO(this.repository.findByDescricao(descricao));
    }

    public ItemDTO buscaPorInventarioItem(List<Inventario_X_Item> inventarioItem) {
        return new ItemDTO(this.repository.findByInventarioItemIn(inventarioItem));
    }

    public List<ItemDTO> buscaTodosPorDescricaoEm(List<String> descricoes) {
        List<ItemEntity> itens = this.repository.findAllByDescricaoIn(descricoes);
        return ItemDTO.toListDTO(itens);
    }

    public List<ItemDTO> buscaTodosPorIdEm(List<Integer> ids) {
        List<ItemEntity> itens = this.repository.findAllByIdIn(ids);
        return ItemDTO.toListDTO(itens);
    }

}
