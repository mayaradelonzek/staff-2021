package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.Inventario_X_ItemEntity;
import br.com.dbccompany.lotr.Entity.Inventario_X_ItemId;
import br.com.dbccompany.lotr.Entity.ItemEntity;

import java.util.ArrayList;
import java.util.List;

public class Inventario_X_ItemDTO {

    private Integer idInventario;
    private Integer idItem;
    private Integer quantidade;

    public Inventario_X_ItemDTO() { }

    public Inventario_X_ItemDTO(Inventario_X_ItemEntity entity) {
        this.idItem = entity.getItem().getId();
        this.idInventario = entity.getInventario().getId();
        this.quantidade = entity.getQuantidade();
    }

    public Inventario_X_ItemEntity converter() {
        Inventario_X_ItemEntity itemQtd = new Inventario_X_ItemEntity();
        ItemEntity ie = new ItemEntity();
        InventarioEntity inve = new InventarioEntity();
        ie.setId(this.idItem);
        inve.setId(this.idInventario);
        itemQtd.setId( new Inventario_X_ItemId(this.idInventario, this.idItem));
        itemQtd.setQuantidade(this.quantidade);

        return itemQtd;
    }

    public static List<Inventario_X_ItemDTO> toListDTO(List<Inventario_X_ItemEntity> itensQtd) {
        List<Inventario_X_ItemDTO> itensQtdDto = new ArrayList<>();

        for(Inventario_X_ItemEntity itemQtd : itensQtd) {
            itensQtdDto.add(new Inventario_X_ItemDTO(itemQtd));
        }

        return itensQtdDto;
    }

    public static List<Inventario_X_ItemEntity> toListEntity(List<Inventario_X_ItemDTO> itensQtdDTO) {
        List<Inventario_X_ItemEntity> itensQtd = new ArrayList<>();

        for(Inventario_X_ItemDTO itemQtdDTO : itensQtdDTO) {
            itensQtd.add(itemQtdDTO.converter());
        }

        return itensQtd;
    }

}
