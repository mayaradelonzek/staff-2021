package br.com.dbccompany.lotr.Repository;

import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.Inventario_X_ItemEntity;
import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface InventarioRepository extends CrudRepository<InventarioEntity, Integer> {
    @Override
    Optional<InventarioEntity> findById(Integer id );
    List<InventarioEntity> findAllById(Iterable<Integer> integers);
    InventarioEntity findByInventarioItemIn(List<Inventario_X_ItemEntity> inventarioItem );
    InventarioEntity findByPersonagem(PersonagemEntity personagem);


}
