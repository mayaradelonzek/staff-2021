package br.com.dbccompany.lotr.Exception;

public class ErroSalvarInventario extends Exception {
    public ErroSalvarInventario() {
        super( "Erro ao salvar inventário, campos obrigatórios não preenchidos!" );
    }
}
