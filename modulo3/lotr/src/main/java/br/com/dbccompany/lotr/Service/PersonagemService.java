package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import br.com.dbccompany.lotr.Entity.StatusEnum;
import br.com.dbccompany.lotr.Repository.InventarioRepository;
import br.com.dbccompany.lotr.Repository.PersonagemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public abstract class PersonagemService<R extends PersonagemRepository<E>, E extends PersonagemEntity> {
    @Autowired
    protected R repository;

    @Autowired
    protected InventarioRepository inventarioRepository;

    protected abstract List<? extends PersonagemEntity> trazerTodosOsPersonagens();

    @Transactional( rollbackFor = Exception.class )
    public E salvar( E e ) {
        return this.salvarEEditar( e );
    }

    @Transactional( rollbackFor = Exception.class )
    public E editar( E e, Integer id ) {
        E atual  = this.buscarPorId(id);
        if(atual != null) {
            atual.setExperiencia(e.getExperiencia());
            atual.setInventario(e.getInventario());
            atual.setNome(e.getNome());
            atual.setStatus(e.getStatus());
            atual.setQtdDano(e.getQtdDano());
            atual.setVida(e.getVida());
            return this.salvarEEditar( atual );
        }
        return atual;
    }

    public E salvarEEditar( E personagem ) {
        return repository.save( personagem );
    }

    public E buscarPorId( Integer id ) {
        Optional<E> personagem = repository.findById(id);
        return personagem.orElse(null);
    }

    public E buscarPorExperiencia( Integer exp ) {
        return repository.findByExperiencia(exp);
    }

    public E buscarPorInventario( InventarioEntity inventario ) {
        return repository.findByInventario(inventario);
    }

    public E buscarPorNome( String nome ) {
        return repository.findByNome(nome);
    }

    public E buscarPorQuantidadeDeDano( Double dano ) {
        return repository.findByQtdDano(dano);
    }

    public E buscarPorStatus(StatusEnum status) {
        return repository.findByStatus(status);
    }

    public E buscarPorQuantidadeDeExpPorAtaque(Integer qtdXpAtq) {
        return repository.findByQtdXpAtaque(qtdXpAtq);
    }
    public E buscarPorVida(Double vida) {
        return repository.findByVida(vida);
    }

    public List<E> buscarTodosPorExperienciaEm(List<Integer> exps) {
        return repository.findAllByExperienciaIn(exps);
    }

    public List<E> buscarTodosPorIdEm(List<Integer> ids) {
        return repository.findAllByIdIn(ids);
    }

    public List<E> buscarTodosPorNomeEm(List<String> nomes) {
        return repository.findAllByNomeIn(nomes);
    }

    public List<E> buscarTodosPorDanoEm(List<Double> danos) {
        return repository.findAllByQtdDanoIn(danos);
    }

    public List<E> buscarTodosPorStatusEm(List<StatusEnum> status) {
        return repository.findAllByStatusIn(status);
    }

    public List<E> buscarTodosPorQuantidadeDeDanoEm(List<Integer> qtdDano) {
        return repository.findAllByQtdXpAtaqueIn(qtdDano);
    }

    public List<E> buscarTodosPorVidaEm(List<Double> vidas) {
        return repository.findAllByVidaIn(vidas);
    }
}
