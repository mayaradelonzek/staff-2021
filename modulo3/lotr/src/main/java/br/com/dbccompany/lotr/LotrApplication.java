package br.com.dbccompany.lotr;

import br.com.dbccompany.lotr.Entity.ItemEntity;
import br.com.dbccompany.lotr.Repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LotrApplication {

	public static void main(String[] args) {

		SpringApplication.run(LotrApplication.class, args);
	}

}
