package br.com.dbccompany.lotr.Entity;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.List;

@Entity
public class InventarioEntity {
    @Id
    @SequenceGenerator( name = "INVENTARIO_SEQ", sequenceName = "INVENTARIO_SEQ" )
    @GeneratedValue( generator = "INVENTARIO_SEQ", strategy = GenerationType.SEQUENCE)
    protected Integer id;

    @OneToMany( mappedBy = "inventario")
    private List<Inventario_X_ItemEntity> inventarioItem;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_personagem")
    private PersonagemEntity personagem;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Inventario_X_ItemEntity> getInventarioItem() {
        return inventarioItem;
    }

    public void setInventarioItem(List<Inventario_X_ItemEntity> inventarioItem) {
        this.inventarioItem = inventarioItem;
    }

    public PersonagemEntity getPersonagem() {
        return personagem;
    }

    public void setPersonagem(PersonagemEntity personagem) {
        this.personagem = personagem;
    }
}
