package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.InventarioDTO;
import br.com.dbccompany.lotr.DTO.Inventario_X_ItemDTO;
import br.com.dbccompany.lotr.DTO.PersonagemDTO;
import br.com.dbccompany.lotr.Exception.InventarioNaoEncontrado;
import br.com.dbccompany.lotr.Service.InventarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping( value = "/api/inventario")
public class InventarioController {

    @Autowired
    private InventarioService service;

    @GetMapping( value = "/")
    @ResponseBody
    public List<InventarioDTO> trazerTodosOsInventarios() {
        return InventarioDTO.toListDTO(service.trazerTodosOsInventarios());
    }

    @GetMapping( value = "/{id}")
    @ResponseBody
    public ResponseEntity<InventarioDTO> trazerInventarioEspecifico(@PathVariable Integer id) {
        try {
            return ResponseEntity.ok(new InventarioDTO(service.buscarPorId(id)));
        } catch (InventarioNaoEncontrado e) {
            System.out.println(e.getMensagem());
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping( value = "/salvar")
    @ResponseBody
    public InventarioDTO salvar( @RequestBody InventarioDTO inventario ) {
        return new InventarioDTO(service.salvar(inventario));
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public InventarioDTO editar( @RequestBody InventarioDTO inventario, @PathVariable Integer id ) {
        return new InventarioDTO(service.editar(inventario.converter(), id));
    }

    @GetMapping( value = "/personagem")
    @ResponseBody
    public InventarioDTO bucarPorPersonagem(@RequestBody PersonagemDTO personagemDTO) {
        return new InventarioDTO(service.bucarPorPersonagem(personagemDTO.converterP()));
    }

    @GetMapping( value = "/inventarioItem")
    @ResponseBody
    public InventarioDTO bucarPorInventarioItem(@RequestBody List<Inventario_X_ItemDTO> inventarioItemDTO) {
        return new InventarioDTO(service.bucarPorInventarioItem(Inventario_X_ItemDTO.toListEntity(inventarioItemDTO)));
    }

    @GetMapping( value = "/ids/{ids}")
    @ResponseBody
    public List<InventarioDTO> bucarTodosPoridEm(@PathVariable String ids) {
        List<Integer> idsList = new ArrayList<>();

        for(String idStr : ids.split(",")) {
            idsList.add(Integer.getInteger(idStr));
        }

        return InventarioDTO.toListDTO(service.bucarTodosPoridEm(idsList));
    }
}
