package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.Inventario_X_ItemDTO;
import br.com.dbccompany.lotr.DTO.ItemDTO;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import br.com.dbccompany.lotr.Exception.ItemNaoEncontrado;
import br.com.dbccompany.lotr.Service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping( value = "/api/item")
public class ItemController {

    @Autowired
    private ItemService service;

    @GetMapping( value = "/")
    @ResponseBody
    public List<ItemEntity> trazerTodosItens() {
        return service.trazerTodosOsItens();
    }

    @GetMapping("/{id}")
    @ResponseBody
    public ResponseEntity<ItemEntity> trazerItemEspecifico(@PathVariable("id") Integer id ) {
        try {
            return ResponseEntity.ok(service.buscarPorId(id));
        } catch (ItemNaoEncontrado e) {
            System.out.println(e.getMensagem());
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping( value = "/salvar")
    @ResponseBody
    public ItemDTO salvarItem(@RequestBody ItemDTO item ) {
        return new ItemDTO(service.salvar( item.converter() ));
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ItemDTO editarItem( @PathVariable("id") Integer id, @RequestBody ItemDTO item ) {
        return service.editar(item.converter(), id);
    }

    @GetMapping( value = "/descricao/{descricao}")
    @ResponseBody
    public ItemDTO buscaPorDescricao(@PathVariable("descricao") String descricao) {
        return service.buscaPorDescricao(descricao);
    }

    @GetMapping( value = "/inventarioItem")
    @ResponseBody
    public ItemDTO buscaPorInventarioItem(@RequestBody List<Inventario_X_ItemDTO> inventarioItemDTO) {
        return service.buscaPorInventarioItem(Inventario_X_ItemDTO.toListEntity(inventarioItemDTO));
    }

    @GetMapping( value = "/descricao/varios/{descricoes}")
    @ResponseBody
    public List<ItemDTO> buscaTodosPorDescricaoEm(@PathVariable("descricoes") String descricoes) {
        return service.buscaTodosPorDescricaoEm(Arrays.asList(descricoes.split(",")));
    }

    @GetMapping( value = "/buscar/varios/{ids}")
    @ResponseBody
    public List<ItemDTO> buscaTodosPorIdEm(@PathVariable("ids") String ids) {
        List<Integer> idsList = new ArrayList<>();

        for(String idStr : ids.split(",")) {
            idsList.add(Integer.getInteger(idStr));
        }

        return service.buscaTodosPorIdEm(idsList);
    }
}
