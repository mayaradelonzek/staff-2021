package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.AnaoEntity;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import br.com.dbccompany.lotr.Entity.StatusEnum;

public class PersonagemDTO {
    protected String nome;
    protected Integer experiencia;
    protected Double vida;
    protected Double qtdDano;
    protected Integer qtdXpAtaque;
    protected StatusEnum status;
    protected InventarioEntity inventario;

    public AnaoEntity converterP() {
        AnaoEntity anao = new AnaoEntity(this.nome);
        anao.setNome(this.nome);
        anao.setExperiencia(this.experiencia);
        anao.setInventario(this.inventario);
        anao.setVida(this.vida);
        anao.setQtdDano(this.qtdDano);
        anao.setQtdXpAtaque(this.qtdXpAtaque);
        anao.setStatus(this.status);
        return anao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getExperiencia() {
        return experiencia;
    }

    public void setExperiencia(Integer experiencia) {
        this.experiencia = experiencia;
    }

    public Double getVida() {
        return vida;
    }

    public void setVida(Double vida) {
        this.vida = vida;
    }

    public Double getQtdDano() {
        return qtdDano;
    }

    public void setQtdDano(Double qtdDano) {
        this.qtdDano = qtdDano;
    }

    public Integer getQtdXpAtaque() {
        return qtdXpAtaque;
    }

    public void setQtdXpAtaque(Integer qtdXpAtaque) {
        this.qtdXpAtaque = qtdXpAtaque;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    public InventarioEntity getInventario() {
        return inventario;
    }

    public void setInventario(InventarioEntity inventario) {
        this.inventario = inventario;
    }
}
