package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.*;

import java.util.ArrayList;
import java.util.List;

public class ElfoDTO extends PersonagemDTO {

    public ElfoDTO() {
    }

    public ElfoDTO(ElfoEntity elfoEntity) {
        if(elfoEntity != null) {
            this.nome = elfoEntity.getNome();
            this.experiencia = elfoEntity.getExperiencia();
            this.inventario = elfoEntity.getInventario();
            this.qtdXpAtaque = elfoEntity.getQtdXpAtaque();
            this.qtdDano = elfoEntity.getQtdDano();
            this.vida = elfoEntity.getVida();
        }
    }

    public ElfoEntity converter() {
        ElfoEntity elfo = new ElfoEntity(this.nome);
        elfo.setNome(this.nome);
        elfo.setExperiencia(this.experiencia);
        elfo.setInventario(this.inventario);
        elfo.setVida(this.vida);
        elfo.setQtdDano(this.qtdDano);
        elfo.setQtdXpAtaque(this.qtdXpAtaque);
        elfo.setStatus(this.status);
        return elfo;
    }

    public static List<ElfoDTO> toListDTO(List<ElfoEntity> elfos) {
        List<ElfoDTO> elfosDto = new ArrayList<>();

        for(ElfoEntity elfo : elfos) {
            elfosDto.add(new ElfoDTO(elfo));
        }

        return elfosDto;
    }

    public static List<ElfoEntity> toListEntity(List<ElfoDTO> elfosDTO) {
        List<ElfoEntity> elfos = new ArrayList<>();

        for(ElfoDTO elfoDTO : elfosDTO) {
            elfos.add(elfoDTO.converter());
        }

        return elfos;
    }

}
