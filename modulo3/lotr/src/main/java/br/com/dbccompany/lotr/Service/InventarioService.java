package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.InventarioDTO;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.Inventario_X_Item;
import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import br.com.dbccompany.lotr.Exception.InventarioNaoEncontrado;
import br.com.dbccompany.lotr.Repository.InventarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class InventarioService {

    @Autowired
    private InventarioRepository repository;

    @Autowired
    private ElfoService elfoService;

    @Autowired
    private AnaoService anaoService;

    public List<InventarioEntity> trazerTodosOsInventarios() {
        return (List<InventarioEntity>) repository.findAll(); //dei cast pq nao sobescrevi no repository. lá está generico. qnd arrumar no repository dai nao precisa o cast.
    }

    @Transactional( rollbackFor = Exception.class )
    public InventarioEntity salvar( InventarioDTO inventarioDTO ) {
        PersonagemEntity personagem = anaoService.buscarPorId(inventarioDTO.getIdPersonagem());
        personagem = personagem == null ? elfoService.buscarPorId(inventarioDTO.getIdPersonagem()) : personagem;
        InventarioEntity inventario = inventarioDTO.converter();
        inventario.setPersonagem(personagem);
        return this.salvarEEditar( inventario );
    }

    @Transactional( rollbackFor = Exception.class )
    public InventarioEntity editar( InventarioEntity inventario, Integer id ) {
        inventario.setId(id);
        return this.salvarEEditar( inventario );
    }

    public InventarioEntity salvarEEditar( InventarioEntity inventario ) {
        return repository.save( inventario );
    }

    public InventarioEntity buscarPorId( Integer id ) throws InventarioNaoEncontrado {
        Optional<InventarioEntity> inventario = repository.findById(id);
        if ( !inventario.isPresent() ) {
            throw new InventarioNaoEncontrado();
        }
        return repository.findById(id).get();
    }

    public InventarioEntity bucarPorPersonagem(PersonagemEntity personagem) {
        return this.repository.findByPersonagem(personagem);
    }

    public InventarioEntity bucarPorInventarioItem(List<Inventario_X_Item> inventarioItem) {
        return this.repository.findByInventarioItemIn(inventarioItem);
    }

    public List<InventarioEntity> bucarTodosPoridEm(List<Integer> ids) {
        return this.repository.findAllById(ids);
    }

}
