package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.AnaoDTO;
import br.com.dbccompany.lotr.Entity.AnaoEntity;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.StatusEnum;
import br.com.dbccompany.lotr.Repository.AnaoRepository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.*;

@Service
public class AnaoService extends PersonagemService<AnaoRepository, AnaoEntity> {

    @PersistenceContext
    private EntityManager manager;

    public AnaoDTO salvarAnao(AnaoEntity anaoEntity ) {
        AnaoEntity anaoNovo = repository.save(anaoEntity);
        InventarioEntity inventarioEntity = new InventarioEntity();
        inventarioEntity.setPersonagem(anaoNovo);
        inventarioEntity = inventarioRepository.save(inventarioEntity);
        anaoNovo.setInventario(inventarioEntity);
        anaoNovo = repository.save(anaoNovo);
        return new AnaoDTO( anaoNovo );
    }

    public List<AnaoEntity> trazerTodosOsPersonagens() {
        StringBuilder jpql = new StringBuilder();
        HashMap<String, Object> parametros = new HashMap<>();
        jpql.append("from AnaoEntity where dtype = :dtype ");
        parametros.put("dtype", "anao");

        TypedQuery<AnaoEntity> query = manager
                .createQuery(jpql.toString(), AnaoEntity.class);

        for (Map.Entry<String, Object> parametro : parametros.entrySet()) {
            query.setParameter(parametro.getKey(), parametro.getValue());
        }

        return query.getResultList();
    }

    public AnaoDTO editarAnao( AnaoEntity anaoEntity, Integer id ) {
        return new AnaoDTO(super.editar( anaoEntity, id ));
    }

    public AnaoEntity buscarPorId( Integer id ) {
        return super.buscarPorId(id);
    }

    public AnaoEntity buscarPorExperiencia( Integer exp ) {
        return super.buscarPorExperiencia(exp);
    }

    public AnaoEntity buscarPorInventario( InventarioEntity inventario ) {
        return super.buscarPorInventario(inventario);
    }

    public AnaoEntity buscarPorNome( String nome ) {
        return super.buscarPorNome(nome);
    }

    public AnaoEntity buscarPorQuantidadeDeDano( Double dano ) {
        return super.buscarPorQuantidadeDeDano(dano);
    }

    public AnaoEntity buscarPorStatus(StatusEnum status) {
        return super.buscarPorStatus(status);
    }

    public AnaoEntity buscarPorQuantidadeDeExpPorAtaque(Integer qtdXpAtq) {
        return super.buscarPorQuantidadeDeExpPorAtaque(qtdXpAtq);
    }
    public AnaoEntity buscarPorVida(Double vida) {
        return super.buscarPorVida(vida);
    }

    public List<AnaoEntity> buscarTodosPorExperienciaEm(List<Integer> exps) {
        return super.buscarTodosPorExperienciaEm(exps);
    }

    public List<AnaoEntity> buscarTodosPorIdEm(List<Integer> ids) {
        return super.buscarTodosPorIdEm(ids);
    }

    public List<AnaoEntity> buscarTodosPorNomeEm(List<String> nomes) {
        return super.buscarTodosPorNomeEm(nomes);
    }

    public List<AnaoEntity> buscarTodosPorDanoEm(List<Double> danos) {
        return super.buscarTodosPorDanoEm(danos);
    }

    public List<AnaoEntity> buscarTodosPorStatusEm(List<StatusEnum> status) {
        return super.buscarTodosPorStatusEm(status);
    }

    public List<AnaoEntity> buscarTodosPorQuantidadeDeDanoEm(List<Integer> qtdDano) {
        return super.buscarTodosPorQuantidadeDeDanoEm(qtdDano);
    }

    public List<AnaoEntity> buscarTodosPorVidaEm(List<Double> vidas) {
        return super.buscarTodosPorVidaEm(vidas);
    }
}
