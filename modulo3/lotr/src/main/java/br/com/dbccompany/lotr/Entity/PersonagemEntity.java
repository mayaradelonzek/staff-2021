package br.com.dbccompany.lotr.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Inheritance( strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorValue( value = "personagem" )
public abstract class PersonagemEntity {
    @Id
    @SequenceGenerator( name = "PERSONAGEM_SEQ", sequenceName = "PERSONAGEM_SEQ" )
    @GeneratedValue( generator = "PERSONAGEM_SEQ", strategy = GenerationType.SEQUENCE)
    @Column( name = "ID_PERSONAGEM" )
    protected Integer id;

    @Column( name = "NOME", nullable = false, unique = true, length = 20 )
    protected String nome;

    @Column( name = "EXPERIENCIA", columnDefinition = "Integer default 0", nullable = false )
    protected Integer experiencia = 0;

    @Column( name = "VIDA", nullable = false, scale = 2, precision = 4 )
    protected Double vida;

    @Column( name = "QTD_DANO", nullable = false )
    protected Double qtdDano = 0.0;

    @Column( name = "QTD_EXPERIENCIA_POR_ATAQUE", nullable = false )
    protected Integer qtdXpAtaque = 1;

    @Enumerated( EnumType.STRING )
    protected StatusEnum status;

    public PersonagemEntity(String nome) {
        this.nome = nome;
    }

    @OneToOne( cascade = CascadeType.ALL)
    @JoinColumn( name = "id_inventario")
    private InventarioEntity inventario;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getExperiencia() {
        return experiencia;
    }

    public void setExperiencia(Integer experiencia) {
        this.experiencia = experiencia;
    }

    public Double getVida() {
        return vida;
    }

    public void setVida(Double vida) {
        this.vida = vida;
    }

    public Double getQtdDano() {
        return qtdDano;
    }

    public void setQtdDano(Double qtdDano) {
        this.qtdDano = qtdDano;
    }

    public Integer getQtdXpAtaque() {
        return qtdXpAtaque;
    }

    public void setQtdXpAtaque(Integer qtdXpAtaque) {
        this.qtdXpAtaque = qtdXpAtaque;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    public InventarioEntity getInventario() {
        return inventario;
    }

    public void setInventario(InventarioEntity inventario) {
        this.inventario = inventario;
    }
}
