package br.com.dbccompany.lotr.Repository;

import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import br.com.dbccompany.lotr.Entity.StatusEnum;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PersonagemRepository<T extends PersonagemEntity> extends CrudRepository<T, Integer> {
    @Override
    Optional<T> findById( Integer id );
    List<T> findAllByIdIn( List<Integer> ids );
    T findByNome(String nome );
    List<T> findAllByNomeIn( List<String> nome );
    T findByExperiencia(Integer experiencia );
    List<T> findAllByExperienciaIn( List<Integer> experiencia );
    T findByVida(Double vida);
    List<T> findAllByVidaIn( List<Double> vida );
    T findByQtdDano(Double qtdDano);
    List<T> findAllByQtdDanoIn( List<Double> qtdDano );
    T findByQtdXpAtaque(Integer qtdXpAtaque);
    List<T> findAllByQtdXpAtaqueIn( List<Integer> qtdXpAtaque );
    T findByStatus(StatusEnum status);
    List<T> findAllByStatusIn( List<StatusEnum> status );
    T findByInventario(InventarioEntity inventario);
}
