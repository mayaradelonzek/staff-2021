package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.ElfoDTO;
import br.com.dbccompany.lotr.DTO.InventarioDTO;
import br.com.dbccompany.lotr.Entity.ElfoEntity;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.StatusEnum;
import br.com.dbccompany.lotr.Service.ElfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping( value = "/api/elfo")
public class ElfoController {

    @Autowired
    private ElfoService service;

    @GetMapping( value = "/")
    @ResponseBody
    public List<ElfoDTO> trazerTodosOsElfos() {
        return ElfoDTO.toListDTO(service.trazerTodosOsPersonagens());
    }

    @GetMapping( value = "/{id}")
    @ResponseBody
    public ElfoDTO trazerElfoEspecifico( @PathVariable("id") Integer id ) {
        return new ElfoDTO(service.buscarPorId(id));
    }

    @PostMapping( value = "/salvar")
    @ResponseBody
    public ElfoDTO salvarElfo(@RequestBody ElfoDTO elfoDTO ) {
        return new ElfoDTO(service.salvar( elfoDTO.converter() ));
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public ElfoDTO editarElfo( @PathVariable("id") Integer id, @RequestBody ElfoDTO elfoDTO) {
        return new ElfoDTO(service.editar(elfoDTO.converter(), id));
    }

    @GetMapping( value = "/exp/{exp}")
    @ResponseBody
    public ElfoDTO buscarPorExperiencia(@PathVariable("exp") Integer exp ) {
        return new ElfoDTO(service.buscarPorExperiencia(exp));
    }

    @GetMapping( value = "/inventario")
    @ResponseBody
    public ElfoDTO buscarPorInventario( @RequestBody InventarioDTO inventarioDTO ) {
        return new ElfoDTO(service.buscarPorInventario(inventarioDTO.converter()));
    }

    @GetMapping( value = "/nome/{nome}")
    @ResponseBody
    public ElfoDTO buscarPorNome(@PathVariable("nome") String nome ) {
        return new ElfoDTO(service.buscarPorNome(nome));
    }

    @GetMapping( value = "/dano/{dano}")
    @ResponseBody
    public ElfoDTO buscarPorQuantidadeDeDano( @PathVariable("dano") Double dano ) {
        return new ElfoDTO(service.buscarPorQuantidadeDeDano(dano));
    }

    @GetMapping( value = "/status/{status}")
    @ResponseBody
    public ElfoDTO buscarPorStatus(@PathVariable("status") String status) {
        return new ElfoDTO(service.buscarPorStatus(StatusEnum.valueOf(status)));
    }

    @GetMapping( value = "/qtdXpAtq/{qtdXpAtq}")
    @ResponseBody
    public ElfoDTO buscarPorQuantidadeDeExpPorAtaque(@PathVariable("qtdXpAtq") Integer qtdXpAtq) {
        return new ElfoDTO(service.buscarPorQuantidadeDeExpPorAtaque(qtdXpAtq));
    }

    @GetMapping( value = "/vida/{vida}")
    @ResponseBody
    public ElfoDTO buscarPorVida(@PathVariable("vida") Double vida) {
        return new ElfoDTO(service.buscarPorVida(vida));
    }

    @GetMapping( value = "/exps/{exps}")
    @ResponseBody
    public List<ElfoDTO> buscarTodosPorExperienciaEm(@PathVariable("exps") String exps) {
        List<Integer> expsList = new ArrayList<>();

        for(String expStr : exps.split(",")) {
            expsList.add(Integer.getInteger(expStr));
        }

        return ElfoDTO.toListDTO(service.buscarTodosPorExperienciaEm(expsList));
    }

    @GetMapping( value = "/ids/{ids}")
    @ResponseBody
    public List<ElfoDTO> buscarTodosPorIdEm(@PathVariable String ids) {
        List<Integer> idsList = new ArrayList<>();

        for(String idStr : ids.split(",")) {
            idsList.add(Integer.getInteger(idStr));
        }

        return ElfoDTO.toListDTO(service.buscarTodosPorIdEm(idsList));
    }

    @GetMapping( value = "/nomes/{nomes}")
    @ResponseBody
    public List<ElfoDTO> buscarTodosPorNomeEm(@PathVariable String nomes) {
        List<String> nomesList = new ArrayList<>();

        for(String nome : nomes.split(",")) {
            nomesList.add(nome);
        }

        return ElfoDTO.toListDTO(service.buscarTodosPorNomeEm(nomesList));
    }

    @GetMapping( value = "/danos/{danos}")
    @ResponseBody
    public List<ElfoDTO> buscarTodosPorDanoEm(@PathVariable List<Double> danos) {
        return ElfoDTO.toListDTO(service.buscarTodosPorDanoEm(danos));
    }

    @GetMapping( value = "/status/varios/{status}")
    @ResponseBody
    public List<ElfoDTO> buscarTodosPorStatusEm(@PathVariable("status") String status) {
        List<StatusEnum> statusList = new ArrayList<>();

        for(String statusStr : status.split(",")) {
            statusList.add(StatusEnum.valueOf(statusStr));
        }

        return ElfoDTO.toListDTO(service.buscarTodosPorStatusEm(statusList));
    }

    @GetMapping( value = "/qtdDanos/{qtdDanos}")
    @ResponseBody
    public List<ElfoDTO> buscarTodosPorQuantidadeDeDanoEm(@PathVariable("qtdDanos") String qtdDano) {
        List<Integer> qtdDanoList = new ArrayList<>();

        for(String qtdDanoStr : qtdDano.split(",")) {
            qtdDanoList.add(Integer.getInteger(qtdDanoStr));
        }

        return ElfoDTO.toListDTO(service.buscarTodosPorQuantidadeDeDanoEm(qtdDanoList));
    }

    @GetMapping( value = "/vidas/{vidas}")
    @ResponseBody
    public List<ElfoDTO> buscarTodosPorVidaEm(@PathVariable String vidas) {
        List<Double> vidaList = new ArrayList<>();

        for(String vidaStr : vidas.split(",")) {
            vidaList.add(Double.parseDouble(vidaStr));
        }

        return ElfoDTO.toListDTO(service.buscarTodosPorVidaEm(vidaList));
    }
}
