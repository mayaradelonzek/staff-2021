package br.com.dbccompany.lotr.DTO;


import br.com.dbccompany.lotr.Entity.ItemEntity;

import java.util.ArrayList;
import java.util.List;

public class ItemDTO {
    private String descricao;
    //private Inventario_X_Item inventarios;


    public ItemDTO(ItemEntity item ) {
        if (item != null) {
            this.descricao = item.getDescricao();
        }
    }

    public ItemDTO( ) {

    }

    public ItemEntity converter() {
        ItemEntity item = new ItemEntity();
        item.setDescricao( this.descricao );
        return item;
    }

    public static List<ItemDTO> toListDTO(List<ItemEntity> itens) {
        List<ItemDTO> itensDto = new ArrayList<>();

        for(ItemEntity item : itens) {
            itensDto.add(new ItemDTO(item));
        }

        return itensDto;
    }

    public static List<ItemEntity> toListEntity(List<ItemDTO> itensDTO) {
        List<ItemEntity> itens = new ArrayList<>();

        for(ItemDTO itemDTO : itensDTO) {
            itens.add(itemDTO.converter());
        }

        return itens;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
