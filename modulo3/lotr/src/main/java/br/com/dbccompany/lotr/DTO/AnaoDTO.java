package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.AnaoEntity;
import br.com.dbccompany.lotr.Entity.ItemEntity;

import java.util.ArrayList;
import java.util.List;

public class AnaoDTO extends PersonagemDTO {

    public AnaoDTO() {
    }

    public AnaoDTO(AnaoEntity anaoEntity) {
        if(anaoEntity != null) {
            this.nome = anaoEntity.getNome();
            this.experiencia = anaoEntity.getExperiencia();
            this.inventario = anaoEntity.getInventario();
            this.qtdXpAtaque = anaoEntity.getQtdXpAtaque();
            this.qtdDano = anaoEntity.getQtdDano();
            this.vida = anaoEntity.getVida();
            this.status = anaoEntity.getStatus();
        }
    }

    public AnaoEntity converter() {
        AnaoEntity anao = new AnaoEntity(this.nome);
        anao.setNome(this.nome);
        anao.setExperiencia(this.experiencia);
        anao.setInventario(this.inventario);
        anao.setVida(this.vida);
        anao.setQtdDano(this.qtdDano);
        anao.setQtdXpAtaque(this.qtdXpAtaque);
        anao.setStatus(this.status);
        return anao;
    }

    public static List<AnaoDTO> toListDTO(List<AnaoEntity> anoes) {
        List<AnaoDTO> anoesDto = new ArrayList<>();

        for(AnaoEntity anao : anoes) {
            anoesDto.add(new AnaoDTO(anao));
        }

        return anoesDto;
    }

    public static List<AnaoEntity> toListEntity(List<AnaoDTO> anoesDTO) {
        List<AnaoEntity> anoes = new ArrayList<>();

        for(AnaoDTO anaoDTO : anoesDTO) {
            anoes.add(anaoDTO.converter());
        }

        return anoes;
    }

}
