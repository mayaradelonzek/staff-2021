package br.com.dbccompany.lotr.Repository;

import br.com.dbccompany.lotr.Entity.*;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface Inventario_X_ItemRepository extends CrudRepository<Inventario_X_ItemEntity, Inventario_X_ItemId> {
    @Override
    Optional<Inventario_X_ItemEntity> findById(Inventario_X_ItemId id );
    List<Inventario_X_ItemEntity> findAllByIdIn(List<Inventario_X_ItemId> ids);
    Inventario_X_ItemEntity findByQuantidade(Integer quantidade );
    List<Inventario_X_ItemEntity> findAllByQuantidade( Integer quantidade );
    List<Inventario_X_ItemEntity> findAllByInventario(InventarioEntity inventarioItem);
    List<Inventario_X_ItemEntity> findAllByItem(ItemEntity item);

}
